CREATE TABLE apicontrol (
    apiId INT UNSIGNED NOT NULL,
    featureId INT UNSIGNED NOT NULL,
    subfeatureId INT UNSIGNED,
    reqFeatureVal TINYINT UNSIGNED NOT NULL,
    reqSubfeatureVal TINYINT UNSIGNED,
    PRIMARY KEY (apiId , featureId),
    UNIQUE (apiId, featureId , subfeatureId),
    FOREIGN KEY (apiId)
        REFERENCES apilist (apiId),
    FOREIGN KEY (featureId)
        REFERENCES features (id),
    FOREIGN KEY (subfeatureId)
        REFERENCES subfeatures (subfeatureId)
);

select * from apicontrol;
desc apicontrol;
drop table apicontrol;
delete from apicontrol;
truncate table apicontrol;

delete from apicontrol where apiId = (select apiId from apilist where methodName = 'getalluserstatus');

#User Management (General)
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (2, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (3, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (4, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (5, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (6, 6, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (7, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (8, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (9, 6, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (10, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (11, 6, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (12, 6, null, 3, null);

#Dashboard
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (13, 1, null, 2, null);

#Sales
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (14, 3, null, 2, null);

#ASN
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (15, 8, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (16, 8, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (17, 8, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (18, 8, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (19, 8, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (20, 8, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (21, 8, null, 3, null);

#Product-Catalog
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (22, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (23, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (24, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (25, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (26, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (27, 2, 2, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (28, 2, 1, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (29, 2, 1, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (30, 2, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (31, 2, 1, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (32, 2, 1, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (33, 2, 1, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (34, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (35, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (36, 2, null, 2, null);

#Inventory
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (37, 5, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (38, 5, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (39, 5, null, 2, null);

#Order
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (40, 4, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (41, 4, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (42, 4, null, 2, null);

#User Management (Roles)
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (43, 6, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (44, 6, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (45, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (46, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (47, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (48, 6, null, 2, null);

#INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (49, 6, null, 2, null);

#Order Return
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (56, 4, 4, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (57, 4, 4, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (58, 4, 4, 2, 2);

#ASN
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (59, 8, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (60, 8, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (61, 8, null, 2, null);

#Catalog-Management
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (62, 2, 5, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (63, 2, 5, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (64, 2, 5, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (65, 2, 5, 2, 2);

#Price-Management
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (66, 2, 6, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (67, 2, 6, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (68, 2, 6, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (69, 2, 6, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (70, 2, 6, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (71, 2, 6, 3, 3);

#Advance filter in ADD BULK product
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (50, 2, 1, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (51, 2, 1, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (52, 2, 1, 3, 3);

INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (73, 6, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (74, 6, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (75, 6, null, 2, null);

INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (76, 1, null, 2, null);

INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (77, 8, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (78, 8, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (79, 8, null, 2, null);

INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (80, 4, 4, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (81, 4, 4, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (82, 4, 4, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (83, 4, 4, 2, 2);