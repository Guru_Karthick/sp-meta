CREATE TABLE apilist (
    apiId INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    methodName VARCHAR(64)BINARY NOT NULL,
    className VARCHAR(64)BINARY NOT NULL,
    UNIQUE (methodName , className)
);
alter table apilist auto_increment = 1;

select * from apilist order by apiId;
desc apilist;
drop table apilist;
delete from apilist;

delete from apilist where methodName = 'getalluserstatus';

INSERT INTO apilist (methodName, className) values ('storelogin', 'general');
INSERT INTO apilist (methodName, className) values ('createuser', 'general');
INSERT INTO apilist (methodName, className) values ('updateuser', 'general');
INSERT INTO apilist (methodName, className) values ('updatestorebankdetails', 'general');
INSERT INTO apilist (methodName, className) values ('updatestoretaxdetails', 'general');
INSERT INTO apilist (methodName, className) values ('getallstoreusers', 'general');
INSERT INTO apilist (methodName, className) values ('checkfielduser', 'general');
INSERT INTO apilist (methodName, className) values ('resetpassword', 'general');
INSERT INTO apilist (methodName, className) values ('getalluserstatus', 'general');
INSERT INTO apilist (methodName, className) values ('updateuserstatus', 'general');
INSERT INTO apilist (methodName, className) values ('getuserdetails', 'general');
INSERT INTO apilist (methodName, className) values ('edituserdetails', 'general');

INSERT INTO apilist (methodName, className) values ('getdashboarddetails', 'dashboard');

INSERT INTO apilist (methodName, className) values ('getsalesdetails', 'sales');

INSERT INTO apilist (methodName, className) values ('generateasnnumber', 'asn');
INSERT INTO apilist (methodName, className) values ('checkasnnumber', 'asn');
INSERT INTO apilist (methodName, className) values ('createasn', 'asn');
INSERT INTO apilist (methodName, className) values ('updateasn', 'asn');
INSERT INTO apilist (methodName, className) values ('getallasn', 'asn');
INSERT INTO apilist (methodName, className) values ('getfilteredasn', 'asn');
INSERT INTO apilist (methodName, className) values ('deleteasn', 'asn');

INSERT INTO apilist (methodName, className) values ('getallproducts', 'product');
INSERT INTO apilist (methodName, className) values ('getpageproducts', 'product');
INSERT INTO apilist (methodName, className) values ('getpageproductsdownload', 'product');
INSERT INTO apilist (methodName, className) values ('getselectedproductsdownload', 'product');
INSERT INTO apilist (methodName, className) values ('getproductcount', 'product');
INSERT INTO apilist (methodName, className) values ('addsingleproduct', 'product');
INSERT INTO apilist (methodName, className) values ('addbulkproducts', 'product');
INSERT INTO apilist (methodName, className) values ('editbulkproducts', 'product');
INSERT INTO apilist (methodName, className) values ('deleteproduct', 'product');
INSERT INTO apilist (methodName, className) values ('filterproduct', 'product');
INSERT INTO apilist (methodName, className) values ('getadvancefilter', 'product');
INSERT INTO apilist (methodName, className) values ('getallbrands', 'product');
INSERT INTO apilist (methodName, className) values ('getallfields', 'product');
INSERT INTO apilist (methodName, className) values ('setselectedfields', 'product');
INSERT INTO apilist (methodName, className) values ('getsortedfields', 'product');

INSERT INTO apilist (methodName, className) values ('getproductinventory', 'product');
INSERT INTO apilist (methodName, className) values ('getpageproductinventory', 'product');
INSERT INTO apilist (methodName, className) values ('getproductinventorycount', 'product');

INSERT INTO apilist (methodName, className) values ('getallorders', 'order');
INSERT INTO apilist (methodName, className) values ('getpageorders', 'order');
INSERT INTO apilist (methodName, className) values ('getordercount', 'order');

INSERT INTO apilist (methodName, className) values ('getmanagemeta', 'role');
INSERT INTO apilist (methodName, className) values ('getroleslist', 'role');
INSERT INTO apilist (methodName, className) values ('createrole', 'role');
INSERT INTO apilist (methodName, className) values ('updaterole', 'role');
INSERT INTO apilist (methodName, className) values ('storeroles', 'role');
INSERT INTO apilist (methodName, className) values ('getassignedroles', 'role');
INSERT INTO apilist (methodName, className) values ('getpermissions', 'role');

INSERT INTO apilist (methodName, className) values ('getpageadvancefilter', 'product');
INSERT INTO apilist (methodName, className) values ('getpageadvancefilterdownload', 'product');
INSERT INTO apilist (methodName, className) values ('getadvancefiltercount', 'product');

INSERT INTO apilist (methodName, className) values ('forgotpassword', 'general');
INSERT INTO apilist (methodName, className) values ('createpassword', 'general');
INSERT INTO apilist (methodName, className) values ('checktokenexpiry', 'general');

INSERT INTO apilist (methodName, className) values ('getallreturns', 'order');
INSERT INTO apilist (methodName, className) values ('getpagereturns', 'order');
INSERT INTO apilist (methodName, className) values ('getreturncount', 'order');

INSERT INTO apilist (methodName, className) values ('getskuwithname', 'asn');
INSERT INTO apilist (methodName, className) values ('getpageasn', 'asn');
INSERT INTO apilist (methodName, className) values ('getasncount', 'asn');

INSERT INTO apilist (methodName, className) values ('getsearchproducts', 'product');
INSERT INTO apilist (methodName, className) values ('getpagesearchproducts', 'product');
INSERT INTO apilist (methodName, className) values ('getpagesearchproductsdownload', 'product');
INSERT INTO apilist (methodName, className) values ('getsearchproductcount', 'product');
INSERT INTO apilist (methodName, className) values ('getmanageprice', 'product');
INSERT INTO apilist (methodName, className) values ('getpagemanageprice', 'product');
INSERT INTO apilist (methodName, className) values ('getsearchmanageprice', 'product');
INSERT INTO apilist (methodName, className) values ('getpagesearchmanageprice', 'product');
INSERT INTO apilist (methodName, className) values ('getsearchmanagepricecount', 'product');
INSERT INTO apilist (methodName, className) values ('updatemanageprice', 'product');

INSERT INTO apilist (methodName, className) values ('resendpasswordlink', 'general');
INSERT INTO apilist (methodName, className) values ('getpageusers', 'general');
INSERT INTO apilist (methodName, className) values ('getpagesearchusers', 'general');
INSERT INTO apilist (methodName, className) values ('getsearchusercount', 'general');

INSERT INTO apilist (methodName, className) values ('gethomedashboarddetails', 'dashboard');

INSERT INTO apilist (methodName, className) values ('editasnshipment', 'asn');
INSERT INTO apilist (methodName, className) values ('getpagesearchasn', 'asn');
INSERT INTO apilist (methodName, className) values ('getsearchasncount', 'asn');

INSERT INTO apilist (methodName, className) values ('getpagesearchorders', 'order');
INSERT INTO apilist (methodName, className) values ('getsearchordercount', 'order');
INSERT INTO apilist (methodName, className) values ('getpagesearchreturns', 'order');
INSERT INTO apilist (methodName, className) values ('getsearchreturncount', 'order');