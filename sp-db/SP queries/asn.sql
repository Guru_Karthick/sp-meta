CREATE TABLE asn (
    asnId INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    asnNumber VARCHAR(16)BINARY NOT NULL UNIQUE,
    sellerId INT UNSIGNED NOT NULL,
    storeId INT UNSIGNED NOT NULL,
    asnStatus VARCHAR(16) NOT NULL DEFAULT 'Inprogress',
    asnType VARCHAR(16) NOT NULL,
    shipmentType VARCHAR(16) NOT NULL,
    deliveryFacility VARCHAR(64),
    referenceNumber VARCHAR(64),
    bolNumber VARCHAR(64),
    carrier VARCHAR(128),
    shippingMethod VARCHAR(64) NOT NULL,
    trackingNumber VARCHAR(64),
    shipmentDate TIMESTAMP NULL DEFAULT NULL,
    estimatedArrival TIMESTAMP NULL DEFAULT NULL,
    modeofTransport VARCHAR(32),
    shipmentFrom VARCHAR(32),
    cogi FLOAT UNSIGNED,
    additionalNotes VARCHAR(1024),
    asnDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    createdBy VARCHAR(32)BINARY NOT NULL,
    createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modifiedBy VARCHAR(32)BINARY NOT NULL,
    modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (sellerId , storeId)
        REFERENCES storeusermapper (sellerId , storeId)
);
alter table asn auto_increment = 1;

select * from asn;
desc asn;
drop table asn;
delete from asn;
truncate table asn;

select max(asnId) from newasn;

create table newasn(asnId INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT );
insert into newasn value();