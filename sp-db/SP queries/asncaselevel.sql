CREATE TABLE asncaselevel (
    caseId INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    asnId INT UNSIGNED NOT NULL,
    palletId INT UNSIGNED,
    caseLpn VARCHAR(64) NOT NULL,
    caseLength FLOAT UNSIGNED,
    caseWidth FLOAT UNSIGNED,
    caseHeight FLOAT UNSIGNED,
    caseDimensionUOM VARCHAR(16),
    caseWeight FLOAT UNSIGNED,
    caseWeightUOM VARCHAR(16),
    specialTreatmentNeeded VARCHAR(3),
    isMaterialHazardous VARCHAR(3),
    UNIQUE (asnId , caseLpn),
    FOREIGN KEY (asnId)
        REFERENCES asn (asnId),
    FOREIGN KEY (palletId)
        REFERENCES asnpalletlevel (palletId)
);

select * from asncaselevel;
desc asncaselevel;
drop table asncaselevel;
delete from asncaselevel;
truncate table asncaselevel;