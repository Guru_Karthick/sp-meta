CREATE TABLE asnpalletlevel (
    palletId INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    asnId INT UNSIGNED NOT NULL,
    palletLpn VARCHAR(64) NOT NULL,
    palletLength FLOAT UNSIGNED,
    palletWidth FLOAT UNSIGNED,
    palletHeight FLOAT UNSIGNED,
    palletDimensionUOM VARCHAR(16),
    palletWeight FLOAT UNSIGNED,
    palletWeightUOM VARCHAR(16),
    UNIQUE (asnId , palletLpn),
    FOREIGN KEY (asnId)
        REFERENCES asn (asnId)
);

select * from asnpalletlevel;
desc asnpalletlevel;
drop table asnpalletlevel;
delete from asnpalletlevel;
truncate table asnpalletlevel;