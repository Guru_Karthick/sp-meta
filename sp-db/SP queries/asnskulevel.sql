CREATE TABLE asnskulevel (
    id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    asnId INT UNSIGNED NOT NULL,
    palletId INT UNSIGNED,
    caseId INT UNSIGNED NOT NULL,
    product_id INT UNSIGNED NOT NULL,
    quantity INT UNSIGNED NOT NULL,
    status VARCHAR(16),
    UNIQUE (caseId , product_id),
    FOREIGN KEY (asnId)
        REFERENCES asn (asnId),
    FOREIGN KEY (palletId)
        REFERENCES asnpalletlevel (palletId),
    FOREIGN KEY (caseId)
        REFERENCES asncaselevel (caseId),
    FOREIGN KEY (product_id)
        REFERENCES productmapper (product_id)
);

select * from asnskulevel;
desc asnskulevel;
drop table asnskulevel;
delete from asnskulevel;
truncate table asnskulevel;

