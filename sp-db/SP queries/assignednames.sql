CREATE TABLE assignednames (
    assignId INT UNSIGNED NOT NULL AUTO_INCREMENT,
    sellerId INT UNSIGNED NOT NULL,
    roles_id INT UNSIGNED NOT NULL,
    PRIMARY KEY (assignId),
    UNIQUE (sellerId , roles_id),
    FOREIGN KEY (sellerId)
        REFERENCES users (sellerId),
    FOREIGN KEY (roles_id)
        REFERENCES roles (roles_id)
);
alter table assignednames auto_increment = 1;

select * from assignednames;
desc assignednames;
drop table assignednames;
delete from assignednames;
truncate table assignednames;