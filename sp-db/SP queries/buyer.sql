CREATE TABLE buyer (
    buyerId INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    customerId INT UNSIGNED NULL,
    name VARCHAR(64) NOT NULL,
    email VARCHAR(80) NOT NULL UNIQUE,
    phoneNumber VARCHAR(16),
    gender ENUM('M', 'F'),
    address1 VARCHAR(80),
    address2 VARCHAR(80),
    address3 VARCHAR(80),
    city VARCHAR(32),
    state VARCHAR(32),
    country VARCHAR(32),
    zipCode VARCHAR(5),
    createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

select * from buyer;
desc buyer;
drop table buyer;
delete from buyer;
truncate table buyer;

alter table buyer add column modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;

alter table buyer add constraint unique (email);

alter table buyer add column sourceCode VARCHAR(16) NOT NULL after buyerId;
update buyer T1 join orders T2 on T1.buyerId = T2.buyerId set T1.sourceCode = T2.sourceCode;