CREATE TABLE cartoninfo (
    orderId VARCHAR(32) NOT NULL,
    storeId INT UNSIGNED NOT NULL,
    cartonId VARCHAR(18) NOT NULL UNIQUE,
    cartonStatus ENUM('WAVED', 'PICKED', 'PACKED', 'SHIPPED', 'DELIVERED'),
    trackingNumber VARCHAR(18) unique,
	facilityId VARCHAR(32),    
    carrier VARCHAR(12),
    serviceLevel VARCHAR(12),
    shippingDate TIMESTAMP NULL DEFAULT NULL,
    deliveryDate TIMESTAMP NULL DEFAULT NULL,
    pickedBy VARCHAR(32),
    packedBy VARCHAR(32),
    psid VARCHAR(32),
    bin VARCHAR(32),
    pickDate TIMESTAMP NULL DEFAULT NULL,
    packDate TIMESTAMP NULL DEFAULT NULL,
    flag varchar(16),
    lastCartonStatusUpdate TIMESTAMP NOT NULL DEFAULT 0,
	FOREIGN KEY (orderId , storeId)
        REFERENCES orders (orderId , storeId)
);

CREATE TABLE cartonitems (
	cartonId VARCHAR(18) NOT NULL,
    product_id INT UNSIGNED NOT NULL,
	quantity INT UNSIGNED NOT NULL,
    UNIQUE KEY(cartonId, product_id),
    FOREIGN KEY (cartonId)
        REFERENCES cartoninfo (cartonId)
);

select * from cartoninfo;
select * from cartonitems;
select * from orders;
select * from orderitem;
drop table  cartoninfo;
select * from stores;


ALTER TABLE orders MODIFY orderStatus ENUM('Processing', 'Partially Shipped', 'Shipped', 'Partially Delivered',
'Delivered', 'Cancelled') NOT NULL;

ALTER TABLE cartoninfo change picker pickedBy VARCHAR(32);
ALTER TABLE cartoninfo change packer packedBy VARCHAR(32);
ALTER TABLE cartoninfo MODIFY cartonStatus ENUM('WAVED', 'PICKED', 'PACKED', 'SHIPPED', 'DELIVERED') NOT NULL;