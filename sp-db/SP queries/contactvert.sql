CREATE TABLE contactvert (
    id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    companyName VARCHAR(64) NOT NULL,
    name VARCHAR(64) NOT NULL,
    email VARCHAR(80) NOT NULL,
    phoneNumber VARCHAR(16) NOT NULL,
    revenue VARCHAR(32),
    maximumProductSku INT UNSIGNED,
    sellingMode VARCHAR(256),
    modeOfContact VARCHAR(8),
    preference VARCHAR(64),
    timeOfContact VARCHAR(64),
    applicationStatus ENUM('APPROVED', 'REJECTED', 'PENDING') DEFAULT 'PENDING'
);
alter table contactvert auto_increment = 1;

select * from contactvert;
desc contactvert;
drop table contactvert;
delete from contactvert;
truncate table contactvert;

delete from contactvert where companyName = 'vivo';
select distinct companyName from contactvert order by companyName;

ALTER TABLE contactvert ADD COLUMN createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
