CREATE TABLE contract (
    contractId INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    contractNumber VARCHAR(18) NOT NULL UNIQUE,
    amendmentNumber INT UNSIGNED NOT NULL,
    storeId INT UNSIGNED NOT NULL,
    contractDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    contractDescription VARCHAR(60) NOT NULL,
    contractStatus ENUM('FRESH', 'CONFIRMED', 'CANCELLED', 'AMENDED_FRESH', 'AMENDMENT_CONFIRMED') NOT NULL DEFAULT 'FRESH',
    validityStatus ENUM('VALID', 'INVALID') NOT NULL DEFAULT 'VALID',
    effectiveFrom TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    effectiveTo TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    currencyType ENUM('IND', 'USD') NOT NULL,
    contractLevelDiscount INT UNSIGNED,
    remarks VARCHAR(255),
    referenceDocumentNumber VARCHAR(60),
    paymentTerm VARCHAR(20),
    createdBy VARCHAR(32) NOT NULL,
    createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modifiedBy VARCHAR(32) NOT NULL,
    modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);

select * from contract;
DROP TABLE contract;

alter table contract modify column currencyType ENUM('INR', 'USD', 'IND') NOT NULL;
update contract set currencyType = 'INR' where currencyType = 'IND';
alter table contract modify column currencyType ENUM('INR', 'USD', 'IND') NOT NULL;

