CREATE TABLE contract_documents (
   id INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
   contractId INT UNSIGNED NOT NULL,
   documentName VARCHAR(32) NOT NULL,
   documentPath VARCHAR(512) NOT NULL,
   UNIQUE (contractId , documentName),
   FOREIGN KEY (contractId)
       REFERENCES contract (contractId)
);