CREATE TABLE contract_documents_history (
    contractId INT UNSIGNED NOT NULL,
    amendmentNumber INT UNSIGNED NOT NULL,
    documentName VARCHAR(32) NOT NULL,
    documentPath VARCHAR(512) NOT NULL,
    PRIMARY KEY (contractId , amendmentNumber , documentName),
    FOREIGN KEY (contractId , amendmentNumber)
        REFERENCES contract_history (contractId , amendmentNumber)
);

select * from contract_documents_history;