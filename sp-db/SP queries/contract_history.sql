CREATE TABLE contract_history (
    contractId INT UNSIGNED NOT NULL,
    amendmentNumber INT UNSIGNED NOT NULL,
    storeId INT UNSIGNED NOT NULL,
    contractDescription VARCHAR(60) NOT NULL,
    validityStatus ENUM('VALID', 'INVALID') NOT NULL DEFAULT 'VALID',
    effectiveFrom TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    effectiveTo TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    currencyType ENUM('INR', 'USD') NOT NULL,
    contractLevelDiscount INT UNSIGNED,
    remarks VARCHAR(255),
    referenceDocumentNumber VARCHAR(60),
    paymentTerm VARCHAR(20),
    amendedBy VARCHAR(32) NOT NULL,
    amendedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (contractId , amendmentNumber),
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);

select * from contract_history;