CREATE TABLE contract_sellingmode_mapping_history (
    contractId INT UNSIGNED NOT NULL,
    amendmentNumber INT UNSIGNED NOT NULL,
    sellingModeId INT UNSIGNED NOT NULL,
    PRIMARY KEY (contractId , amendmentNumber , sellingmodeId),
    FOREIGN KEY (contractId , amendmentNumber)
        REFERENCES contract_history (contractId , amendmentNumber),
    FOREIGN KEY (sellingModeId)
        REFERENCES sellingmode (id)
);

drop table contract_sellingmode_mapping_history;