CREATE TABLE contract_tariff_mapping (
    contractId INT UNSIGNED NOT NULL,
    tariffId INT UNSIGNED NOT NULL,
    UNIQUE (contractId , tariffId),
    PRIMARY KEY (contractId , tariffId),
    FOREIGN KEY (contractId)
        REFERENCES contract (contractId),
    FOREIGN KEY (tariffId)
        REFERENCES tariff (tariffId)
);

select * from contract_tariff_mapping;
DROP TABLE contract_tariff_mapping;