CREATE TABLE contract_tariff_mapping_history (
    contractId INT UNSIGNED NOT NULL,
    amendmentNumber INT UNSIGNED NOT NULL,
    tariffId INT UNSIGNED NOT NULL,
    PRIMARY KEY (contractId , amendmentNumber , tariffId),
    FOREIGN KEY (contractId , amendmentNumber)
        REFERENCES contract_history (contractId , amendmentNumber),
    FOREIGN KEY (tariffId)
        REFERENCES tariff (tariffId)
);

select * from contract_tariff_mapping_history;