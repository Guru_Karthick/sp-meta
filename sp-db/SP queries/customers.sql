CREATE TABLE customers (
    orderId VARCHAR(32) NOT NULL,
    storeId INT UNSIGNED NOT NULL,
    customerId INT UNSIGNED NOT NULL,
    customerName VARCHAR(64) NOT NULL,
    email VARCHAR(80) NOT NULL,
    phoneNumber VARCHAR(16),
    address1 VARCHAR(80),
    address2 VARCHAR(80),
    address3 VARCHAR(80),
    city VARCHAR(32),
    state VARCHAR(32),
    country VARCHAR(32),
    zipCode VARCHAR(5),
    PRIMARY KEY (orderId , storeId),
    FOREIGN KEY (orderId , storeId)
        REFERENCES orders (orderId , storeId)
);

select * from customers;
desc customers;
drop table customers;
delete from customers;
truncate table customers;

