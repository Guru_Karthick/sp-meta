CREATE TABLE features (
    id INT UNSIGNED AUTO_INCREMENT NOT NULL UNIQUE,
    feature VARCHAR(20) NOT NULL,
    sub_feature_id INT UNSIGNED,
    none INT UNSIGNED,
    view INT UNSIGNED,
    view_edit INT UNSIGNED,
    PRIMARY KEY (id)
);
alter table features auto_increment = 1;


select * from features;
desc features;
drop table features;
delete from features;
truncate table features;

insert into features (feature,none,view,view_edit)values("Dashboard", 1,1,1);
insert into features (feature,none,view,view_edit)values("Product Catalog", 1,1,1);
insert into features (feature,none,view,view_edit)values("Sales Dashboard", 1,1,1);
insert into features (feature,none,view,view_edit)values("Orders Dashboard", 1,1,0);
insert into features (feature,none,view,view_edit)values("Inventory Dashboard", 1,1,1);
insert into features (feature,none,view,view_edit)values("User Management", 1,0,0);
insert into features (feature,none,view,view_edit)values("Financial", 1,0 ,0);
insert into features (feature,none,view,view_edit)values("ASN", 1,1,1);
update  features set sub_feature_id=1 where id=2;
SET SQL_SAFE_UPDATES = 0;


start transaction;
savepoint role_save;
rollback to savepoint role_save;
release savepoint role_save;