CREATE TABLE fieldmaster (
    field_id INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT,
    name VARCHAR(32) NOT NULL UNIQUE,
    datatype VARCHAR(32) NOT NULL,
    length SMALLINT,
    cons_notnull BOOLEAN NOT NULL,
    cons_possible_value BOOLEAN NOT NULL,
    possible_values JSON,
    PRIMARY KEY (field_id)
);

SELECT * FROM fieldmaster;
DESC fieldmaster;
DROP TABLE fieldmaster;
DELETE FROM fieldmaster;
truncate table fieldmaster;

alter table fieldmaster
add column issearchable BOOLEAN NOT NULL default 0 after ismandotry;
SELECT field_id FROM fieldmaster WHERE issearchable = 0;

update fieldmaster set datatype = 'boolean' where name like 'country_of_manufacture';

update fieldmaster set cons_notnull = 1 where name = 'MPN';

INSERT INTO `fieldmaster` VALUES (1,'sku','varchar',64,1,0,NULL),(2,'parent_id','varchar',64,1,0,NULL),(3,'UPC','int',64,1,0,NULL),(4,'MPN','int',64,1,0,NULL),(5,'product_name','varchar',64,1,0,NULL),(6,'description','varchar',256,1,0,NULL),(7,'long_description','varchar',1024,0,0,NULL),(8,'weight','float',NULL,1,0,NULL),(9,'price_on_ecommerce','float',NULL,1,0,NULL),(10,'main_image','varchar',512,1,0,NULL),(11,'child_product','boolean',NULL,0,0,NULL),(12,'display_position','varchar',32,0,0,NULL),(13,'multipack','boolean',NULL,0,0,NULL),(14,'multipack_Quantity','int',256,0,0,NULL),(15,'multipack_items_name','varchar',256,0,0,NULL),(16,'product_length','float',NULL,0,0,NULL),(17,'product_width','float',NULL,0,0,NULL),(18,'product_height','float',NULL,0,0,NULL),(19,'product_color','varchar',64,0,0,NULL),(20,'product_size','varchar',64,0,0,NULL),(21,'volume_UOM','float',NULL,0,0,NULL),(22,'weight_UOM','float',NULL,0,0,NULL),(23,'dimensions_UOM','float',NULL,0,0,NULL),(24,'standard_price','float',NULL,0,0,NULL),(25,'retail_price','float',NULL,1,0,NULL),(26,'name_registered','varchar',64,1,0,NULL),(27,'barcode_number','varchar',256,0,0,NULL),(28,'shipping_exceptions','varchar',256,0,0,NULL),(29,'package_type','varchar',32,1,1,'{\"value\": [\"Box\", \"wrapper\", \"plastic cover\"]}'),(30,'shipping_weight','float',NULL,0,0,NULL),(31,'package_length','float',NULL,0,0,NULL),(32,'package_width','float',NULL,0,0,NULL),(33,'package_height','float',NULL,0,0,NULL),(34,'package_length_UOM','float',NULL,0,0,NULL),(35,'package_width_UOM','float',NULL,0,0,NULL),(36,'package_height_UOM','float',NULL,0,0,NULL),(37,'package_weight_UOM','float',NULL,0,0,NULL),(38,'handling_type','varchar',32,1,1,'{\"value\": [\"NORMAL\", \"Special Handling\"]}'),(39,'fragile','boolean',NULL,0,0,NULL),(40,'country_of_manufacture','varchar',64,0,0,NULL),(41,'sub_category','varchar',64,0,0,NULL),(42,'taxable','boolean',NULL,0,0,NULL),(43,'tax_category','varchar',64,0,0,NULL),(44,'brand','varchar',64,0,0,NULL),(45,'serial_number_tracked','boolean',NULL,0,0,NULL),(46,'batch_number_tracked','boolean',NULL,0,0,NULL),(47,'manufacturer','varchar',64,0,0,NULL),(48,'features','varchar',256,0,0,NULL),(49,'search_term_suggestions','varchar',256,0,0,NULL),(50,'customization_available','boolean',NULL,0,0,NULL),(51,'gift_message_available','boolean',NULL,0,0,NULL),(52,'Safety_wrap_required','boolean',NULL,0,0,NULL),(53,'side_image_1','varchar',512,0,0,NULL),(54,'side_image_2','varchar',512,0,0,NULL),(55,'top_image_1','varchar',512,0,0,NULL),(56,'top_image_2','varchar',512,0,0,NULL),(57,'front_image_1','varchar',512,0,0,NULL),(58,'front_image_2','varchar',512,0,0,NULL),(59,'rear_image_1','varchar',512,0,0,NULL),(60,'rear_image_2','varchar',512,0,0,NULL),(61,'specification_image','varchar',512,0,0,NULL),(62,'size_chart_image','varchar',512,0,0,NULL),(63,'additional_attribute_1','varchar',512,0,0,NULL),(64,'additional_attribute_2','varchar',512,0,0,NULL),(65,'additional_attribute_3','varchar',512,0,0,NULL),(66,'additional_attribute_4','varchar',512,0,0,NULL),(67,'additional_attribute_5','varchar',512,0,0,NULL),(68,'additional_attribute_6','varchar',512,0,0,NULL),(69,'additional_attribute_7','varchar',512,0,0,NULL),(70,'additional_attribute_8','varchar',512,0,0,NULL),(71,'additional_attribute_9','varchar',512,0,0,NULL),(72,'additional_attribute_10','varchar',512,0,0,NULL),(73,'created_at','timestamp',NULL,0,0,NULL),(74,'updated_at','timestamp',NULL,0,0,NULL);

INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('sku', 'varchar', 64, true, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('product_type', 'varchar', 32, true, true, JSON_OBJECT("value",JSON_ARRAY("simple", "grouped", "bundle", "configurable", "virtual")));
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('name', 'varchar', 64, true, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('description', 'varchar', 1024, true, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('short_description', 'varchar', 256, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('weight', 'float', NULL, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('country_of_manufacture', 'varchar', 64, true, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('product_online', 'boolean', NULL, true, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('visibility', 'varchar', 32, true, true, JSON_OBJECT("value",JSON_ARRAY("Not visible", "Individually", "Catalog", "Search", "Catalog, Search")));
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('price', 'float', NULL, true, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('special_price', 'float', NULL, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('special_price_from_date', 'timestamp', NULL, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('special_price_to_date', 'timestamp', NULL, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('base_image', 'varchar', 512, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('new_from_date', 'timestamp', NULL, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('new_to_date', 'timestamp', NULL, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('map_price', 'float', NULL, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('msrp_price', 'float', NULL, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('map_enabled', 'boolean', NULL, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('gift_message_available', 'boolean', NULL, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('qty', 'int', NULL, true, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('min_cart_qty', 'int', NULL, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('max_cart_qty', 'int', NULL, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('related_skus', 'varchar', 64, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('crosssell_skus', 'varchar', 64, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('upsell_skus', 'varchar', 64, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('created_at', 'timestamp', NULL, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('updated_at', 'timestamp', NULL, false, false, null);

INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('product_type', 'varchar', 32, false, true, JSON_OBJECT("value",JSON_ARRAY("simple", "grouped", "bundle", "configurable", "virtual")));
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values)
VALUES ('facility_store', 'varchar', 64, false, true, JSON_OBJECT("value",JSON_ARRAY("Refrigerate", "Normal Storage", "Hanging Racks", "Special handling")));
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('category_level', 'varchar', 64, false, true, JSON_OBJECT("value",JSON_ARRAY("Parent Category", "Child Category", "Sub Category")));
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('parent_category', 'varchar', 64, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('child_category', 'varchar', 64, false, false, null);
#INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
#VALUES ('sub_category', 'varchar', 64, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('online_availability', 'boolean', null, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('material', 'varchar', 256, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('design', 'varchar', 64, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('sleeve', 'varchar', 64, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('radial_dimension', 'varchar', 64, false, false, null);
#INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
#VALUES ('search_term_suggestion', 'varchar', 256, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('new_from_date', 'timestamp', NULL, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('new_to_date', 'timestamp', NULL, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('asin', 'varchar', 10, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('vas_description', 'varchar', 256, false, false, null);
INSERT INTO fieldmaster (name, datatype, length, cons_notnull, cons_possible_value, possible_values) 
VALUES ('product_category', 'varchar', 64, false, true, JSON_OBJECT("value",JSON_ARRAY("Automotives", "Electronics", "Apparels", "Accesories")));

update fieldmaster set possible_values = JSON_OBJECT("value",JSON_ARRAY("Parent Category", "Child Category", "Sub Category"));
update fieldmaster set possible_values = null where name != 'category_level';

select * from fieldmaster where name <> 'created_at' AND name <> 'updated_at';



alter table fieldmaster 
add column title VARCHAR(32) NOT NULL after name;
alter table fieldmaster 
add column default_selected boolean NOT NULL after length;
alter table fieldmaster
change cons_notnull ismandotry BOOLEAN NOT NULL;
alter table fieldmaster
change cons_possible_value has_possible_values BOOLEAN NOT NULL;

update fieldmaster set title = 'SKU', default_selected = 1 where field_id = 1;
update fieldmaster set title = 'Parent ID', default_selected = 0 where field_id = 2;
update fieldmaster set title = 'UPC', default_selected = 1 where field_id = 3;
update fieldmaster set title = 'MPN', default_selected = 1 where field_id = 4;
update fieldmaster set title = 'Product Name', default_selected = 1 where field_id = 5;
update fieldmaster set title = 'Description', default_selected = 1 where field_id = 6;
update fieldmaster set title = 'Long Description', default_selected = 0 where field_id = 7;
update fieldmaster set title = 'Weight', default_selected = 0 where field_id = 8;
update fieldmaster set title = 'Price $', default_selected = 1 where field_id = 9;
update fieldmaster set title = 'Image', default_selected = 1 where field_id = 10;
update fieldmaster set title = 'Child Product', default_selected = 0 where field_id = 11;
update fieldmaster set title = 'Display Position', default_selected = 0 where field_id = 12;
update fieldmaster set title = 'Multipack', default_selected = 0 where field_id = 13;
update fieldmaster set title = 'Multipack Quantity', default_selected = 0 where field_id = 14;
update fieldmaster set title = 'Multipack Items', default_selected = 0 where field_id = 15;
update fieldmaster set title = 'Product Length', default_selected = 0 where field_id = 16;
update fieldmaster set title = 'Product Width', default_selected = 0 where field_id = 17;
update fieldmaster set title = 'Product Height', default_selected = 0 where field_id = 18;
update fieldmaster set title = 'Product Color', default_selected = 0 where field_id = 19;
update fieldmaster set title = 'Product Size', default_selected = 0 where field_id = 20;
update fieldmaster set title = 'Volume UOM', default_selected = 0 where field_id = 21;
update fieldmaster set title = 'Weight UOM', default_selected = 0 where field_id = 22;
update fieldmaster set title = 'Dimensions UOM', default_selected = 0 where field_id = 23;
update fieldmaster set title = 'Standard Price', default_selected = 0 where field_id = 24;
update fieldmaster set title = 'Retail Price', default_selected = 0 where field_id = 25;
update fieldmaster set title = 'Registered Name', default_selected = 0 where field_id = 26;
update fieldmaster set title = 'Barcode', default_selected = 0 where field_id = 27;
update fieldmaster set title = 'Shipping Exceptions', default_selected = 0 where field_id = 28;
update fieldmaster set title = 'Package Type', default_selected = 0 where field_id = 29;
update fieldmaster set title = 'Shipping Weight', default_selected = 0 where field_id = 30;
update fieldmaster set title = 'Package Length', default_selected = 0 where field_id = 31;
update fieldmaster set title = 'Package Width', default_selected = 0 where field_id = 32;
update fieldmaster set title = 'Package Height', default_selected = 0 where field_id = 33;
update fieldmaster set title = 'Package Length UOM', default_selected = 0 where field_id = 34;
update fieldmaster set title = 'Package Width UOM', default_selected = 0 where field_id = 35;
update fieldmaster set title = 'Package Height UOM', default_selected = 0 where field_id = 36;
update fieldmaster set title = 'Package Weight UOM', default_selected = 0 where field_id = 37;
update fieldmaster set title = 'Handling Type', default_selected = 0 where field_id = 38;
update fieldmaster set title = 'Fragile', default_selected = 0 where field_id = 39;
update fieldmaster set title = 'Country of manufacture', default_selected = 0 where field_id = 40;
update fieldmaster set title = 'Subcategory', default_selected = 0 where field_id = 41;
update fieldmaster set title = 'Taxable', default_selected = 0 where field_id = 42;
update fieldmaster set title = 'Tax Category', default_selected = 0 where field_id = 43;
update fieldmaster set title = 'Brand', default_selected = 1 where field_id = 44;
update fieldmaster set title = 'Serial Number Tracked', default_selected = 0 where field_id = 45;
update fieldmaster set title = 'Batch Number Tracked', default_selected = 0 where field_id = 46;
update fieldmaster set title = 'Manufacturer', default_selected = 1 where field_id = 47;
update fieldmaster set title = 'Features', default_selected = 0 where field_id = 48;
update fieldmaster set title = 'Search Term Suggestions', default_selected = 0 where field_id = 49;
update fieldmaster set title = 'Customizations Available', default_selected = 0 where field_id = 50;
update fieldmaster set title = 'Gift Message Available', default_selected = 0 where field_id = 51;
update fieldmaster set title = 'Safety Wrap Required', default_selected = 0 where field_id = 52;
update fieldmaster set title = 'Side Image 1', default_selected = 0 where field_id = 53;
update fieldmaster set title = 'Side Image 2', default_selected = 0 where field_id = 54;
update fieldmaster set title = 'Top Image 1', default_selected = 0 where field_id = 55;
update fieldmaster set title = 'Top Image 2', default_selected = 0 where field_id = 56;
update fieldmaster set title = 'Front Image 1', default_selected = 0 where field_id = 57;
update fieldmaster set title = 'Front Image 2', default_selected = 0 where field_id = 58;
update fieldmaster set title = 'Rear Image 1', default_selected = 0 where field_id = 59;
update fieldmaster set title = 'Rear Image 2', default_selected = 0 where field_id = 60;
update fieldmaster set title = 'Specification Image', default_selected = 0 where field_id = 61;
update fieldmaster set title = 'Size Chart Image', default_selected = 0 where field_id = 62;
update fieldmaster set title = 'Additional Attribute 1', default_selected = 0 where field_id = 63;
update fieldmaster set title = 'Additional Attribute 2', default_selected = 0 where field_id = 64;
update fieldmaster set title = 'Additional Attribute 3', default_selected = 0 where field_id = 65;
update fieldmaster set title = 'Additional Attribute 4', default_selected = 0 where field_id = 66;
update fieldmaster set title = 'Additional Attribute 5', default_selected = 0 where field_id = 67;
update fieldmaster set title = 'Additional Attribute 6', default_selected = 0 where field_id = 68;
update fieldmaster set title = 'Additional Attribute 7', default_selected = 0 where field_id = 69;
update fieldmaster set title = 'Additional Attribute 8', default_selected = 0 where field_id = 70;
update fieldmaster set title = 'Additional Attribute 9', default_selected = 0 where field_id = 71;
update fieldmaster set title = 'Additional Attribute 10', default_selected = 0 where field_id = 72;
update fieldmaster set title = 'Created At', default_selected = 0 where field_id = 73;
update fieldmaster set title = 'Updated At', default_selected = 0 where field_id = 74;
update fieldmaster set title = 'Product Type', default_selected = 0 where field_id = 75;
update fieldmaster set title = 'Facility Store', default_selected = 0 where field_id = 76;
update fieldmaster set title = 'Category Level', default_selected = 0 where field_id = 77;
update fieldmaster set title = 'Parent Category', default_selected = 0 where field_id = 78;
update fieldmaster set title = 'Child Category', default_selected = 0 where field_id = 79;
update fieldmaster set title = 'Online Catgeory', default_selected = 0 where field_id = 80;
update fieldmaster set title = 'Material', default_selected = 0 where field_id = 81;
update fieldmaster set title = 'Design', default_selected = 0 where field_id = 82;
update fieldmaster set title = 'Sleeve', default_selected = 0 where field_id = 83;
update fieldmaster set title = 'Radial Dimension', default_selected = 0 where field_id = 84;
update fieldmaster set title = 'New From Date', default_selected = 0 where field_id = 85;
update fieldmaster set title = 'New To Date', default_selected = 0 where field_id = 86;
update fieldmaster set title = 'ASIN', default_selected = 0 where field_id = 87;
update fieldmaster set title = 'VAS Description', default_selected = 0 where field_id = 88;
update fieldmaster set title = 'Product Category', default_selected = 0 where field_id = 89;


INSERT INTO fieldmaster (name, title, datatype, length, ismandotry, default_selected, has_possible_values, possible_values) 
VALUES ('package_dimension_UOM','Package Dimension UOM', 'varchar', 8, false, false, true, JSON_OBJECT("value",JSON_ARRAY("CM", "INCH")));

update fieldmaster set title = 'Parent SKU ID', default_selected = 0, ismandotry = 0 where field_id = 2;

update fieldmaster set name = 'parent_sku_id' where field_id = 2;
update fieldmaster set name = 'storage', title = 'Storage' where field_id = 76;



UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='1';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='2';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='3';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='4';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='5';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='6';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='7';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='89';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='15';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='19';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='27';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='29';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='78';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='79';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='41';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='43';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='44';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='47';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='48';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='88';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='87';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='63';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='64';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='65';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='66';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='67';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='68';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='69';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='70';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='71';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='72';

INSERT INTO fieldmaster (name, title, datatype, length, default_selected, ismandotry, issearchable, has_possible_values, possible_values)
VALUES ('safety_stock_qty','Safety Stock', 'int', 8, 0, true, false, false, null);
INSERT INTO fieldmaster (name, title, datatype, length, default_selected, ismandotry, issearchable, has_possible_values, possible_values)
VALUES ('distressed_stock_qty','Distressed Stock', 'int', 8, 0, true, false, false, null);

UPDATE `sellerportal`.`fieldmaster` SET `ismandotry`='1' WHERE `field_id`='24';
UPDATE `sellerportal`.`fieldmaster` SET `ismandotry`='1' WHERE `field_id`='86';
UPDATE `sellerportal`.`fieldmaster` SET `ismandotry`='1' WHERE `field_id`='85';
UPDATE `sellerportal`.`fieldmaster` SET `ismandotry`='1' WHERE `field_id`='16';
UPDATE `sellerportal`.`fieldmaster` SET `ismandotry`='1' WHERE `field_id`='17';
UPDATE `sellerportal`.`fieldmaster` SET `ismandotry`='1' WHERE `field_id`='18';
