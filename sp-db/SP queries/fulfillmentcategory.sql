CREATE TABLE fulfillmentcategory (
    categoryId INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    categoryName VARCHAR(64) NOT NULL,
    UNIQUE (categoryName)
);

select * from fulfillmentcategory;
desc fulfillmentcategory;
drop table fulfillmentcategory;
delete from fulfillmentcategory;
truncate table fulfillmentcategory;


INSERT INTO fulfillmentcategory(categoryName) VALUE ('Bags');
INSERT INTO fulfillmentcategory(categoryName) VALUE ('Watches');
INSERT INTO fulfillmentcategory(categoryName) VALUE ('Suitcases');
INSERT INTO fulfillmentcategory(categoryName) VALUE ('Shoes');
INSERT INTO fulfillmentcategory(categoryName) VALUE ('EarPhones');