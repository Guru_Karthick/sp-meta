CREATE TABLE global_fieldmaster (
    fieldId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    category ENUM('ASN_OUTER_GRID', 'ASN_INNER_GRID') NOT NULL,
    fieldName VARCHAR(32) NOT NULL,
    title VARCHAR(32) NOT NULL,
    datatypeInUI VARCHAR(16) NOT NULL,
    defaultSelected BOOLEAN NOT NULL,
    UNIQUE (category , fieldName)
);

SELECT * FROM global_fieldmaster;
DESC global_fieldmaster;
DROP TABLE global_fieldmaster;
DELETE FROM global_fieldmaster;
truncate table global_fieldmaster;

INSERT INTO global_fieldmaster VALUES(1, 'ASN_OUTER_GRID', 'asnNumber', 'ASN Number', 'string', TRUE);
INSERT INTO global_fieldmaster VALUES(2, 'ASN_OUTER_GRID', 'asnStatus', 'ASN Status','string', TRUE);
INSERT INTO global_fieldmaster VALUES(3, 'ASN_OUTER_GRID', 'amendmentNumber', 'Amendment Number','number', TRUE);
INSERT INTO global_fieldmaster VALUES(4, 'ASN_OUTER_GRID', 'asnDateTime', 'ASN Date','datetime', TRUE);
INSERT INTO global_fieldmaster VALUES(5, 'ASN_OUTER_GRID', 'asnSource', 'Source of ASN','string', TRUE);
INSERT INTO global_fieldmaster VALUES(6, 'ASN_OUTER_GRID', 'deliveryMode', 'Delivery Mode','string', TRUE);
INSERT INTO global_fieldmaster VALUES(7, 'ASN_OUTER_GRID', 'shipmentDate', 'Shipment Date','datetime', TRUE);
INSERT INTO global_fieldmaster VALUES(8, 'ASN_OUTER_GRID', 'appointmentNumber', 'Appointment Number','string', TRUE);
INSERT INTO global_fieldmaster VALUES(9, 'ASN_OUTER_GRID', 'originatingLocation', 'Originating Location','string', TRUE);
INSERT INTO global_fieldmaster VALUES(10, 'ASN_OUTER_GRID', 'destinationLocation', 'Destination Location','string', TRUE);
INSERT INTO global_fieldmaster VALUES(11, 'ASN_OUTER_GRID', 'sealNumber', 'Seal Number','string', TRUE);
INSERT INTO global_fieldmaster VALUES(12, 'ASN_OUTER_GRID', 'referenceDocumentNumber', 'Reference Document Number','string', TRUE);
INSERT INTO global_fieldmaster VALUES(13, 'ASN_OUTER_GRID', 'vendorNumber', 'Vendor Number','string', TRUE);
INSERT INTO global_fieldmaster VALUES(14, 'ASN_OUTER_GRID', 'vendorName', 'Vendor Name','string', TRUE);
INSERT INTO global_fieldmaster VALUES(15, 'ASN_OUTER_GRID', 'vendorAsnNumber', 'Vendor ASN Number','string', TRUE);
INSERT INTO global_fieldmaster VALUES(16, 'ASN_OUTER_GRID', 'asnSentBy', 'ASN Sent by','string', TRUE);
INSERT INTO global_fieldmaster VALUES(17, 'ASN_OUTER_GRID', 'carrierName', 'Carrier Name','string', TRUE);
INSERT INTO global_fieldmaster VALUES(18, 'ASN_OUTER_GRID', 'billOfLadingNumber', 'Bill of Lading Number','string', TRUE);
INSERT INTO global_fieldmaster VALUES(19, 'ASN_OUTER_GRID', 'driverName', 'Driver Name','string', TRUE);
INSERT INTO global_fieldmaster VALUES(20, 'ASN_OUTER_GRID', 'vehicleNumber', 'Vehicle Number','string', TRUE);
INSERT INTO global_fieldmaster VALUES(21, 'ASN_OUTER_GRID', 'trackingNumber', 'Tracking Number','string', TRUE);
INSERT INTO global_fieldmaster VALUES(22, 'ASN_OUTER_GRID', 'packagingOptions', 'Packaging Options','string', TRUE);


INSERT INTO global_fieldmaster VALUES(23, 'ORDER_RETURN_GRID', 'orderReturnId', 'Return/RMA ID','string', TRUE);
INSERT INTO global_fieldmaster VALUES(24, 'ORDER_RETURN_GRID', 'orderId', 'Order ID','string', TRUE);
INSERT INTO global_fieldmaster VALUES(25, 'ORDER_RETURN_GRID', 'source', 'Source','string', TRUE);
INSERT INTO global_fieldmaster VALUES(26, 'ORDER_RETURN_GRID', 'returnStatus', 'Return Status','string', TRUE);
INSERT INTO global_fieldmaster VALUES(27, 'ORDER_RETURN_GRID', 'customerName', 'Customer Name','string', TRUE);
INSERT INTO global_fieldmaster VALUES(28, 'ORDER_RETURN_GRID', 'sku', 'SKU','string', TRUE);
INSERT INTO global_fieldmaster VALUES(29, 'ORDER_RETURN_GRID', 'productName', 'Product Name','string', TRUE);
INSERT INTO global_fieldmaster VALUES(30, 'ORDER_RETURN_GRID', 'manufacturer', 'Manufacturer Name','string', TRUE);
INSERT INTO global_fieldmaster VALUES(31, 'ORDER_RETURN_GRID', 'reasonForReturn', 'Reason for Return','string', TRUE);
INSERT INTO global_fieldmaster VALUES(32, 'ORDER_RETURN_GRID', 'returnedProductImage', 'Product Image','string', TRUE);
INSERT INTO global_fieldmaster VALUES(33, 'ORDER_RETURN_GRID', 'orderedQuantity', 'Order Quantity','number', TRUE);
INSERT INTO global_fieldmaster VALUES(34, 'ORDER_RETURN_GRID', 'returnedQuantity', 'Return Quantity','number', TRUE);
INSERT INTO global_fieldmaster VALUES(35, 'ORDER_RETURN_GRID', 'purchasedOn', 'Purchased On','datetime', TRUE);
INSERT INTO global_fieldmaster VALUES(36, 'ORDER_RETURN_GRID', 'returnRequestedOn', 'Return Requested On','datetime', TRUE);
INSERT INTO global_fieldmaster VALUES(37, 'ORDER_RETURN_GRID', 'additionalComments', 'Comments','string', TRUE);
