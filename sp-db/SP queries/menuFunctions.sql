CREATE TABLE menuFunctions (
   functionId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
   labelId VARCHAR(64) NOT NULL,
   functionName VARCHAR(32) NOT NULL,
   menuId INT UNSIGNED NOT NULL,
   yes  BOOLEAN NOT NULL,
   no BOOLEAN NOT NULL,
   functionHierachy INT UNSIGNED,
   FOREIGN KEY (menuId)
        REFERENCES menus (menuId)
  );
  
  SELECT * FROM menuFunctions;
  
  
INSERT INTO menuFunctions VALUES('1', 'addProduct', 'Add Product', '17', '1', '1', '1');
 INSERT INTO menuFunctions VALUES('2', 'addBulk', 'Add Bulk', '17', '1', '1', '2');
 INSERT INTO menuFunctions VALUES('3', 'bulkEdit', 'Bulk Edit', '17', '1', '1', '3');
 INSERT INTO menuFunctions VALUES('4', 'activateOrDeactivate', 'Activate Or De-Active Products', '17', '1', '1', '4');
 INSERT INTO menuFunctions VALUES('5', 'users', 'Users', '19', '1', '1', '1');
 INSERT INTO menuFunctions VALUES('6', 'roles', 'Roles', '19', '1', '1', '2');
 INSERT INTO menuFunctions VALUES('7', 'addUser', 'Add User', '19', '1', '1', '3');
 INSERT INTO menuFunctions VALUES('8', 'editUser', 'Edit User', '19', '1', '1', '4');
 INSERT INTO menuFunctions VALUES('9', 'addRole', 'Add Role', '19', '1', '1', '5');
 INSERT INTO menuFunctions VALUES('10', 'editRole', 'Edit Role', '19', '1', '1', '6');
 INSERT INTO menuFunctions VALUES('11', 'activateOrDeactivateUsers', 'Activate Or De-Active Users', '19', '1', '1', '7');
 INSERT INTO menuFunctions VALUES('12', 'columns', 'Columns', '17', '1', '1', '5');
 INSERT INTO menuFunctions VALUES('13', 'export', 'Export', '17', '1', '1', '6'); 
 INSERT INTO menuFunctions VALUES('14', 'orderDetails', 'Order Details', '8', '1', '1', '1');
 INSERT INTO menuFunctions VALUES('15', 'orderColumns', 'Columns', '8', '1', '1', '2');
 INSERT INTO menuFunctions VALUES('16', 'orderExport', 'Export', '8', '1', '1', '3'); 
 INSERT INTO menuFunctions VALUES('17', 'returnDetails', 'Return Details', '9', '1', '1', '1');
 INSERT INTO menuFunctions VALUES('18', 'returnColumns', 'Return Columns', '9', '1', '1', '2'); 
 INSERT INTO menuFunctions VALUES('19', 'invColumns', 'Export', '11', '1', '1', '1');
 INSERT INTO menuFunctions VALUES('20', 'invExport', 'Columns', '11', '1', '1', '2');
 INSERT INTO menuFunctions VALUES('21', 'addAsn', 'Add ASN', '12', '1', '1', '1');
 INSERT INTO menuFunctions VALUES('22', 'editAsn', 'Edit ASN', '12', '1', '1', '2'); 
 INSERT INTO menuFunctions VALUES('23', 'addBulkAsn', 'Add Bulk Asn', '13', '1', '1', '1');
 INSERT INTO menuFunctions VALUES('24', 'asnFilter', 'Filters', '13', '1', '1', '2');
 INSERT INTO menuFunctions VALUES('25', 'asnBulkColumns', 'Columns', '13', '1', '1', '3');  
 INSERT INTO menuFunctions VALUES('26', 'editPrice', 'Edit Price', '15', '1', '1', '1');
 INSERT INTO menuFunctions VALUES('27', 'auditDownload', 'Download', '21', '1', '1', '1');
 
 
INSERT INTO menuFunctions VALUES(28, 'asnAdd', 'Add ASN', 23, TRUE, TRUE, 1);
INSERT INTO menuFunctions VALUES(29, 'asnColumns', 'Columns', 23, TRUE, TRUE, 2);
INSERT INTO menuFunctions VALUES(30, 'asnAmend', 'Amend', 23, TRUE, TRUE, 3);
INSERT INTO menuFunctions VALUES(31, 'asnPrintPackSlip', 'Print Packslip', 23, TRUE, TRUE, 4);
INSERT INTO menuFunctions VALUES(32, 'asnPrintLabels', 'Print Labels', 23, TRUE, TRUE, 5);
INSERT INTO menuFunctions VALUES(33, 'asnCancel', 'Cancel', 23, TRUE, TRUE, 6);
INSERT INTO menuFunctions VALUES(34, 'asnExport', 'Export', 23, TRUE, TRUE, 7);