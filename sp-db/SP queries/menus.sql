CREATE TABLE menus (
   menuId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
   menuName VARCHAR(32) NOT NULL,
   routerPath VARCHAR(64),
   menuIconNormal VARCHAR(512),
   menuIconActive VARCHAR(512),
   parentMenuId INT UNSIGNED,
   yes  BOOLEAN NOT NULL,
   no  BOOLEAN NOT NULL,
   isMainMenu BOOLEAN NOT NULL,
   menuHierachy INT UNSIGNED,
    FOREIGN KEY (parentMenuId)
        REFERENCES menus (menuId)
  );

select * from menus;

INSERT INTO menus VALUES(1, 'Dashboards', NULL, '../../../../assets/Assets/Whendonothoverover/home-normal-1.svg', '../../../../assets/Assets/Whendohoverover/home-active-1.svg', NULL, TRUE, TRUE, TRUE, 1);
INSERT INTO menus VALUES(2, 'Summary Dashboard', '/summary-dashboard', NULL, NULL, 1, TRUE, TRUE, FALSE, 1);
INSERT INTO menus VALUES(3, 'Sales Dashboard', '/sales-dashboard', NULL, NULL, 1, TRUE, TRUE, FALSE, 2);
INSERT INTO menus VALUES(4, 'Finance Dashboard', '/financial-dashboard', NULL, NULL, 1, TRUE, TRUE, FALSE, 3);
INSERT INTO menus VALUES(5, 'Orders Dashboard', '/orders-dashboard', NULL, NULL, 1, TRUE, TRUE, FALSE, 4);
INSERT INTO menus VALUES(6, 'Inventory Dashboard', '/inventory-dashboard', NULL, NULL, 1, TRUE, TRUE, FALSE, 5);

INSERT INTO menus VALUES(7, 'Sales', NULL, '../../../../assets/Assets/Whendonothoverover/sales-normal-1.svg', '../../../../assets/Assets/Whendohoverover/sales-active-1.svg', NULL, TRUE, TRUE, TRUE, 2);
INSERT INTO menus VALUES(8, 'Orders Enquiry', '/ordersEnquiry', NULL, NULL, 7, TRUE, TRUE, FALSE, 1);
INSERT INTO menus VALUES(9, 'Order-Returns', '/orderReturns', NULL, NULL, 7, TRUE, TRUE, FALSE, 2);


INSERT INTO menus VALUES(10, 'Inventory', NULL, '../../../../assets/Assets/Whendonothoverover/inventory-normal-1.svg', '../../../../assets/Assets/Whendohoverover/inventory-active-1.svg', NULL, TRUE, TRUE, TRUE, 3);
INSERT INTO menus VALUES(11, 'Stock Enquiry', '/stockEnquiry', NULL, NULL, 10, TRUE, TRUE, FALSE, 1);
INSERT INTO menus VALUES(12, 'Manage ASN', '/manageASN', NULL, NULL, 10, TRUE, TRUE, FALSE, 2);
INSERT INTO menus VALUES(13, 'ASN BulkUpload', '/ASN-bulkupload', NULL, NULL, 10, TRUE, TRUE, FALSE, 3);

INSERT INTO menus VALUES(14, 'Pricing', NULL, '../../../../assets/Assets/Whendonothoverover/catalog-normal-1.svg', '../../../../assets/Assets/Whendohoverover/catalog-active-1.svg', NULL, TRUE, TRUE, TRUE, 4);
INSERT INTO menus VALUES(15, 'Manage Pricing', '/managePrice', NULL, NULL, 14, TRUE, TRUE, FALSE, 1);


INSERT INTO menus VALUES(16, 'Catalog', NULL, '../../../../assets/Assets/Whendonothoverover/catalog-normal-1.svg', '../../../../assets/Assets/Whendohoverover/catalog-active-1.svg', NULL, TRUE, TRUE, TRUE, 5);
INSERT INTO menus VALUES(17, 'Manage Catalog', '/manageCatalog', NULL, NULL, 16, TRUE, TRUE, FALSE, 1);

INSERT INTO menus VALUES(18, 'Settings', NULL, '../../../../assets/Assets/Whendonothoverover/users-normal-1.svg', '../../../../assets/Assets/Whendohoverover/users-active-1.svg', NULL, TRUE, TRUE, TRUE, 6);
INSERT INTO menus VALUES(19, 'Roles and Users', '/rolesAndUsers', NULL, NULL, 18, TRUE, TRUE, FALSE, 1);
INSERT INTO menus VALUES(20, 'Notification Preferences', NULL, NULL, NULL, 18, TRUE, TRUE, FALSE, 2);
INSERT INTO menus VALUES(21, 'Audit Trail', '/audit-trail', NULL, NULL, 18, TRUE, TRUE, FALSE, 3);

INSERT INTO menus VALUES(22, 'Order-D3', '/orderdashboardD3', NULL, NULL, 7, TRUE, TRUE, FALSE, 3);

UPDATE menus SET menuHierachy = 4 WHERE menuId = 12;
INSERT INTO menus VALUES(23, 'ASN', '/asn', NULL, NULL, 10, TRUE, TRUE, FALSE, 3);
INSERT INTO menus VALUES(24, 'Sales-D3', '/salesdashboardD3', NULL, NULL, 7, TRUE, TRUE, FALSE, 4);


select * FROM MENUS;



DELETE T1 FROM userSelectedMenus T1 JOIN 
(SELECT T1.roleId, T1.menuId FROM userSelectedMenus T1 JOIN menus T2 ON T1.menuId = T2.menuId WHERE T2.menuId IN (1,2,3,4,5,6)) AS T2
WHERE (T1.roleId, T1.menuId) = (T2.roleId, T2.menuId);

DELETE FROM menus where menuId IN (2,3,4,5,6);  
DELETE FROM menus where menuId = 1;  

INSERT INTO menus VALUES(1, 'Dashboards', NULL, 'assets/Assets/Whendonothoverover/home-normal-1.svg', 'assets/Assets/Whendohoverover/home-active-1.svg', NULL, TRUE, TRUE, TRUE, 1);
INSERT INTO menus VALUES(25, 'Summary-D3', '/summarydashboardD3', NULL, NULL, 1, TRUE, TRUE, FALSE, 6);

UPDATE menus SET parentMenuId=1, menuHierachy=7 WHERE menuId=22;
UPDATE menus SET parentMenuId=1, menuHierachy=8 WHERE menuId=24;


INSERT INTO menus VALUES(26, 'Inventory-D3', '/inventorydashboardD3', NULL, NULL, 1, TRUE, TRUE, FALSE, 9);
