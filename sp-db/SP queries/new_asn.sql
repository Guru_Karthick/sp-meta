CREATE TABLE new_asn (
    asnId INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    storeId INT UNSIGNED NOT NULL,
    asnNumber VARCHAR(32)BINARY NOT NULL,
    asnStatus ENUM('FRESH', 'CONFIRMED', 'FAILED', 'CANCELLED', 'AMENDED_FRESH', 'AMENDED_CONFIRMED', 'AMENDMENT_FAILED', 'CLOSED') NOT NULL DEFAULT 'FRESH',
    isIncomplete BOOLEAN NOT NULL DEFAULT FALSE,
    createdBy VARCHAR(32)BINARY NOT NULL,
    createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modifiedBy VARCHAR(32)BINARY NOT NULL,
    modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    UNIQUE (asnNumber),
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);

select * from new_asn;
desc new_asn;
drop table new_asn;
delete from new_asn;
truncate table new_asn;


alter table new_asn_details modify column 

alter table new_asn add column reasonForRejection varchar(256) null after asnStatus;

select T1.*, T2.* from new_asn T1 join new_asnDetails T2 on T1.asnId = T2.asnId;

SELECT * FROM new_asn T1 JOIN (SELECT * FROM new_asndetails UNION ALL SELECT * FROM new_asndetailstemp) T2 ON T1.asnId = T2.asnId
WHERE T1.storeId = 1
ORDER BY T1.modifiedOn DESC LIMIT 50 OFFSET 0;

alter table new_asn change column isIncomplete isSubmitted BOOLEAN NOT NULL DEFAULT TRUE;

update new_asn set isIncomplete = true where asnId in (select asnId from new_asndetailstemp);
alter table new_asn drop column isIncomplete;

 SELECT T1.asnId, asnId FROM new_asn T1 JOIN (SELECT asnId, amendmentNumber, asnDateTime, asnSource, deliveryMode, shipmentDate, originatingLocation, destinationLocation, appointmentNumber, sealNumber, referenceDocumentNumber, vendorNumber, vendorName, vendorAsnNumber, asnSentBy FROM new_asnDetails
 UNION ALL SELECT asnId, amendmentNumber, asnDateTime, asnSource, deliveryMode, shipmentDate, originatingLocation, destinationLocation, appointmentNumber, sealNumber, referenceDocumentNumber, vendorNumber, vendorName, vendorAsnNumber, asnSentBy FROM new_asnDetailsTemp) T2 ON T1.asnId = T2.asnId WHERE T1.storeId = 1 ORDER BY asnNumber ASC LIMIT 100 OFFSET 0;
 
  SELECT * FROM new_asn T1 JOIN (SELECT * FROM new_asnDetails UNION ALL SELECT * FROM new_asnDetailsTemp) T2 ON T1.asnId = T2.asnId WHERE T1.storeId = 1 LIMIT 100 OFFSET 0;


 SELECT asnId, amendmentNumber, asnDateTime, asnSource, deliveryMode, shipmentDate, originatingLocation, destinationLocation, appointmentNumber, sealNumber, referenceDocumentNumber, vendorNumber, vendorName, vendorAsnNumber, asnSentBy FROM new_asnDetails
 UNION ALL SELECT asnId, amendmentNumber, asnDateTime, asnSource, deliveryMode, shipmentDate, originatingLocation, destinationLocation, appointmentNumber, sealNumber, referenceDocumentNumber, vendorNumber, vendorName, vendorAsnNumber, asnSentBy FROM new_asnDetailsTemp;