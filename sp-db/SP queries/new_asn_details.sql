CREATE TABLE new_asn_details (
    id INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    asnId INT UNSIGNED NOT NULL,
    amendmentNumber TINYINT NOT NULL DEFAULT 0,
    asnDateTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    asnSource ENUM('SELLER_PORTAL', 'BULK_UPLOAD', 'SELLER_SYSTEMS', 'WMS') NOT NULL DEFAULT 'SELLER_PORTAL',
    deliveryMode ENUM('SMALL_PARCEL', 'LTL', 'FTL') NOT NULL,
    originatingLocation VARCHAR(50) NOT NULL,
    destinationLocation ENUM('ATLANTA_DC') NOT NULL,
    appointmentNumber VARCHAR(20),
    sealNumber VARCHAR(20),
    shipmentDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    estimatedDeliveryDate TIMESTAMP NOT NULL,
    referenceDocumentType ENUM('ASN', 'PO') NOT NULL,
    referenceDocumentNumber VARCHAR(20) NOT NULL,
    referenceDocumentDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    vendorNumber VARCHAR(32),
    vendorName VARCHAR(60),
    vendorAsnNumber VARCHAR(32),
    vendorAsnDate TIMESTAMP NULL,
    asnSentBy VARCHAR(50) NOT NULL,
    remarks VARCHAR(255),
    carrierName VARCHAR(60) NOT NULL,
    shipVia VARCHAR(30),
    modeOfTransport ENUM('ROAD', 'SHIP', 'AIR') NOT NULL,
    vehicleType ENUM('TRUCK', 'CONTAINER') NULL,
    vehicleNumber VARCHAR(32) NULL,
    typeOfContainer ENUM('DRY_STORAGE_CONTAINER', 'FLAT_RACK_CONTAINER') NULL,
    driverName VARCHAR(50) NOT NULL,
    packagingOptions ENUM('PALLET', 'CASE') NOT NULL,
    trackingNumber VARCHAR(32) NOT NULL,
    billOfLadingNumber VARCHAR(32) NOT NULL,
    grossWeight decimal(12,2),
    nettWeight decimal(12,2),
    volume decimal(12,2),
    quarantineType ENUM('AQIS', 'IFIP'),
    quarantineStatus ENUM('YES', 'NO'),
    blockingStage ENUM('UNLOADING', 'PUTAWAY'),
    releaseNumber VARCHAR(32),
    releaseDate TIMESTAMP NULL,
    fromName VARCHAR(50) NOT NULL,
    fromAddress1 VARCHAR(50) NOT NULL,
    fromAddress2 VARCHAR(50) NOT NULL,
    fromCity VARCHAR(32) NOT NULL,
    fromState VARCHAR(32) NOT NULL,
    fromCountry VARCHAR(50) NOT NULL,
    fromZipCode VARCHAR(10) NOT NULL,
    fromPhoneNumber VARCHAR(16) NOT NULL,
    toName VARCHAR(50) NOT NULL,
    toAddress1 VARCHAR(50) NOT NULL,
    toAddress2 VARCHAR(50) NOT NULL,
    toCity VARCHAR(32) NOT NULL,
    toState VARCHAR(32) NOT NULL,
    toCountry VARCHAR(50) NOT NULL,
    toZipCode VARCHAR(10) NOT NULL,
    cartonizeOption ENUM('MANUAL', 'AUTO') NOT NULL,
    UNIQUE (asnId),
    FOREIGN KEY (asnId)
        REFERENCES new_asn (asnId)
);


select * from new_asn_details;
desc new_asn_details;
drop table new_asn_details;
delete from new_asn_details;
truncate table new_asn_details;

select * from new_asn_details where asnId = 215;



ALTER TABLE new_asn_details MODIFY COLUMN grossWeight DECIMAL(12,2);
ALTER TABLE new_asn_details MODIFY COLUMN nettWeight DECIMAL(12,2);
ALTER TABLE new_asn_details MODIFY COLUMN volume DECIMAL(12,2);

ALTER TABLE new_asn_details_temp MODIFY COLUMN grossWeight DECIMAL(12,2);
ALTER TABLE new_asn_details_temp MODIFY COLUMN nettWeight DECIMAL(12,2);
ALTER TABLE new_asn_details_temp MODIFY COLUMN volume DECIMAL(12,2);


alter table new_asndetails drop column asnStatus;

select * from new_asndetails, new_asndetailstemp where new_asndetails.asnId = 8;

select * from new_asndetails union all select * from new_asndetailstemp;

select * from new_asn T1 where T1.storeId = 1
and T1.asnId in (select asnId from (select * from new_asndetails union all select * from new_asndetailstemp where new_asndetailstemp.asnStatus = 'FRESH') as T2 where T2.originatingLocation like '%zz%');

select * from new_asn T1 join (select * from new_asndetails union all select * from new_asndetailstemp) T2 on T1.asnId = T2.asnId;

select T1.* from new_asn T1  join new_asndetails T2 on T1.asnId = T2.asnId join new_asndetailstemp T3 on T1.asnId = T3.asnId where T1.storeId = 1;