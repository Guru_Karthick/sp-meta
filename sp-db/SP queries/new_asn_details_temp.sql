CREATE TABLE new_asn_details_temp (
    id INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    asnId INT UNSIGNED NOT NULL,
    amendmentNumber TINYINT NOT NULL DEFAULT 0,
    asnDateTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    asnSource ENUM('SELLER_PORTAL', 'BULK_UPLOAD') NOT NULL DEFAULT 'SELLER_PORTAL',
    deliveryMode ENUM('SMALL_PARCEL', 'LTL', 'FTL') NOT NULL,
    originatingLocation VARCHAR(50) NULL,
    destinationLocation ENUM('ATLANTA_DC') NOT NULL,
    appointmentNumber VARCHAR(20) NULL,
    sealNumber VARCHAR(20) NULL,
    shipmentDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    estimatedDeliveryDate TIMESTAMP NOT NULL,
    referenceDocumentType ENUM('ASN', 'PO') NOT NULL,
    referenceDocumentNumber VARCHAR(20) NULL,
    referenceDocumentDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    vendorNumber VARCHAR(32) NULL,
    vendorName VARCHAR(60) NULL,
    vendorAsnNumber VARCHAR(32) NULL,
    vendorAsnDate TIMESTAMP NULL,
    asnSentBy VARCHAR(50) NOT NULL,
    remarks VARCHAR(255) NULL,
    carrierName VARCHAR(60) NULL,
    shipVia VARCHAR(30) NULL,
    modeOfTransport ENUM('ROAD', 'SHIP', 'AIR') NOT NULL,
    vehicleType ENUM('TRUCK', 'CONTAINER') NULL,
    vehicleNumber VARCHAR(32) NULL,
    typeOfContainer ENUM('DRY_STORAGE_CONTAINER', 'FLAT_RACK_CONTAINER') NULL,
    driverName VARCHAR(50) NULL,
    packagingOptions ENUM('PALLET', 'CASE') NOT NULL,
    trackingNumber VARCHAR(32) NULL,
    billOfLadingNumber VARCHAR(32) NULL,
    grossWeight DECIMAL(12 , 2 ) NULL,
    nettWeight DECIMAL(12 , 2 ) NULL,
    volume DECIMAL(12 , 2 ) NULL,
    quarantineType ENUM('AQIS', 'IFIP') NULL,
    quarantineStatus ENUM('YES', 'NO') NULL,
    blockingStage ENUM('UNLOADING', 'PUTAWAY') NULL,
    releaseNumber VARCHAR(32) NULL,
    releaseDate TIMESTAMP NULL,
    fromName VARCHAR(50) NULL,
    fromAddress1 VARCHAR(50) NULL,
    fromAddress2 VARCHAR(50) NULL,
    fromCity VARCHAR(32) NULL,
    fromState VARCHAR(32) NULL,
    fromCountry VARCHAR(50) NULL,
    fromZipCode VARCHAR(5) NULL,
    fromPhoneNumber VARCHAR(16) NULL,
    toName VARCHAR(50) NULL,
    toAddress1 VARCHAR(50) NULL,
    toAddress2 VARCHAR(50) NULL,
    toCity VARCHAR(32) NULL,
    toState VARCHAR(32) NULL,
    toCountry VARCHAR(50) NULL,
    toZipCode VARCHAR(5) NULL,
    cartonizeOption ENUM('MANUAL', 'AUTO') NOT NULL,
    UNIQUE (asnId),
    FOREIGN KEY (asnId)
        REFERENCES new_asn (asnId)
);

select * from new_asn_details_temp;
desc new_asn_details_temp;
drop table new_asn_details_temp;
delete from new_asn_details_temp;
truncate table new_asn_details_temp;

select * from new_asn_details_temp where asnId = 215;


alter table new_asn_details_temp drop column asnStatus;

select curdate();
select asnId from new_asn where asnId not in (select asnId from new_asn_details_temp);