CREATE TABLE new_asn_documents (
    id INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    asnId INT UNSIGNED NOT NULL,
    documentName VARCHAR(32) NOT NULL,
    documentPath VARCHAR(512) NOT NULL,
    UNIQUE (asnId , documentName),
    FOREIGN KEY (asnId)
        REFERENCES new_asn (asnId)
);

SELECT * FROM new_asn_documents;
DESC new_asn_documents;
DROP TABLE new_asn_documents;
DELETE FROM new_asn_documents;
truncate table new_asn_documents;

SELECT * FROM new_asn_documents where id = 10;

delete from new_asn_documents where id = 1;