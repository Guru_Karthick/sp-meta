CREATE TABLE new_asn_packaging_info (
    id INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    asnId INT UNSIGNED NOT NULL,
    productId INT UNSIGNED NOT NULL,
    palletLpn VARCHAR(32) NULL,
    caseLpn VARCHAR(32) NOT NULL,
    skuQuantity SMALLINT UNSIGNED NOT NULL,
    batchNumber VARCHAR(32) NOT NULL,
    UNIQUE (asnId , palletLpn),
    UNIQUE (asnId , caseLpn),
    FOREIGN KEY (asnId , productId)
        REFERENCES new_asn_sku_info (asnId , productId)
        ON DELETE CASCADE
);

select * from new_asn_packaging_info;
desc new_asn_packaging_info;
drop table new_asn_packaging_info;
delete from new_asn_packaging_info;
truncate table new_asn_packaging_info;

select * from new_asn_packaging_info where asnId = 44;

insert into new_asn_packaging_info ( asnId, productId, palletLpn, caseLpn, skuQuantity, batchNumber) values(3, 2, 'P2', 'C2', 10, 'BNO');

insert into new_asn_packaging_info ( asnId, productId, palletLpn, caseLpn, skuQuantity, batchNumber) values(5, 99, null, 'Case1124', 10, 'BNO')
on duplicate key update skuQuantity = 10;