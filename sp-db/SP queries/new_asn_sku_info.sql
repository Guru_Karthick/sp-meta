CREATE TABLE new_asn_sku_info (
    id INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    asnId INT UNSIGNED NOT NULL,
    lineNumber SMALLINT UNSIGNED NOT NULL,
    productId INT UNSIGNED NOT NULL,
    productStatus ENUM('NEW', 'REFURBISHED', 'DAMAGED') NOT NULL,
    shippedUnits INT UNSIGNED NOT NULL,
    skuQuantityPerCase SMALLINT UNSIGNED NOT NULL,
    caseQuantityPerPallet TINYINT UNSIGNED NULL,
    countryOfOrigin VARCHAR(50) NOT NULL,
    vasCode VARCHAR(32) NULL,
    UNIQUE (asnId , lineNumber),
    UNIQUE (asnId , productId),
    FOREIGN KEY (asnId)
        REFERENCES new_asn (asnId),
    FOREIGN KEY (productId)
        REFERENCES productmapper (product_id)
);

select * from new_asn_sku_info;
desc new_asn_sku_info;
drop table new_asn_sku_info;
delete from new_asn_sku_info where asnId = 44;
truncate table new_asn_sku_info;

select * from new_asn_sku_info where asnId = 44;

insert into new_asn_sku_info (asnId, lineNumber, productId, productStatus, shippedUnits, skuQuantityPerCase, caseQuantityPerPallet, countryOfOrigin, vasCode)
values()