CREATE TABLE notification_preferences (
    storeId INT UNSIGNED NOT NULL,
    notificationCategory ENUM('LOW_STOCK', 'DISTRESSED_STOCK') NOT NULL,
    notificationType ENUM('EMAIL', 'PHONE', 'BOTH') NOT NULL,
	emailIds JSON,
	phoneNumbers JSON,
    notificationEnabled BOOLEAN NOT NULL DEFAULT FALSE,
    UNIQUE (storeId , notificationCategory),
    PRIMARY KEY (storeId , notificationCategory),
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);

select * from notification_preferences;