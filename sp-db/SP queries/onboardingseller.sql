CREATE TABLE onboardingseller (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(64) NOT NULL,
    email VARCHAR(64) NOT NULL,
    storeName VARCHAR(64) NOT NULL,
    revenue VARCHAR(64) NOT NULL,
    interestedIn VARCHAR(64) NOT NULL,
    maximumProductSku INT UNSIGNED NOT NULL,
    address VARCHAR(80) NOT NULL,
    city VARCHAR(32) NOT NULL,
    state VARCHAR(32) NOT NULL,
    country VARCHAR(32) NOT NULL,
    zipCode VARCHAR(5) NOT NULL,
    phoneNumber VARCHAR(16) NOT NULL,
    modeOfContact VARCHAR(8),
    preference VARCHAR(64) NOT NULL,
    currentSellingMode VARCHAR(256) NOT NULL,
    bankLocation VARCHAR(256) NOT NULL,
    accountHolderName VARCHAR(256) NOT NULL,
    routingNumber VARCHAR(9) NOT NULL,
    accountNumber VARCHAR(17) NOT NULL,
    incomeReceiver ENUM('Individuals/Sales Proprietor', 'Business') NOT NULL,
    usCitizen BOOLEAN NOT NULL,
    taxName VARCHAR(256) NOT NULL,
    tradeName VARCHAR(200) NOT NULL,
    taxAddress VARCHAR(512) NOT NULL,
    taxClassification ENUM('S Corporation', 'C Corporation', 'Partnership', 'Trust/Estate', 'Limited Liability Company', 'Others') NOT NULL,
    taxpayerIdentificationType ENUM('SSN or TIN Number', 'EIN Number') NOT NULL,
    taxpayerIdentificationNumber VARCHAR(9) NOT NULL,
    needElectronicSignature BOOLEAN NOT NULL,
    sellingOnOtherMarketPlace BOOLEAN NOT NULL,
    sellingOn ENUM('amazon', 'ebay', 'both') DEFAULT NULL,
    ebaySellingCategory VARCHAR(64) DEFAULT NULL,
    allowOffersOnEbay BOOLEAN,
    allowAutomaticFeedback BOOLEAN,
    needFulfillmentForEbay BOOLEAN,
    paymentType VARCHAR(64),
    shippingMethodForEbay VARCHAR(64),
    amazonSellingCategory VARCHAR(64),
    needProductListing ENUM('Retain Amazon existing listing only', 'Retain and Add new Product as well') DEFAULT 'Retain Amazon existing listing only',
    needFulfillmentForAmazon ENUM('Fulfillment by Vert', 'Fulfillment by Seller', 'Third Party Fulfillment') DEFAULT 'Fulfillment by Vert',
    shippingMethodForAmazon VARCHAR(64),
    obtainFBAandFBMForAmazon BOOLEAN,
    productSource ENUM('I manufacture them', 'I resell Products that I Purchase', 'I sell products manufactured by me', 'I dont know'),
    productIdentifier ENUM('All of my products have them', 'Some of my products have them', 'None of my products have them', 'I dont know'),
    productCount ENUM('1-10', '11-100', '101-500', 'More than 500', 'I dont know'),
    webAddressAvailable BOOLEAN DEFAULT NULL,
    webAddress VARCHAR(64),
    mainProductCategory VARCHAR(64),
    ssnDocumentName VARCHAR(32),
    ssnDocumentPath VARCHAR(512),
    govIdDocumentName VARCHAR(32),
    govIdDocumentPath VARCHAR(512),
    bankDocumentName VARCHAR(32),
    bankDocumentPath VARCHAR(512)
);

select * from onboardingseller;
desc onboardingseller;
drop table onboardingseller;
delete from onboardingseller;
truncate table onboardingseller;

ALTER TABLE onboardingseller ADD COLUMN processed BOOLEAN NOT NULL DEFAULT 0 AFTER bankDocumentPath;
ALTER TABLE onboardingseller MODIFY COLUMN email VARCHAR(80) NOT NULL;

ALTER TABLE onboardingseller MODIFY COLUMN revenue VARCHAR(32) NOT NULL;
ALTER TABLE onboardingseller MODIFY COLUMN preference VARCHAR(32) NOT NULL;

ALTER TABLE contactvert MODIFY COLUMN revenue VARCHAR(32);

ALTER TABLE contactvert MODIFY COLUMN sellingMode VARCHAR(64);
alter table onboardingseller MODIFY COLUMN currentSellingMode VARCHAR(64) NOT NULL;

ALTER TABLE onboardingseller MODIFY productSource ENUM('I manufacture them', 'I resell Products that I Purchase', 'I sell products manufactured by me', 'I dont know') NOT NULL ;
ALTER TABLE onboardingseller MODIFY productIdentifier ENUM('All of my products have them', 'Some of my products have them', 'None of my products have them', 'I dont know') NOT NULL ;
ALTER TABLE onboardingseller MODIFY productCount ENUM('1-10', '11-100', '101-500', 'More than 500', 'I dont know') NOT NULL ;
ALTER TABLE onboardingseller MODIFY webAddressAvailable BOOLEAN NOT NULL ;
ALTER TABLE onboardingseller MODIFY mainProductCategory VARCHAR(64) NOT NULL ;


ALTER TABLE onboardingseller MODIFY COLUMN maximumProductSku INT UNSIGNED ;

ALTER TABLE onboardingseller MODIFY COLUMN preference VARCHAR(64);
ALTER TABLE onboardingseller MODIFY incomeReceiver ENUM('Individuals/Sales Proprietor','Individuals/Sole Proprietor', 'Business') NOT NULL;
SET SQL_SAFE_UPDATES = 0;
Update onboardingseller set incomeReceiver = 'Individuals/Sole Proprietor' where incomeReceiver = 'Individuals/Sales Proprietor';
ALTER TABLE onboardingseller MODIFY incomeReceiver ENUM('Individuals/Sole Proprietor', 'Business') NOT NULL;

ALTER TABLE sellertaxdetails MODIFY incomeReceiver ENUM('Individuals/Sales Proprietor','Individuals/Sole Proprietor', 'Business') NOT NULL;
Update sellertaxdetails set incomeReceiver = 'Individuals/Sole Proprietor' where incomeReceiver = 'Individuals/Sales Proprietor';
ALTER TABLE sellertaxdetails MODIFY incomeReceiver ENUM('Individuals/Sole Proprietor', 'Business') NOT NULL;

ALTER TABLE onboardingseller MODIFY needFulfillmentForAmazon ENUM('Fulfillment by Vert','Fulfillment by Verte', 'Fulfillment by Seller', 'Third Party Fulfillment');
Update onboardingseller set needFulfillmentForAmazon = 'Fulfillment by Verte' where needFulfillmentForAmazon = 'Fulfillment by Vert';
ALTER TABLE onboardingseller MODIFY needFulfillmentForAmazon ENUM('Fulfillment by Verte', 'Fulfillment by Seller', 'Third Party Fulfillment');

ALTER TABLE sellerquestionnaire MODIFY needFulfillmentForAmazon ENUM('Fulfillment by Vert','Fulfillment by Verte', 'Fulfillment by Seller', 'Third Party Fulfillment');
Update sellerquestionnaire set needFulfillmentForAmazon = 'Fulfillment by Verte' where needFulfillmentForAmazon = 'Fulfillment by Vert';
ALTER TABLE sellerquestionnaire MODIFY needFulfillmentForAmazon ENUM('Fulfillment by Verte', 'Fulfillment by Seller', 'Third Party Fulfillment');

ALTER TABLE sellerquestionnaire MODIFY productSource ENUM('I manufacture them', 'I purchase them in USA', 'I import them from other countries', 'I resell Products that I Purchase', 'I sell products manufactured by me (private label)') NOT NULL ;
update sellerquestionnaire set productSource = 'I purchase them in USA' where productSource = 'I resell Products that I Purchase';
update sellerquestionnaire set productSource = 'I import them from other countries' WHERE productSource =  'I sell products manufactured by me (private label)';
ALTER TABLE sellerquestionnaire MODIFY productSource ENUM('I manufacture them', 'I purchase them in USA', 'I import them from other countries') NOT NULL ;