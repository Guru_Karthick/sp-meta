CREATE TABLE order_shipment (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY,
    shipmentNumber VARCHAR(32) NOT NULL UNIQUE,
    shipEngineId VARCHAR(12) UNIQUE,
    orderId VARCHAR(32) NOT NULL,
    storeId INT UNSIGNED NOT NULL,
    shipmentDate TIMESTAMP NOT NULL,
    isCustomShipment BOOLEAN NOT NULL,
    carrierId VARCHAR(12),
    carrierCode VARCHAR(32),
    serviceCode VARCHAR(64),
    trackingNumber VARCHAR(32),
    from_name VARCHAR(80) NOT NULL,
    from_phoneNumber VARCHAR(16) NOT NULL,
    from_address1 VARCHAR(80) NOT NULL,
    from_address2 VARCHAR(80) NOT NULL,
    from_address3 VARCHAR(80) NOT NULL,
    from_city VARCHAR(32) NOT NULL,
    from_state VARCHAR(32) NOT NULL,
    from_country VARCHAR(32) NOT NULL,
    from_zipCode VARCHAR(5) NOT NULL,
    labelId VARCHAR(24),
    labelHrefUrl VARCHAR(512),
    deliveryPersonName VARCHAR(64),
    deliveryPersonPhone VARCHAR(16),
    deliveryPersonVehicleNumber VARCHAR(16),
    createdBy VARCHAR(32) NOT NULL,
    createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modifiedBy VARCHAR(32) NOT NULL,
    modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (orderId , storeId)
        REFERENCES orders (orderId , storeId)
);
ALTER TABLE order_shipment ADD COLUMN shipmentStatus ENUM('PROCESSING') NOT NULL DEFAULT 'PROCESSING' AFTER storeId;

ALTER TABLE order_shipment MODIFY COLUMN shipmentStatus enum('PROCESSING','DELIVERED','FAILED_TO_DELIVER') NOT NULL DEFAULT 'PROCESSING';


select * from order_shipment;
desc order_shipment;

drop table order_shipment;

select count(*) from order_shipment where orderId = 'LASTOFUS01' AND storeId = 1 AND shipmentStatus = 'PROCESSING';

select * from orderitem where orderId = 'LASTOFUS03' AND storeId = 1 AND quantity = shippedQuantity;