CREATE TABLE order_shipment_package (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY,
    packageNumber VARCHAR(32) NOT NULL UNIQUE,
    shipmentId INT UNSIGNED NOT NULL,
    packageCode VARCHAR(64),
    weight DECIMAL(12 , 2 ),
    length DECIMAL(12 , 2 ),
    width DECIMAL(12 , 2 ),
    height DECIMAL(12 , 2 ),
    estimatedShipingCost DECIMAL(6 , 2 ),
    estimatedDeliveryDate TIMESTAMP,
    productId INT UNSIGNED NOT NULL,
    itemQuantity INT UNSIGNED NOT NULL,
    FOREIGN KEY (shipmentId)
        REFERENCES order_shipment (id),
    FOREIGN KEY (productId)
        REFERENCES orderitem (product_id)
);

drop table order_shipment_package;