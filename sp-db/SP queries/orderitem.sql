CREATE TABLE orderitem (
    orderId VARCHAR(32) NOT NULL,
    storeId INT UNSIGNED NOT NULL,
    product_id INT UNSIGNED NOT NULL,
    quantity INT UNSIGNED NOT NULL,
    price FLOAT UNSIGNED NOT NULL,
    PRIMARY KEY (orderId , storeId , product_id),
    FOREIGN KEY (orderId , storeId)
        REFERENCES orders (orderId , storeId),
    FOREIGN KEY (product_id)
        REFERENCES productmapper (product_id)
);

select * from orderitem;
desc orderitem;
drop table orderitem1;
delete from orderitem;
truncate table orderitem;

select distinct orderId from orderitem;


alter table orderitem add column unitPrice FLOAT UNSIGNED NOT NULL after quantity;
alter table orderitem add column vasCode VARCHAR(16) after price;
alter table orderitem add column vasPrice FLOAT UNSIGNED after vasCode;
alter table orderitem add column estimatedDeliveryDate TIMESTAMP DEFAULT 0 NULL after vasPrice;
alter table orderitem add column additionalField VARCHAR(16) after estimatedDeliveryDate;
alter table orderitem modify estimatedDeliveryDate TIMESTAMP DEFAULT NULL NULL;
