CREATE TABLE orderreturn (
    orderReturnId VARCHAR(32) NOT NULL,
    orderId VARCHAR(32) NOT NULL,
    storeId INT UNSIGNED NOT NULL,
    product_id INT UNSIGNED NOT NULL,
    returnedQuantity INT UNSIGNED NOT NULL,
    returnStatus VARCHAR(32) NOT NULL,
    refundAmount FLOAT UNSIGNED NOT NULL,
    returnRequestedOn TIMESTAMP NOT NULL DEFAULT now(),
    reasonForReturn VARCHAR(1024) NOT NULL,
    additionalComments VARCHAR(1024),
    PRIMARY KEY (orderReturnId , orderId , storeId),
    FOREIGN KEY (orderId , storeId , product_id)
        REFERENCES orderlist (orderId , storeId , product_id)
);
     
select * from orderreturn;
desc orderreturn;
drop table orderreturn;
delete from orderreturn;
truncate table orderreturn;

insert into orderreturn values('RET001_ORD002', 'ORD002', 1, 32, 3, 'Initiated', 100.00, now(), 'Not a good product', 'CoMMENT');