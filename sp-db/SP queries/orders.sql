CREATE TABLE orders (
    orderId VARCHAR(32) NOT NULL,
    storeId INT UNSIGNED NOT NULL,
    orderStatus ENUM('Processing', 'Partially Shipped', 'Shipped', 'Delivered', 'Cancelled') DEFAULT 'Processing',
    source VARCHAR(64) NOT NULL,
    sourceCode VARCHAR(16) NOT NULL,
    orderSubtotal FLOAT UNSIGNED NOT NULL,
    orderTax FLOAT UNSIGNED NOT NULL,
    orderShippingcharges FLOAT UNSIGNED NOT NULL,
    orderTotal FLOAT UNSIGNED NOT NULL,
    createdOn TIMESTAMP NOT NULL DEFAULT 0,
    lastStatusUpdate TIMESTAMP NOT NULL DEFAULT 0,
    PRIMARY KEY (orderId, storeId),
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);


select * from orders;
desc orders;
drop table orders1;
delete from orders;
truncate table orders;


insert into orders 
values('ORD001', 2, 'PROCESSING','Vert', 'VE-01', 10000, 500, 1500, 12000,  now(), now());


SELECT T1.* FROM orderreturn T1 JOIN productvaluemaster T2 ON T1.product_id = T2.product_id
WHERE T1.storeId = 1 AND T2.field_id = 5  AND (T1.orderId LIKE '%feviquick%' OR T1.orderReturnId LIKE '%feviquick%' OR T2.value LIKE '%feviquick%');

select * from orderreturn;
select * from orderreturn T1 join productvaluemaster T2 on T1.product_id = T2.product_id
where T1.storeId = 1 and T2.field_id = 5;

select distinct T1.orderId, T3.customerName, sum(T2.quantity), T1.orderTotal
from orders T1 join orderlist T2 on T1.orderId = T2.orderId join customers T3 on T1.orderId = T3.orderId
group by T1.createdOn DESC, T2.orderId LIMIT 15;

select T2.value as product_name, sum(T1.quantity) as TotalQuantity, sum(T1.quantity * T1.price) as TotalPrice
from orderlist T1 JOIN productvaluemaster T2 on T1.product_id = T2.product_id
where T1.storeId = 1 and T2.field_id = 5
group by T1.product_id  order by TotalQuantity desc limit 20;

SELECT T1.orderId,  T3.customerName ,SUM(T2.quantity), T1.orderTotal
FROM orders T1 JOIN orderlist T2 ON T1.orderId = T2.orderId JOIN  customers T3 ON T1.orderId = T3.orderId
GROUP BY T1.createdOn DESC, T2.orderId LIMIT 25 ;

SELECT COUNT(*) AS ordercount FROM orders WHERE storeId = 1 AND createdOn > now()-(30 * 24 * 60 * 60 * 1000);


alter table orders add column orderType VARCHAR(16) NOT NULL after sourceCode;
alter table orders add column paymentCurrency VARCHAR(6) after orderType;
alter table orders add column paymentMethod VARCHAR(16) after paymentCurrency;
alter table orders add column giftCard VARCHAR(16) after paymentMethod;
alter table orders add column giftCardCurrency VARCHAR(6) after giftCard;
alter table orders add column estimatedDeliveryDate TIMESTAMP NOT NULL DEFAULT 0 after giftCardCurrency;
alter table orders add column vasCode VARCHAR(16) after estimatedDeliveryDate;
alter table orders add column vasPrice FLOAT UNSIGNED after vasCode;

alter table orders add column buyerId INT unsigned after vasPrice;

alter table orders add column billing_customerName VARCHAR(80) after buyerId;
alter table orders add column billing_email VARCHAR(80) after billing_customerName;
alter table orders add column billing_phoneNumber VARCHAR(16) after billing_email;
alter table orders add column billing_address1 VARCHAR(80) after billing_phoneNumber;
alter table orders add column billing_address2 VARCHAR(80) after billing_address1;
alter table orders add column billing_address3 VARCHAR(80) after billing_address2;
alter table orders add column billing_city VARCHAR(32) after billing_address3;
alter table orders add column billing_state VARCHAR(32) after billing_city;
alter table orders add column billing_country VARCHAR(32) after billing_state;
alter table orders add column billing_zipCode VARCHAR(5) after billing_country;

alter table orders add column shipping_customerName VARCHAR(80) after billing_zipCode;
alter table orders add column shipping_phoneNumber VARCHAR(16) after shipping_customerName;
alter table orders add column shipping_address1 VARCHAR(80) after shipping_phoneNumber;
alter table orders add column shipping_address2 VARCHAR(80) after shipping_address1;
alter table orders add column shipping_address3 VARCHAR(80) after shipping_address2;
alter table orders add column shipping_city VARCHAR(32) after shipping_address3;
alter table orders add column shipping_state VARCHAR(32) after shipping_city;
alter table orders add column shipping_country VARCHAR(32) after shipping_state;
alter table orders add column shipping_zipCode VARCHAR(5) after shipping_country;

alter table orders add column additionalField VARCHAR(16) after lastStatusUpdate;

alter table orders add constraint foreign key (buyerId) references buyer (buyerId);


alter table orders modify estimatedDeliveryDate  TIMESTAMP NULL DEFAULT NULL;

SELECT * FROM orders WHERE orderStatus IN ('Processing','Partially Shipped');
SELECT T1.* FROM orderitem T1 JOIN orders T2 on T1.orderId = T2.orderId WHERE T2.orderStatus IN ('Processing','Partially Shipped');
SELECT T1.* FROM buyer T1 JOIN orders T2 on T1.buyerId = T2.buyerId WHERE T2.orderStatus IN ('Processing','Partially Shipped');

SELECT * FROM buyer WHERE buyerId IN ( SELECT distinct(T1.buyerId) FROM buyer T1 JOIN orders T2 on T1.buyerId = T2.buyerId WHERE T2.orderStatus IN ('Processing','Partially Shipped'));