CREATE TABLE orders_file_upload_info (
    documentId INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    storeId INT UNSIGNED NOT NULL,
    documentName VARCHAR(32) NOT NULL,
    documentPath VARCHAR(512) NOT NULL,
    documentStatus ENUM('PROCESSING', 'SUCCESS', 'ERROR') NOT NULL,
    uploadedBy VARCHAR(32) NOT NULL,
    uploadedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    processCompletedOn TIMESTAMP NULL DEFAULT NULL,
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);

SELECT * FROM orders_file_upload_info;
DESC orders_file_upload_info;
DROP TABLE orders_file_upload_info;
DELETE FROM orders_file_upload_info;
truncate table orders_file_upload_info;
