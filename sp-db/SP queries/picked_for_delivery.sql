CREATE TABLE picked_for_delivery (
    deliveryId INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    deilveryBoyId INT(10) UNSIGNED NOT NULL,
    shipmentId INT(10) UNSIGNED NOT NULL UNIQUE,
    deliveryStatus ENUM('PENDING', 'DELIVERED', 'FAILED_TO_DELIVER') NOT NULL DEFAULT 'PENDING',
    noOfAttempts TINYINT UNSIGNED NOT NULL DEFAULT 0,
    failureReason VARCHAR(256),
    FOREIGN KEY (deilveryBoyId)
        REFERENCES users (sellerId),
    FOREIGN KEY (shipmentId)
        REFERENCES order_shipment (id)
);

desc picked_for_delivery;

select * from picked_for_delivery;

select * from menus;

INSERT INTO menus VALUES(1100, 'Delivery', NULL, 'assets/Assets/Whendonothoverover/home-normal-1.svg', 'assets/Assets/Whendohoverover/home-active-1.svg', NULL, TRUE, TRUE, TRUE, 110);
INSERT INTO menus VALUES(1101, 'Manage Delivery', '/manage-delivery', NULL, NULL, 1100, TRUE, TRUE, FALSE, 1);
INSERT INTO menus VALUES(1102, 'My Tasks', '/mytasks', NULL, NULL, 1100, TRUE, TRUE, FALSE, 2);
