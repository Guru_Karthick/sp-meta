CREATE TABLE productcategory (
    categoryId INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    categoryName VARCHAR(64) NOT NULL,
    categoryLevel ENUM('PARENT', 'CHILD', 'SUB') NOT NULL,
    parentId INT UNSIGNED NOT NULL,
    UNIQUE (categoryName , parentId)
);
show tables;
show create table productcategory;

select * from productcategory;
desc productcategory;
drop table productcategory;
delete from productcategory;
truncate table productcategory;

select * from orders;

insert into productcategory values(1,'Bags&Luggage','PARENT',NULL);
insert into productcategory values(2,'Backpacks','CHILD',1);
insert into productcategory values(3,'Suitcases','CHILD',1);
insert into productcategory values(4,'Wallets','CHILD',1);
insert into productcategory values(5,'CasualBackpacks','SUB',2);
insert into productcategory values(6,'LaptopBackpacks','SUB',2);
insert into productcategory values(7,'CameraBackpacks','SUB',3);
insert into productcategory values(8,'WomensWallet','SUB',4);
insert into productcategory values(9,'Books','PARENT',NULL);

SELECT * FROM productcategory WHERE  categoryLevel= 'PARENT';


SELECT * FROM productcategory
WHERE productcategory.parentId = productcategory.categoryId and categoryLevel = 'CHILD';
  
insert into productcategory values(9,'Books','PARENT',NULL) on duplicate key update categoryId = 1;