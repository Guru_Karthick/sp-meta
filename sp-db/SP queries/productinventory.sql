CREATE TABLE productinventory (
    product_id INT UNSIGNED NOT NULL UNIQUE,
    availableToPromise INT UNSIGNED NOT NULL,
    fulfillableStock INT UNSIGNED NOT NULL,
    damagedStock INT UNSIGNED NOT NULL,
    reservedStock INT UNSIGNED NOT NULL,
    updatedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (product_id),
    FOREIGN KEY (product_id)
        REFERENCES productmapper (product_id)
);

alter table productinventory
add column updatedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;

SELECT * FROM productinventory;
DESC productinventory;
DROP TABLE productinventory;
DELETE FROM productinventory;
truncate table productinventory;

update productinventory set availableToPromise = 200 ,fulfillableStock = 160 where product_id = 66;

DELETE FROM productinventory where product_id = 185;


INSERT INTO productinventory (product_id, availableToPromise, fulfillableStock, damagedStock, reservedStock)
SELECT product_id, 0, 0, 0, 0 FROM productmapper
WHERE product_id NOT IN (SELECT product_id FROM productinventory);