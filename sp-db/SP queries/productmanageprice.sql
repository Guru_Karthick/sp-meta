CREATE TABLE productmanageprice (
    product_id INT UNSIGNED NOT NULL,
    price FLOAT UNSIGNED NOT NULL,
    scheduled_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (product_id , scheduled_at),
    UNIQUE (product_id , scheduled_at),
    FOREIGN KEY (product_id)
        REFERENCES productmapper (product_id)
);

SELECT * FROM productmanageprice;
DESC productmanageprice;
DROP TABLE productmanageprice;
DELETE FROM productmanageprice;
TRUNCATE TABLE productmanageprice;

DELETE FROM productmanageprice where product_id = 185;

INSERT INTO productmanageprice (product_id, price, scheduled_at)
SELECT product_id, 0, 0 FROM productmapper;

UPDATE productmanageprice
INNER JOIN productvaluemaster ON productmanageprice.product_id = productvaluemaster.product_id
SET productmanageprice.price = CAST(productvaluemaster.value AS DECIMAL(10,2))
WHERE productvaluemaster.field_id = 9;

UPDATE productmanageprice
INNER JOIN productvaluemaster ON productmanageprice.product_id = productvaluemaster.product_id
SET productmanageprice.scheduled_at = FROM_UNIXTIME(CAST(productvaluemaster.value AS DECIMAL(15))/1000)
WHERE productvaluemaster.field_id = 74;


SELECT * FROM productmanageprice WHERE scheduled_at >= NOW() ORDER BY scheduled_at ASC LIMIT 10;

INSERT INTO productmanageprice (product_id, price, scheduled_at) values (67, 1290, '2018-07-25');
INSERT INTO productmanageprice (product_id, price, scheduled_at) values (67, 1200, now());