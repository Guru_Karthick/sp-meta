CREATE TABLE productmapper (
    product_id INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT,
    sku VARCHAR(64)BINARY NOT NULL,
    storeId INT UNSIGNED NOT NULL,
    PRIMARY KEY (product_id),
    UNIQUE (sku , storeId),
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);
alter table productmapper auto_increment = 1;
use sellerportal;
alter table productmapper 
change sku sku VARCHAR(64)BINARY NOT NULL;

SELECT * FROM productmapper;
DESC productmapper;
DROP TABLE productmapper;
DELETE FROM productmapper;
truncate table productmapper;

SELECT * FROM productmapper where storeId = 2 order by sku;
insert into productmapper (sku) values (3000);
DELETE FROM productmapper WHERE product_id = 60;
show tables;

SELECT * FROM productmapper order by product_id;
SELECT DISTINCT product_id FROM productmapper order by product_id;


ALTER TABLE productmapper ADD COLUMN 
productStatus ENUM('Processing', 'Accepted', 'Rejected') DEFAULT 'Processing' NOT NULL AFTER storeId;

ALTER TABLE productmapper ADD COLUMN 
errorDescription VARCHAR(128) NULL AFTER productStatus;

ALTER TABLE productmapper MODIFY
errorDescription VARCHAR(128) NULL;

delete from productinventory where product_id = 55;
delete from orderitem where product_id = 55;
select * from orderitem where product_id = 55;
delete from productmapper where product_id = 55;
