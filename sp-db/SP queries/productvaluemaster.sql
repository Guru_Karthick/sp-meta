CREATE TABLE productvaluemaster (
    product_id INT UNSIGNED NOT NULL,
    field_id INT UNSIGNED NOT NULL,
    value VARCHAR(2048),
    PRIMARY KEY (product_id , field_id),
    FOREIGN KEY (product_id)
        REFERENCES productmapper (product_id),
    FOREIGN KEY (field_id)
        REFERENCES fieldmaster (field_id)
);

SELECT DISTINCT(value), product_id FROM productvaluemaster where field_id = 75;

SELECT * FROM productvaluemaster;
DESC productvaluemaster;
DROP TABLE productvaluemaster;
DELETE FROM productvaluemaster;
truncate table productvaluemaster;

SELECT product_id, value FROM productvaluemaster  where field_id = 80 order by product_id;


select distinct product_id from productvaluemaster order by product_id;
SELECT DISTINCT product_id FROM productvaluemaster WHERE product_id IN (SELECT product_id FROM productvaluemaster WHERE value LIKE '%Puma%' AND field_id ='44');

SELECT DISTINCT product_id FROM productvaluemaster WHERE product_id IN (SELECT product_id FROM productvaluemaster WHERE value LIKE '%Bag%' AND field_id ='5');

SELECT * FROM productvaluemaster WHERE field_id = 48 order by value;
SELECT * FROM productvaluemaster WHERE product_id = 41 order by value;
select distinct(product_id) from productvaluemaster where value like '%f%' and product_id = 27 AND (field_id = 1 OR field_id = 2);

select * from productvaluemaster where value like '%n%';
select count(*) from productvaluemaster;

select count(*) from productvaluemaster
where field_id = 21;


select * from (select * from productvaluemaster 
where  product_id IN (select product_id from productvaluemaster P1 where P1.field_id = 80 AND P1.value = 'true')
order by CASE WHEN field_id = 1 THEN value END DESC) AS temp group by product_id;

select * from productvaluemaster 
where  product_id IN (select product_id from productvaluemaster P1 where P1.field_id = 80 AND P1.value = 'true') AND field_id = 74
order by CASE WHEN field_id = 74 THEN value END ASC;

select * from productvaluemaster 
where  product_id IN (select product_id from productvaluemaster P1 where P1.field_id = 80 AND P1.value = 'true')
order by product_id, value  ASC;


select * from productvaluemaster P1  group by  product_id order by CASE WHEN field_id = 1 THEN value END ASC;


select * from productvaluemaster 
where  product_id IN (143,142, 141);


SELECT * FROM productvaluemaster 
WHERE  product_id IN (SELECT product_id FROM productvaluemaster P1 WHERE P1.field_id = 80 AND P1.value = 'false') AND field_id = 74
ORDER BY CASE WHEN field_id = 74 THEN value END ASC;

(SELECT product_id FROM productvaluemaster P1 WHERE P1.field_id = 80 AND P1.value = 'false');

SELECT T1.product_id FROM productvaluemaster T1 JOIN productmapper T2 ON T1.product_id = T2.product_id
WHERE T2.storeId = 1 AND T2.productStatus = 'Processing' AND T1.field_id = 51 
ORDER BY CASE WHEN T1.field_id = 51 THEN value END ASC  LIMIT 1000 OFFSET 0;

SELECT T1.* FROM productvaluemaster T1 JOIN productmapper T2 ON T1.product_id = T2.product_id
WHERE T2.storeId = 1 AND T2.productStatus = 'Processing' AND T1.field_id = 1
ORDER BY CASE WHEN T1.field_id = 1 THEN value END ASC  LIMIT 100 OFFSET 0;

SELECT T1.product_id FROM productvaluemaster T1 JOIN productmapper T2 ON T1.product_id = T2.product_id
WHERE T2.storeId = 1 AND T1.field_id = 1 AND T2.productStatus = 'Processing' AND
T1.product_id IN (SELECT P1.product_id FROM productvaluemaster P1 WHERE P1.field_id = 80 AND P1.value = 'true')
ORDER BY CASE WHEN T1.field_id = 1 THEN value END ASC  LIMIT 150 OFFSET 0;
