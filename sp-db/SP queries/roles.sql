CREATE TABLE roles (
    roles_id INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT,
    name VARCHAR(20),
    storeId INT UNSIGNED NOT NULL,
    UNIQUE (name , storeId),
    PRIMARY KEY (roles_id),
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);
alter table roles auto_increment = 1;

select * from roles;
desc roles;
drop table roles;
delete from roles;
truncate table roles;

alter table roles
add column description varchar(512) not null after storeId;