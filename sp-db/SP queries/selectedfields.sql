CREATE TABLE selectedfields (
    sellerId INT UNSIGNED NOT NULL,
    field_id INT UNSIGNED NOT NULL,
    PRIMARY KEY (sellerId , field_id),
     FOREIGN KEY (sellerId)
        REFERENCES users (sellerId),
    FOREIGN KEY (field_id)
        REFERENCES fieldmaster (field_id)
);

SELECT * FROM selectedfields;
DESC selectedfields;
DROP TABLE selectedfields;
DELETE FROM selectedfields;
truncate table selectedfields;

SELECT * FROM selectedfields where sellerId = 1 ORDER BY listId ASC;
SELECT * FROM selectedfields order by sellerId;

insert into selectedfields values(1, 5);

ALTER TABLE selectedfields ADD COLUMN listId INT UNSIGNED NOT NULL AFTER field_id;