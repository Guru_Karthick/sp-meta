CREATE TABLE selectedsubvalues (
    sub_values_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    values_id INT UNSIGNED NOT NULL,
    feature VARCHAR(20) NOT NULL,
    selected INT UNSIGNED NOT NULL,
    none INT UNSIGNED NOT NULL,
    view INT UNSIGNED NOT NULL,
    view_edit INT UNSIGNED NOT NULL,
    PRIMARY KEY (sub_values_id),
    FOREIGN KEY (values_id)
        REFERENCES selectedvalues (values_id)
);
alter table selectedsubvalues auto_increment = 49;

select * from selectedsubvalues;
desc selectedsubvalues;
drop table selectedsubvalues;
delete from selectedsubvalues;
truncate table selectedsubvalues;

select * from selectedsubvalues where feature = 'Add Bulk Product';
select * from selectedsubvalues where feature = 'Add Single Product';
select * from selectedsubvalues where feature = 'Order Management';
select * from selectedsubvalues where feature = 'Return Management';

insert into selectedsubvalues (values_id, feature, selected, none, view, view_edit)
select values_id, 'Return Management', 1, 1, 1, 0 from selectedvalues
WHERE feature = 'Orders Dashboard' AND (values_id, 'Return Management') NOT IN (SELECT values_id, feature FROM selectedsubvalues);

insert into selectedsubvalues (values_id, feature, selected, none, view, view_edit)
select values_id, 'Order Management', 1, 1, 1, 0 from selectedvalues
WHERE feature = 'Orders Dashboard' AND (values_id, 'Order Management') NOT IN (SELECT values_id, feature FROM selectedsubvalues);

insert into selectedsubvalues (values_id, feature, selected, none, view, view_edit)
select values_id, 'Catalog Management', 1, 1, 1, 1 from selectedvalues
WHERE feature = 'Product Catalog' AND (values_id, 'Catalog Management') NOT IN (SELECT values_id, feature FROM selectedsubvalues);

insert into selectedsubvalues (values_id, feature, selected, none, view, view_edit)
select values_id, 'Pricing Management', 1, 1, 1, 1 from selectedvalues
WHERE feature = 'Product Catalog' AND (values_id, 'Pricing Management') NOT IN (SELECT values_id, feature FROM selectedsubvalues);