CREATE TABLE selectedvalues (
    values_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    roles_id INT UNSIGNED NOT NULL,
    feature VARCHAR(20) NOT NULL,
    selected INT UNSIGNED NOT NULL,
    none INT UNSIGNED NOT NULL,
    view INT UNSIGNED NOT NULL,
    view_edit INT UNSIGNED NOT NULL,
    PRIMARY KEY (values_id),
    FOREIGN KEY (roles_id)
        REFERENCES roles (roles_id)
);
alter table selectedvalues auto_increment = 1;

select * from selectedvalues;
desc selectedvalues;
drop table selectedvalues;
delete from selectedvalues;
truncate table selectedvalues;

select * from selectedvalues where roles_id = 17;
select * from selectedvalues where feature = 'Orders Dashboard';
select * from selectedvalues where feature = 'Product Catalog';

insert into selectedvalues(roles_id, feature, selected, none, view, view_edit) values (7, 'ASN', 1, 1, 1, 1);
insert into selectedvalues(roles_id, feature, selected, none, view, view_edit) values (7, 'Financial', 1, 1, 0, 0);
insert into selectedvalues(roles_id, feature, selected, none, view, view_edit) values (17, 'Financial', 1, 1, 0, 0);