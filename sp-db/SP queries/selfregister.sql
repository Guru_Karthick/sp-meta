CREATE TABLE selfregister (
  id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  uuid VARCHAR(36)BINARY NOT NULL UNIQUE,
  name VARCHAR(64) NOT NULL,
  email VARCHAR(80) NOT NULL,
  phoneNumber VARCHAR(16) NOT NULL,
  processed BOOLEAN DEFAULT 0
);

select * from selfregister;
desc selfregister;
drop table selfregister;
delete from selfregister;
truncate table selfregister;