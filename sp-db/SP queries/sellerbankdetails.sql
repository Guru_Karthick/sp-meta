CREATE TABLE sellerbankdetails (
  storeId INT UNSIGNED NOT NULL PRIMARY KEY,
  accountNumber VARCHAR(17) NOT NULL,
  accountHolderName VARCHAR(256) NOT NULL,
  bankLocation VARCHAR(256) NOT NULL,
  routingNumber VARCHAR(9) NOT NULL,
  FOREIGN KEY (storeId)
      REFERENCES stores (storeId)
);

SELECT * FROM sellerbankdetails;
DESC sellerbankdetails;
DROP TABLE sellerbankdetails;
DELETE FROM sellerbankdetails;
truncate table sellerbankdetails;