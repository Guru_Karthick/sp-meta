CREATE TABLE sellerdocuments (
    id INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    storeId INT UNSIGNED NOT NULL,
    documentName VARCHAR(32) NOT NULL,
    documentPath VARCHAR(512) NOT NULL,
    uploadedBy VARCHAR(32)BINARY NOT NULL,
    uploadedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (storeId , documentName),
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);

SELECT * FROM sellerdocuments;
DESC sellerdocuments;
DROP TABLE sellerdocuments;
DELETE FROM sellerdocuments;
truncate table sellerdocuments;
