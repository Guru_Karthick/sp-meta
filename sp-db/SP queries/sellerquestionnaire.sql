CREATE TABLE sellerquestionnaire (
  storeId INT UNSIGNED NOT NULL PRIMARY KEY,
  sellingOnOtherMarketPlace BOOLEAN NOT NULL,
  sellingOn enum('amazon','ebay','both') DEFAULT NULL,
  ebaySellingCategory varchar(64) DEFAULT NULL,
  allowOffersOnEbay BOOLEAN DEFAULT NULL,
  allowAutomaticFeedback BOOLEAN DEFAULT NULL,
  needFulfillmentForEbay BOOLEAN DEFAULT NULL,
  paymentType VARCHAR(64) DEFAULT NULL,
  shippingMethodForEbay VARCHAR(64) DEFAULT NULL,
  amazonSellingCategory varchar(64) DEFAULT NULL,
  needProductListing ENUM('Retain Amazon existing listing only', 'Retain and Add new Product as well') DEFAULT 'Retain Amazon existing listing only',
  needFulfillmentForAmazon ENUM('Fulfillment by Vert', 'Fulfillment by Seller', 'Third Party Fulfillment') DEFAULT 'Fulfillment by Vert',
  shippingMethodForAmazon VARCHAR(64) DEFAULT NULL,
  obtainFBAandFBMForAmazon BOOLEAN DEFAULT NULL,
  productSource ENUM('I manufacture them', 'I resell Products that I Purchase', 'I sell products manufactured by me', 'I dont know') NOT  NULL,
  productIdentifier ENUM('All of my products have them', 'Some of my products have them', 'None of my products have them', 'I dont know') NOT  NULL,
  productCount ENUM('1-10', '11-100', '101-500', 'More than 500', 'I dont know') NOT  NULL,
  webAddressAvailable BOOLEAN NOT  NULL,
  webAddress VARCHAR(64),
  mainProductCategory VARCHAR(64) NOT NULL,
  FOREIGN KEY (storeId)
      REFERENCES stores (storeId)
);

SELECT * FROM sellerquestionnaire;
DESC sellerquestionnaire;
DROP TABLE sellerquestionnaire;
DELETE FROM sellerquestionnaire;
truncate table sellerquestionnaire;

