CREATE TABLE sellertaxdetails (
  storeId INT UNSIGNED NOT NULL PRIMARY KEY,
  incomeReceiver ENUM('Individuals/Sales Proprietor', 'Business') NOT NULL,
  usCitizen BOOLEAN NOT NULL,
  taxName VARCHAR(256) NOT NULL,
  tradeName VARCHAR(200) NOT NULL,
  taxAddress VARCHAR(512) NOT NULL,
  taxClassification ENUM('S Corporation', 'C Corporation', 'Partnership', 'Trust/Estate', 'Limited Liability Company', 'Others') NOT NULL,
  taxpayerIdentificationType ENUM('SSN or TIN Number', 'EIN Number') NOT NULL,
  taxpayerIdentificationNumber VARCHAR(9) NOT NULL,
  needElectronicSignature BOOLEAN NOT  NULL,
  FOREIGN KEY (storeId)
      REFERENCES stores (storeId)
);

SELECT * FROM sellertaxdetails;
DESC sellertaxdetails;
DROP TABLE sellertaxdetails;
truncate table sellertaxdetails;
set sql_safe_updates =0;
