CREATE TABLE sellingmode (
    id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    storeId INT UNSIGNED NOT NULL,
    currentSellingMode VARCHAR(64) NOT NULL,
    sellerSource VARCHAR(64) NOT NULL,
    orderPrefix VARCHAR(64) NOT NULL,
    source VARCHAR(64) NOT NULL,
    hostURL VARCHAR(512) NOT NULL,
    port INT NOT NULL,
    hostUserName VARCHAR(64) NOT NULL,
    hostPassword VARCHAR(192) NOT NULL,
    SFTPHostURL VARCHAR(512) NOT NULL,
    SFTPUserName VARCHAR(64) NOT NULL,
    SFTPPassword VARCHAR(64) NOT NULL,
    SFTPFolderPath VARCHAR(512) NOT NULL,
    SFTPKeyPath VARCHAR(512) NOT NULL,
    APIKey VARCHAR(512) NOT NULL,
    UNIQUE (storeId , currentSellingMode),
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);
alter table sellingmode auto_increment = 1;

ALTER TABLE sellingmode ADD COLUMN contractId INT(10) UNSIGNED DEFAULT NULL AFTER storeId;
ALTER TABLE sellingmode ADD CONSTRAINT FOREIGN KEY (contractId) REFERENCES contract (contractId);


select * from sellingmode;
desc sellingmode;
drop table sellingmode;
delete from sellingmode;
truncate table sellingmode;

show create table sellingmode;

update sellingmode set storeId = 2 where currentSellingMode = 'd';
insert into sellingmode ()
values(1, 1, 'Amazon', 'EMP01', 'VertOrder', 'EMP01', 'https://www.amazon.in', 1119, 'AmazonHost', 'AmazonPwsd',
 'https://www.amazon.in', 'AmazonHost', 'AmazonPwsd', 'https://www.amazon.in/folder', 'https://www.amazon.in/folder/key', 'My Name is KHAN');