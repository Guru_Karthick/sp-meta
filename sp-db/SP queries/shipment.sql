CREATE TABLE shipment (
    shipmentId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
    shipmentNumber VARCHAR(32)BINARY NOT NULL,
    shipmentStatus ENUM('FRESH', 'CONFIRMED', 'CANCELLED', 'DISPATCHED', 'IN_TRANSIT', 'AT_CONSIGNEE', 'DELIVERED') NOT NULL,
    isIncomplete BOOLEAN NOT NULL DEFAULT FALSE,
    createdBy VARCHAR(32)BINARY NOT NULL,
    createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modifiedBy VARCHAR(32)BINARY NOT NULL,
    modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    UNIQUE (shipmentNumber)
);

select * from shipment;
desc shipment;
drop table shipment;
delete from shipment;
truncate table shipment;

alter table shipment modify shipmentStatus ENUM('FRESH', 'CONFIRMED', 'CANCELLED', 'DISPATCHED', 'IN_TRANSIT', 'AT_CONSIGNEE', 'DELIVERED') NOT NULL;