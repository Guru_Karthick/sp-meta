CREATE TABLE shipment_asn_info (
    shipmentId INT UNSIGNED NOT NULL,
    asnId INT UNSIGNED NOT NULL,
    UNIQUE (shipmentId , asnId),
    PRIMARY KEY (shipmentId , asnId),
    FOREIGN KEY (shipmentId)
        REFERENCES shipment (shipmentId),
    FOREIGN KEY (asnId)
        REFERENCES new_asn (asnId)
);

SELECT * FROM shipment_asn_info;
DESC shipment_asn_info;
DROP TABLE shipment_asn_info;
DELETE FROM shipment_asn_info;
truncate table shipment_asn_info;

update shipment_asn_info set shipmentId = 1 where shipmentId = 2 and asnId in (228);