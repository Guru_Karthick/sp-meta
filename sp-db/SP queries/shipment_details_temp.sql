CREATE TABLE shipment_details_temp (
    shipmentId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    shipmentDateTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    masterBillOfLadingNumber VARCHAR(32),
    originatingLocation VARCHAR(50),
    destinationLocation ENUM('ATLANTA_DC'),
    shipmentSource ENUM('VERTE_SYSTEM', 'BULK_UPLOAD', 'SHIPPER_SYSTEMS'),
    deliveryMode ENUM('CONTAINER', 'LTL', 'FTL'),
    appointmentNumber VARCHAR(20),
    sealNumber VARCHAR(20),
    shipmentDate TIMESTAMP,
    estimatedDeliveryDate TIMESTAMP,
    referenceDocumentType ENUM('ASN', 'PO'),
    referenceDocumentNumber VARCHAR(20),
    referenceDocumentDate TIMESTAMP,
    shipmentSentBy VARCHAR(50),
    isMaterialHazardous BOOLEAN,
    emergencyContactNumber VARCHAR(16),
    emergencyContactName VARCHAR(60),
    remarks VARCHAR(255),
    carrierName VARCHAR(60),
    shipVia VARCHAR(30),
    modeOfTransport ENUM('ROAD', 'SHIP', 'AIR'),
    vehicleType ENUM('TRUCK', 'CONTAINER'),
    typeOfContainer ENUM('_8FT_CONTAINER', '_10FT_CONTAINER', '_20FT_CONTAINER', '_40FT_CONTAINER'),
    vehicleNumber VARCHAR(32),
    driverName VARCHAR(50),
    containerLength DECIMAL(12 , 2 ),
    containerWidth DECIMAL(12 , 2 ),
    containerHeight DECIMAL(12 , 2 ),
    unloaderName VARCHAR(50),
    consignmentUser VARCHAR(50),
    supervisor VARCHAR(50),
    packagingOptions ENUM('PALLET', 'CASE', 'MIXED'),
    trackingNumber VARCHAR(32),
    grossWeight DECIMAL(12 , 2 ),
    nettWeight DECIMAL(12 , 2 ),
    volume DECIMAL(12 , 2 ),
    quarantineType ENUM('AQIS', 'IFIP'),
    quarantineStatus ENUM('YES', 'NO'),
    blockingStage ENUM('UNLOADING', 'PUTAWAY'),
    releaseNumber VARCHAR(32),
    releaseDate TIMESTAMP,
    fromName VARCHAR(50),
    fromAddress1 VARCHAR(50),
    fromAddress2 VARCHAR(50),
    fromCity VARCHAR(32),
    fromState VARCHAR(32),
    fromCountry VARCHAR(50),
    fromZipCode VARCHAR(10),
    fromPhoneNumber VARCHAR(16),
    toName VARCHAR(50),
    toAddress1 VARCHAR(50),
    toAddress2 VARCHAR(50),
    toCity VARCHAR(32),
    toState VARCHAR(32),
    toCountry VARCHAR(50),
    toZipCode VARCHAR(10),
    FOREIGN KEY (shipmentId)
        REFERENCES shipment (shipmentId)
);


select * from shipment_details_temp;
desc shipment_details_temp;
drop table shipment_details_temp;
delete from shipment_details_temp;
truncate table shipment_details_temp;


set sql_safe_updates = 0;
update shipment_details_temp set destinationLocation = 'ATLANTA_DC', shipmentSource = 'VERTE_SYSTEM';


ALTER TABLE shipment_details_temp ADD totalDeclaredValue DECIMAL(12 , 2 ) AFTER volume;