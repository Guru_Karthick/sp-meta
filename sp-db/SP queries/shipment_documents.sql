CREATE TABLE shipment_documents (
    id INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    shipmentId INT UNSIGNED NOT NULL,
    documentName VARCHAR(32) NOT NULL,
    documentPath VARCHAR(512) NOT NULL,
    uploadedBy VARCHAR(32) NOT NULL,
    uploadedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    UNIQUE (shipmentId , documentName),
    FOREIGN KEY (shipmentId)
        REFERENCES shipment (shipmentId)
);

SELECT * FROM shipment_documents;
DESC shipment_documents;
DROP TABLE shipment_documents;
DELETE FROM shipment_documents;
truncate table shipment_documents;

ALTER TABLE shipment_documents ADD COLUMN uploadedBy VARCHAR(32) NOT NULL;
ALTER TABLE shipment_documents ADD COLUMN uploadedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;
