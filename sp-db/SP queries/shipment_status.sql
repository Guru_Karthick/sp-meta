CREATE TABLE shipment_status (
	id INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    shipmentId INT UNSIGNED NOT NULL,
    currentShipmentStatus ENUM('DISPATCHED', 'IN_TRANSIT', 'AT_CONSIGNEE', 'DELIVERED') NOT NULL,
    currentLocation VARCHAR(50) NOT NULL,
    currentEstimatedTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updatedBy VARCHAR(32) NOT NULL,
    updatedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (shipmentId)
        REFERENCES shipment (shipmentId)
);

select * from shipment_status;
desc shipment_status;
drop table shipment_status;
delete from shipment_status;
truncate table shipment_status;

ALTER TABLE shipment_status ADD COLUMN updatedBy VARCHAR(32) NOT NULL;
ALTER TABLE shipment_status ADD COLUMN updatedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;


ALTER TABLE shipment_status DROP PRIMARY KEY;
ALTER TABLE shipment_status DROP INDEX shipmentId;

ALTER TABLE `sellerportal`.`shipment_status` DROP FOREIGN KEY `shipment_status_ibfk_1`;
ALTER TABLE `sellerportal`.`shipment_status` DROP INDEX `shipmentId` ;

ALTER TABLE shipment_status ADD COLUMN id INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY FIRST;

ALTER TABLE shipment_status ADD CONSTRAINT FOREIGN KEY (shipmentId) REFERENCES shipment(shipmentId);