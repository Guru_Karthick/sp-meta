select * from states;

DROP TABLE IF EXISTS `states`;
CREATE TABLE `states` (
  `stateName` varchar(30) NOT NULL,
  `stateCode` varchar(2) NOT NULL,
  `countryCode` varchar(4) NOT NULL,
  PRIMARY KEY (`stateCode`)
);

INSERT INTO `states` VALUES ('Alaska', 'AK', 'US');
INSERT INTO `states` VALUES ('Alabama', 'AL', 'US');
INSERT INTO `states` VALUES ('Arkansas', 'AR', 'US');
INSERT INTO `states` VALUES ('Arizona', 'AZ', 'US');
INSERT INTO `states` VALUES ('California', 'CA', 'US');
INSERT INTO `states` VALUES ('Colorado', 'CO', 'US');
INSERT INTO `states` VALUES ('Connecticut', 'CT', 'US');
INSERT INTO `states` VALUES ('District of Columbia', 'DC', 'US');
INSERT INTO `states` VALUES ('Delaware', 'DE', 'US');
INSERT INTO `states` VALUES ('Florida', 'FL', 'US');
INSERT INTO `states` VALUES ('Georgia', 'GA', 'US');
INSERT INTO `states` VALUES ('Hawaii', 'HI', 'US');
INSERT INTO `states` VALUES ('Iowa', 'IA', 'US');
INSERT INTO `states` VALUES ('Idaho', 'ID', 'US');
INSERT INTO `states` VALUES ('Illinois', 'IL', 'US');
INSERT INTO `states` VALUES ('Indiana', 'IN', 'US');
INSERT INTO `states` VALUES ('Kansas', 'KS', 'US');
INSERT INTO `states` VALUES ('Kentucky', 'KY', 'US');
INSERT INTO `states` VALUES ('Louisiana', 'LA', 'US');
INSERT INTO `states` VALUES ('Massachusetts', 'MA', 'US');
INSERT INTO `states` VALUES ('Maryland', 'MD', 'US');
INSERT INTO `states` VALUES ('Maine', 'ME', 'US');
INSERT INTO `states` VALUES ('Michigan', 'MI', 'US');
INSERT INTO `states` VALUES ('Minnesota', 'MN', 'US');
INSERT INTO `states` VALUES ('Missouri', 'MO', 'US');
INSERT INTO `states` VALUES ('Mississippi', 'MS', 'US');
INSERT INTO `states` VALUES ('Montana', 'MT', 'US');
INSERT INTO `states` VALUES ('North Carolina', 'NC', 'US');
INSERT INTO `states` VALUES ('North Dakota', 'ND', 'US');
INSERT INTO `states` VALUES ('Nebraska', 'NE', 'US');
INSERT INTO `states` VALUES ('New Hampshire', 'NH', 'US');
INSERT INTO `states` VALUES ('New Jersey', 'NJ', 'US');
INSERT INTO `states` VALUES ('New Mexico', 'NM', 'US');
INSERT INTO `states` VALUES ('Nevada', 'NV', 'US');
INSERT INTO `states` VALUES ('New York', 'NY', 'US');
INSERT INTO `states` VALUES ('Ohio', 'OH', 'US');
INSERT INTO `states` VALUES ('Oklahoma', 'OK', 'US');
INSERT INTO `states` VALUES ('Oregon', 'OR', 'US');
INSERT INTO `states` VALUES ('Pennsylvania', 'PA', 'US');
INSERT INTO `states` VALUES ('Rhode Island', 'RI', 'US');
INSERT INTO `states` VALUES ('South Carolina', 'SC', 'US');
INSERT INTO `states` VALUES ('South Dakota', 'SD', 'US');
INSERT INTO `states` VALUES ('Tennessee', 'TN', 'US');
INSERT INTO `states` VALUES ('Texas', 'TX', 'US');
INSERT INTO `states` VALUES ('Utah', 'UT', 'US');
INSERT INTO `states` VALUES ('Virginia', 'VA', 'US');
INSERT INTO `states` VALUES ('Vermont', 'VT', 'US');
INSERT INTO `states` VALUES ('Washington', 'WA', 'US');
INSERT INTO `states` VALUES ('Wisconsin', 'WI', 'US');
INSERT INTO `states` VALUES ('West Virginia', 'WV', 'US');
INSERT INTO `states` VALUES ('Wyoming', 'WY', 'US');

INSERT INTO `states` VALUES ('Puerto Rico', 'PR', 'US');


