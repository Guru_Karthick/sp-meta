CREATE TABLE storebankdetails (
    storeId INT UNSIGNED NOT NULL PRIMARY KEY,
    accountantFullName VARCHAR(40),
    bankName VARCHAR(60),
    accountNumber VARCHAR(20),
    routingNumber VARCHAR(10),
    swiftCode VARCHAR(15),
    createdBy VARCHAR(32) NOT NULL,
    createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modifiedBy VARCHAR(32) NOT NULL,
    modifiedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);

select * from  storebankdetails;
desc storebankdetails;
drop table storebankdetails;
delete from storebankdetails;
select * from roles;


use integration;
select * from integration;