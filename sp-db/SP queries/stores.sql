CREATE TABLE stores (
    storeId INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    storeName VARCHAR(64) NOT NULL UNIQUE,
    revenue VARCHAR(64),
    maximumProductSku INT UNSIGNED,
    modeOfContact VARCHAR(8),
    preference VARCHAR(64),
    timeOfContact VARCHAR(64),
    applicationStatus ENUM('APPROVED', 'REJECTED', 'PENDING') NOT NULL DEFAULT 'APPROVED',
    fulfillment VARCHAR(64),
    imagePath VARCHAR(512),
    orderPickFrequency VARCHAR(64),
    additionalParameter1 VARCHAR(256),
    additionalParameter2 VARCHAR(256),
    additionalParameter3 VARCHAR(256),
    allDataEntered BOOLEAN DEFAULT 0,
    address VARCHAR(80),
    city VARCHAR(32),
    state VARCHAR(32),
    country VARCHAR(32),
    zipCode VARCHAR(5),
    createdBy VARCHAR(32)BINARY NOT NULL,
    createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modifiedBy VARCHAR(32)BINARY NOT NULL,
    modifiedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
alter table stores auto_increment = 1;

SELECT LAST_INSERT_ID() AS id FROM stores;

select * from stores;
desc stores;
drop table stores;
delete from stores;
delete from stores where storeId = 5;
truncate table stores;

select distinct storeName from stores order by storeName;


update users set sellerStatus = 'ACTIVE' where isFirstUser = 1;

alter table stores modify storeStatus enum('ACTIVE','INACTIVE','WAITING','REJECTED') default 'WAITING';

alter table stores add column reasonForRejection varchar(128) null after storeStatus;
