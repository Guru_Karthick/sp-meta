CREATE TABLE storetaxdetails (
    storeId INT UNSIGNED NOT NULL PRIMARY KEY,
    USCitizen VARCHAR(5),
    taxClassification VARCHAR(10),
    taxName VARCHAR(20),
    tradeName VARCHAR(30),
    tinType VARCHAR(5),
    createdBy VARCHAR(32) NOT NULL,
    createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modifiedBy VARCHAR(32) NOT NULL,
    modifiedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);

select * from storetaxdetails;
desc storetaxdetails;
drop table storetaxdetails;
delete from storetaxdetails;