CREATE TABLE storeusermapper (
    sellerId INT UNSIGNED NOT NULL,
    storeId INT UNSIGNED NOT NULL,
    FOREIGN KEY (sellerId)
        REFERENCES users (sellerId),
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);

alter table storeusermapper
add constraint unique(sellerId, storeId);

select * from storeusermapper;
select * from storeusermapper where storeId = 1;
desc storeusermapper;
drop table storeusermapper;
delete from storeusermapper;
truncate table storeusermapper;

delete from storeusermapper where storeId = 1;
insert into storeusermapper
values (1, 1);

SELECT t1.storeId,t2.sellerId
FROM stores t1 LEFT OUTER JOIN storeusermapper t2
ON t1.storeId = t2.storeId
ORDER BY t1.storeId;

delete from stores where storeId = (SELECT t1.storeId
FROM stores t1 LEFT OUTER JOIN storeusermapper t2
ON t1.storeId = t2.storeId WHERE sellerId is null);

SELECT t1.storeId
FROM stores t1 LEFT OUTER JOIN storeusermapper t2
ON t1.storeId = t2.storeId WHERE sellerId is null;

SELECT t1.sellerId
FROM users t1 LEFT OUTER JOIN storeusermapper t2
ON t1.sellerId = t2.sellerId WHERE storeId is null;

UPDATE users SET phoneNumber = LPAD(FLOOR(RAND() * 10000000000), 10, '0') WHERE phoneNumber IN (
SELECT phoneNumber FROM (SELECT phoneNumber FROM users) AS T1
GROUP BY T1.phoneNumber HAVING COUNT(*) > 1);

SELECT  T2.phoneNumber
FROM (select phoneNumber from stores) as T2
GROUP BY T2.phoneNumber
HAVING COUNT(*) > 1;

SELECT COUNT(*) AS cnt, T3.allowedUsersCount FROM users T1 JOIN storeusermapper T2 ON T1.sellerId = T2.sellerId 
JOIN stores T3 ON T2.storeId = T3.storeId
 WHERE T2.storeId = 1 AND T1.sellerStatus = 'ACTIVE';

SELECT *, T3.allowedUsersCount FROM users T1 JOIN storeusermapper T2 ON T1.sellerId = T2.sellerId JOIN stores T3 ON T2.storeId = T3.storeId
WHERE T2.storeId = 1;

SELECT LAST_INSERT_ID() AS id FROM users;

SELECT DISTINCT  t1.sellerId,t2.storeId,t1.userName, DATE(t1.createdOn) as date,TIME(t1.createdOn) as time
FROM users t1 JOIN stores t2 ON t1.sellerId = t2.storeId
ORDER BY t1.createdOn ASC;

select t2.storeId, t1.sellerId from users t1 join storeusermapper t3 on t1.sellerId = t3.sellerId join stores t2 on t2.storeId = t3.storeId
where t1.isFirstUser = 1;
;

select t1.* from users t1 join storeusermapper t3 on t1.sellerId = t3.sellerId join stores t2 on t2.storeId = t3.storeId
where t2.storeId = 1;

update users set sellerStatus = 'INACTIVE' where isFirstUser = 0;

select * from users where sellerStatus = 'ACTIVE';