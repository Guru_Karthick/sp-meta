CREATE TABLE subfeatures (
    id INT UNSIGNED,
    feature VARCHAR(20) NOT NULL UNIQUE,
    none INT UNSIGNED NOT NULL,
    view INT UNSIGNED NOT NULL,
    view_edit INT UNSIGNED NOT NULL
);
alter table subfeatures
add column subfeatureId INT UNSIGNED AUTO_INCREMENT NOT NULL UNIQUE first;

select * from subfeatures;
desc subfeatures;
drop table subfeatures;
delete from subfeatures;
truncate table subfeatures;

insert into subfeatures (id,feature,none,view,view_edit) values(1,"Add Single Product",1,0,1);
insert into subfeatures (id,feature,none,view,view_edit) values(1,"Add Bulk Product",1,0,1);

insert into subfeatures (id,feature,none,view,view_edit) values(2,"Order Management",1,1,0);
insert into subfeatures (id,feature,none,view,view_edit) values(2,"Return Management",1,1,0);

insert into subfeatures (id,feature,none,view,view_edit) values(1,"Catalog Management",1,1,1);
insert into subfeatures (id,feature,none,view,view_edit) values(1,"Pricing Management",1,1,1);