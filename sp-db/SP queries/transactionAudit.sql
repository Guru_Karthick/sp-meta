CREATE TABLE transactionAudit (
   transcationId INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
   storeId INT UNSIGNED NOT NULL,
   userId INT UNSIGNED NOT NULL,
   transactionType ENUM('SKU') NOT NULL,
   transactionAction ENUM('Created','Modified') NOT NULL,
    transactionItem varchar(64) NOT NULL,
    originatingIp varchar(45) NOT NULL,
   actionPerformedAt TIMESTAMP NOT NULL,
    oldValue JSON
);

alter table userLoginAudit modify originatingIp varchar(45) NOT NULL;