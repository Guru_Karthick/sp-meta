CREATE TABLE userAssignedRoles (
   assignId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
   userId INT UNSIGNED NOT NULL,
   roleId INT UNSIGNED NOT NULL,
   UNIQUE (userId , roleId),
   FOREIGN KEY (userId)
        REFERENCES users (sellerId),
    FOREIGN KEY (roleId)
        REFERENCES userRoles (roleId)
  );
 
 select * from  userAssignedRoles;
 desc userAssignedRoles;
 
 
 INSERT INTO userAssignedRoles (userId, roleId) SELECT sellerId, roles_id FROM assignednames;