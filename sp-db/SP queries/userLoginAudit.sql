CREATE TABLE userLoginAudit (
   loginId INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
   userId INT UNSIGNED NOT NULL,
   originatingIp varchar(15) NOT NULL,
   loginAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   logoutAt TIMESTAMP NULL DEFAULT NULL,
   FOREIGN KEY (userId)
       REFERENCES users(sellerId)
);

select * from userLoginAudit;
desc userLoginAudit;
drop table userLoginAudit;
delete from userLoginAudit;
truncate table userLoginAudit;

use sellerportal;

INSERT INTO userloginactivity(userId, originatingIp, loginAt, logoutAt) VALUES ( 1, '210.18.140.124', '2018-09-24 11:22:36.057', null);
