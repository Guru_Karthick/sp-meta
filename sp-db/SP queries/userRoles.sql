CREATE TABLE userRoles(
	roleId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
	storeId INT UNSIGNED NOT NULL,
	roleName VARCHAR(64) NOT NULL,
	description VARCHAR(256),
	createdBy VARCHAR(32) NOT NULL,
	createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	modifiedBy VARCHAR(32) NOT NULL,
	modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	UNIQUE (roleName , storeId),
	FOREIGN KEY (storeId) REFERENCES stores(storeId));
 
 select * from userRoles;
 

INSERT INTO userRoles (roleId, storeId, roleName, description,createdBy, modifiedBy) SELECT roles_id, storeId, name, description,'admin','admin' FROM  roles;