CREATE TABLE userSelectedFunctions (
    roleId INT UNSIGNED NOT NULL,
    functionId INT UNSIGNED NOT NULL,
	UNIQUE (roleId , functionId),
    FOREIGN KEY (roleId)
        REFERENCES userroles (roleId),
    FOREIGN KEY (functionId)
        REFERENCES menuFunctions (functionId)
   );
   
 
 select * from userSelectedFunctions;
 
