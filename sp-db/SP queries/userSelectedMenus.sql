CREATE TABLE userSelectedMenus (
   roleId INT UNSIGNED NOT NULL,
   menuId INT UNSIGNED NOT NULL,
   UNIQUE (roleId , menuId),
    FOREIGN KEY (roleId)
        REFERENCES userRoles (roleId),
    FOREIGN KEY (menuId)
        REFERENCES menus (menuId)
  );
 

INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,1 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,2 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,3 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,4 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,5 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,6 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,7 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,8 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,9 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,10 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,11 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,12 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,13 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,14 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,15 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,16 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,17 FROM userRoles ;



