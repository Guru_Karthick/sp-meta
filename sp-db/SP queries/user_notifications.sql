CREATE TABLE user_notifications (
    notificationId INT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY,
    storeId INT UNSIGNED NOT NULL,
    notificationCategory ENUM('LOW_STOCK', 'DISTRESSED_STOCK'),
    message VARCHAR(256) NOT NULL,
    notifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);

insert into user_notifications (storeId, notificationCategory, message, notifiedOn) values(1, 'LOW_STOCK', 'Inventory level is very very low', now());