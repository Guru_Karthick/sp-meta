CREATE TABLE user_selected_fields (
    userId INT UNSIGNED NOT NULL,
    category ENUM('ASN_OUTER_GRID', 'ASN_INNER_GRID') NOT NULL,
    selectedFields JSON NOT NULL,
    PRIMARY KEY (userId, category),
    FOREIGN KEY (userId)
        REFERENCES users (sellerId)
);

SELECT * FROM user_selected_fields;
DESC user_selected_fields;
DROP TABLE user_selected_fields;
DELETE FROM user_selected_fields;
truncate table user_selected_fields;

select t2.sku, t2.productStatus, t1.product_id, t1.field_id, t1.value from productvaluemaster1 t1 join productmapper1 t2 on t1.product_id = t2.product_id
join productmanageprice1 t3 on t1.product_id = t3.product_id;