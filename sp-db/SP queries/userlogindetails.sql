CREATE TABLE userlogindetails (
    sellerId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    failedLoginCount INT UNSIGNED NOT NULL default 0,
    lastLoginAttemptAt TIMESTAMP NULL DEFAULT NULL,
    lastLoginAt TIMESTAMP NULL DEFAULT NULL,
    lastLoginFailedAt TIMESTAMP NULL DEFAULT NULL,
    accountLockedAt TIMESTAMP NULL DEFAULT NULL,
    FOREIGN KEY (sellerId)
        REFERENCES users (sellerId)
);

select * from userlogindetails;
desc userlogindetails;
drop table userlogindetails;
delete from userlogindetails;
truncate table userlogindetails;

INSERT INTO userlogindetails (sellerId, failedLoginCount, lastLoginAttemptAt, lastLoginAt, lastLoginFailedAt, accountLockedAt)
SELECT sellerId, 0, NULL, NULL, NULL, NULL
FROM users ORDER BY sellerId ASC;

UPDATE userlogindetails T1 JOIN users T2 ON T1.sellerId = T2.sellerId
SET lastLoginAttemptAt = T2.tokenIssuedAt, lastLoginAt = T2.tokenIssuedAt
WHERE T2.tokenIssuedAt <> 0;
