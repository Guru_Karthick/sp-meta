CREATE TABLE userpasswordjti (
    sellerId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    jti VARCHAR(36)BINARY NOT NULL UNIQUE,
    FOREIGN KEY (sellerId)
        REFERENCES users (sellerId)
);

select * from userpasswordjti;
desc userpasswordjti;
drop table userpasswordjti;
delete from userpasswordjti;
truncate table userpasswordjti;

rename table forgotpassword to userpasswordjti;