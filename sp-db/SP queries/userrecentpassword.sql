CREATE TABLE userrecentpassword (
    sellerId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    password1 VARCHAR(192)BINARY NOT NULL,
    password2 VARCHAR(192)BINARY NOT NULL,
    password3 VARCHAR(192)BINARY NOT NULL,
    FOREIGN KEY (sellerId)
        REFERENCES users (sellerId)
);


INSERT INTO userrecentpassword (sellerId, password1, password2, password3)
SELECT sellerId, password , password, password
FROM users ORDER BY sellerId ASC ;

select * from userrecentpassword;