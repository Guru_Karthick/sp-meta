CREATE TABLE users (
    sellerId INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    userName VARCHAR(32)BINARY NOT NULL UNIQUE,
    password VARCHAR(192)BINARY NOT NULL,
    name VARCHAR(64) NOT NULL,
    email VARCHAR(80) NOT NULL UNIQUE,
    phoneNumber VARCHAR(16),
    userType VARCHAR(32) NOT NULL,
    sellerStatus ENUM('ACTIVE', 'INACTIVE') DEFAULT 'INACTIVE',
    activeFrom TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    activeTo TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    firstlogin BOOLEAN DEFAULT FALSE,
    isCreatedByVert boolean,
    tokenIssuedAt TIMESTAMP NOT NULL DEFAULT 0,
    createdBy VARCHAR(32)BINARY NOT NULL,
    createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modifiedBy VARCHAR(32)BINARY NOT NULL,
    modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (userType)
        REFERENCES usertype (userType)
);
alter table users auto_increment = 1;
set sql_mode = 0;

alter table users drop column rp_token_created_at;
alter table users add column rp_token_created_at timestamp default 0 after rp_token ;
alter table users add column firstlogin boolean default false after activeTo;
update  users set rp_token = 'eyJraWQiOiJOTyBLRVkgSUQiLCJ0eXAiOiJKV1QtVmVydGUiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyTmFtZSI6ImplZXZhIiwiZXhwIjoxNTI5NzU5NTEwLCJpYXQiOjE1Mjk2NzMxMTB9.aNVL3S2b2iH21qh0V8VNl3dV3YI2ocrDi-t7yUv4RbQ' where userName='harini';

select * from users;
desc users;
drop table users;
delete from users where sellerId = 22;
delete from users;
truncate table users;

select storeusermapper.sellerId, storeusermapper.storeId, users.userName, stores.storeName from storeusermapper, users, stores where storeusermapper.sellerId = 1;

select * from users where userName = 'admin';

SELECT COUNT(*) AS cnt FROM users T1 JOIN storeusermapper T2 ON T1.sellerId = T2.sellerId WHERE T2.storeId = 1 AND T1.sellerStatus = 'ACTIVE';






