CREATE TABLE usertype (
   id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
   userType VARCHAR(32) NOT NULL UNIQUE
);

select * from usertype;
desc usertype;
drop table usertype;
delete from usertype;

insert into usertype(userType) VALUES("SUPERUSER");
insert into usertype(userType) VALUES("STANDARDUSER");
