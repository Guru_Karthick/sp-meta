CREATE TABLE vendor_metadata (
    id INT UNSIGNED NOT NULL PRIMARY KEY,
    vendorCode VARCHAR(64) NOT NULL,
    vendorName VARCHAR(64) NOT NULL,
    country VARCHAR(16) NOT NULL
);

INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (1,'CH01280', 'Anhui Huaining Shansen Garments co.,ltd', 'China');
INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (2, 'CH01281', 'Changzhou Jiacheng Garments Co.,Ltd', 'China');
INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (3, 'CH01282', 'Changzhou Meijia Gongyi Xiupin Factory', 'China' );
INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (4, 'CH01283', 'Chongqing Tooku Dec. Manufacturing Co., Ltd','China' );
INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (5, 'US02345', 'Complete Garment, Inc', 'USA');
INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (6, 'US02346', 'Icon Screening, Inc', 'USA');
INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (7, 'US02347', 'ReadyOne Industries , Inc.', 'USA');
INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (8, 'US02348', 'Yesterwear Production Inc', 'USA');
INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (9, 'VI07589', 'Branch of Yupoong Vietnam Co.,Ltd', 'Vietnam');
INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (10, 'VI07590', 'Dong - A Vina Co., Ltd', 'Vietnam');


SELECT * FROM vendor_metadata;
DESC vendor_metadata;
DROP TABLE vendor_metadata;
DELETE FROM vendor_metadata;
truncate table vendor_metadata;
