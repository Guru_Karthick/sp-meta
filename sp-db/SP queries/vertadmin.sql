CREATE TABLE vertadmin (
    userId INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    userName VARCHAR(32)BINARY NOT NULL UNIQUE,
    password VARCHAR(192)BINARY NOT NULL,
    name VARCHAR(64) NOT NULL,
    email VARCHAR(80) NOT NULL UNIQUE,
    phoneNumber VARCHAR(16),
    userType ENUM('VERTADMIN') NOT NULL DEFAULT 'VERTADMIN',
    tokenIssuedAt TIMESTAMP NOT NULL DEFAULT 0,
    lastAccessTime TIMESTAMP NOT NULL DEFAULT 0,
    createdBy VARCHAR(32)BINARY NOT NULL,
    createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modifiedBy VARCHAR(32)BINARY NOT NULL,
    modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
alter table vertadmin auto_increment = 1;

select * from vertadmin;
desc vertadmin;
drop table vertadmin;
delete from vertAdmin where userName <> 'admin';
truncate table vertadmin;

insert into vertAdmin 
values(1, 'admin', 'oNed0pBaaW1ppiMVOcC+l9J0nqCxipnwqDTCEpnBs/44OfQ8GTd7XKZR+WrcA+LMhXuTfg==', 'Administrator', 'vertadmin@gmail.com', '9842030520', 'VERTADMIN', 0, 0, 'vert', now(), 'vert', now());

show tables;

use sellerportal;
use integration;
select * from integration;

ALTER table cities CHARACTER SET utf8;
SELECT default_character_set_name FROM information_schema.SCHEMATA S WHERE schema_name = "integration";

show table status;


DELETE FROM vertrecentpassword WHERE userId IN (SELECT userId FROM vertadmin WHERE userName <> 'admin');
DELETE FROM vertadmin WHERE userName <> 'admin';

ALTER TABLE vertadmin ADD  COLUMN vertuserStatus ENUM('ACTIVE', 'INACTIVE') NOT NULL DEFAULT 'INACTIVE' AFTER userType;
ALTER TABLE vertadmin ADD COLUMN isActivated BOOLEAN NOT NULL DEFAULT 0 AFTER vertuserStatus;
ALTER TABLE vertadmin ADD COLUMN firstlogin BOOLEAN NOT NULL DEFAULT 1 AFTER isActivated;
ALTER TABLE vertadmin ADD COLUMN isFirstUser BOOLEAN NOT NULL DEFAULT 0 AFTER firstlogin;

UPDATE vertadmin SET vertuserStatus = 'ACTIVE', isActivated = 1, firstlogin = 0, isFirstUser = 1 WHERE userName = 'admin';
UPDATE vertadmin SET email = 'vertadmin@projectverte.com', phoneNumber = '9999888776' WHERE userName = 'admin';

select * from vertadmin where name like '%Admin%';

select * from vertlogindetails;

alter table stores 