CREATE TABLE vertapicontrol (
    apiId INT UNSIGNED NOT NULL,
    featureId INT UNSIGNED NOT NULL,
    reqFeatureVal INT UNSIGNED NOT NULL,
    PRIMARY KEY (apiId , featureId),
    FOREIGN KEY (apiId)
        REFERENCES vertapilist (apiId),
    FOREIGN KEY (featureId)
        REFERENCES vertfeatures (feature_id)
);

select * from vertapicontrol;
desc vertapicontrol;
drop table vertapicontrol;
delete from vertapicontrol;
truncate table vertapicontrol;

insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(8, 2, 2);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(9, 2, 2);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(10, 2, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(11, 2, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(12, 2, 2);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(13, 2, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(14, 2, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(15, 2, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(16, 2, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(17, 1, 2);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(18, 1, 2);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(19, 1, 2);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(20, 1, 2);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(21, 1, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(22, 1, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(23, 1, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(24, 1, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(25, 1, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(26, 1, 3);