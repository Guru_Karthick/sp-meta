CREATE TABLE vertapilist (
    apiId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    resourceName VARCHAR(64)BINARY NOT NULL,
    methodName VARCHAR(64)BINARY NOT NULL,
    verb ENUM('GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'OPTIONS', 'HEAD') NOT NULL,
    UNIQUE (resourceName , methodName , verb)
);

select * from vertapilist order by apiId;
desc vertapilist;
drop table vertapilist;
delete from vertapilist;

insert into vertapilist (apiId, resourceName , methodName , verb) values(1, '', 'vertadminlogin', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(2, '', 'getvertpermission', 'GET');
insert into vertapilist (apiId, resourceName , methodName , verb) values(3, '', 'checktokenexpiry', 'GET');
insert into vertapilist (apiId, resourceName , methodName , verb) values(4, '', 'createpassword', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(5, '', 'forgotpassword', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(6, '', 'resetpassword', 'PUT');
insert into vertapilist (apiId, resourceName , methodName , verb) values(7, '', 'resendpasswordlink', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(8, '', 'getmetadata', 'GET');
insert into vertapilist (apiId, resourceName , methodName , verb) values(9, '', 'getallroles', 'GET');
insert into vertapilist (apiId, resourceName , methodName , verb) values(10, '', 'createrole', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(11, '', 'updaterole', 'PUT');
insert into vertapilist (apiId, resourceName , methodName , verb) values(12, '', 'getalladmins', 'GET');
insert into vertapilist (apiId, resourceName , methodName , verb) values(13, '', 'createvertadmin', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(14, '', 'updatevertadmin', 'PUT');
insert into vertapilist (apiId, resourceName , methodName , verb) values(15, '', 'updatevertuserstatus', 'PUT');
insert into vertapilist (apiId, resourceName , methodName , verb) values(16, '', 'checkfieldvert', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(17, '', 'getallsellers', 'GET');
insert into vertapilist (apiId, resourceName , methodName , verb) values(18, '', 'getawaitingsellers', 'GET');
insert into vertapilist (apiId, resourceName , methodName , verb) values(19, '', 'getactivesellers', 'GET');
insert into vertapilist (apiId, resourceName , methodName , verb) values(20, '', 'getinactivesellers', 'GET');
insert into vertapilist (apiId, resourceName , methodName , verb) values(21, '', 'createseller', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(22, '', 'updateseller', 'PUT');
insert into vertapilist (apiId, resourceName , methodName , verb) values(23, '', 'updatestorestatus', 'PUT');
insert into vertapilist (apiId, resourceName , methodName , verb) values(24, '', 'updateuserstatus', 'PUT');
insert into vertapilist (apiId, resourceName , methodName , verb) values(25, '', 'checkfieldstore', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(26, '', 'checkfielduser', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(27, '', 'getaddress', 'POST');