CREATE TABLE vertassignedroles (
    userId INT UNSIGNED NOT NULL,
    role_id INT UNSIGNED NOT NULL,
    UNIQUE (userId , role_id),
    FOREIGN KEY (userId)
        REFERENCES vertadmin (userId),
    FOREIGN KEY (role_id)
        REFERENCES vertroles (role_id)
);
show tables;
select * from vertassignedroles;
desc vertassignedroles;
drop table vertassignedroles;
delete from vertassignedroles;
truncate table vertassignedroles;
