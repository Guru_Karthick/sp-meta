CREATE TABLE vertfeatures (
    feature_id INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    feature VARCHAR(64) NOT NULL UNIQUE,
    title VARCHAR(64) NOT NULL,
    parent_id INT UNSIGNED,
    none INT UNSIGNED NOT NULL,
    view INT UNSIGNED NOT NULL,
    view_edit INT UNSIGNED NOT NULL,
    FOREIGN KEY (parent_id)
        REFERENCES vertfeatures (feature_id)
);

select * from vertfeatures;
desc vertfeatures;
drop table vertfeatures;
delete from vertfeatures;
truncate table vertfeatures;

insert into vertfeatures values(5, 'inactiver', 'seller', 1, 1, 1, 1);
select * from vertfeatures where parent_id is null;


insert into vertfeatures values(1, 'sellers', 'Sellers', null, 1, 1, 1);
insert into vertfeatures values(2, 'admins', 'Admins', null, 1, 1, 1);
insert into vertfeatures values(3, 'catagory', 'Catagory', null, 1, 1, 1);
