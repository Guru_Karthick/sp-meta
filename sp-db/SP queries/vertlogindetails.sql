CREATE TABLE vertlogindetails (
    userId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    failedLoginCount INT UNSIGNED NOT NULL default 0,
    lastLoginAttemptAt TIMESTAMP NULL DEFAULT NULL,
    lastLoginAt TIMESTAMP NULL DEFAULT NULL,
    lastLoginFailedAt TIMESTAMP NULL DEFAULT NULL,
    accountLockedAt TIMESTAMP NULL DEFAULT NULL,
    FOREIGN KEY (userId)
        REFERENCES vertadmin (userId)
);

select * from vertlogindetails;
desc vertlogindetails;
drop table vertlogindetails;
delete from vertlogindetails;
truncate table vertlogindetails;

INSERT INTO vertlogindetails (userId, failedLoginCount, lastLoginAttemptAt, lastLoginAt, lastLoginFailedAt, accountLockedAt)
SELECT userId, 0, NULL, NULL, NULL, NULL
FROM vertadmin ORDER BY userId ASC;

UPDATE vertlogindetails T1 JOIN vertadmin T2 ON T1.userId = T2.userId
SET lastLoginAttemptAt = T2.tokenIssuedAt, lastLoginAt = T2.tokenIssuedAt
WHERE T2.tokenIssuedAt <> 0;
