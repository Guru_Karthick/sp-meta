CREATE TABLE vertpasswordjti (
    userId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    jti VARCHAR(36)BINARY NOT NULL UNIQUE,
    FOREIGN KEY (userId)
        REFERENCES vertadmin (userId)
);

select * from vertpasswordjti;
desc vertpasswordjti;
drop table vertpasswordjti;
delete from vertpasswordjti;
truncate table vertpasswordjti;