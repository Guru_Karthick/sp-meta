CREATE TABLE vertrecentpassword (
    userId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    password1 VARCHAR(192)BINARY NOT NULL,
    password2 VARCHAR(192)BINARY NOT NULL,
    password3 VARCHAR(192)BINARY NOT NULL,
    FOREIGN KEY (userId)
        REFERENCES vertadmin (userId)
);


INSERT INTO vertrecentpassword (userId, password1, password2, password3)
SELECT userId, password , password, password
FROM vertadmin ORDER BY userId ASC ;

select * from vertrecentpassword;
desc vertrecentpassword;