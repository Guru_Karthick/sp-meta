CREATE TABLE vertroles (
    role_id INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
    role_name VARCHAR(64) NOT NULL UNIQUE,
    description VARCHAR(256) NULL,
    createdBy VARCHAR(32)BINARY NOT NULL,
    createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modifiedBy VARCHAR(32)BINARY NOT NULL,
    modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

select * from vertroles;
desc vertroles;
drop table vertroles;
delete from vertroles;
truncate table vertroles;

insert into vertroles values(1, 'man', 'desc');
insert into vertroles values(3, 'hey', 'desc');
select last_insert_id(feature_id) from vertfeatures;

