CREATE TABLE vertselectedfeatures (
    id INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
    role_id INT UNSIGNED NOT NULL,
    feature_id INT UNSIGNED NOT NULL,
    selected_value INT UNSIGNED NOT NULL,
    UNIQUE (role_id , feature_id),
    FOREIGN KEY (role_id)
        REFERENCES vertroles (role_id),
    FOREIGN KEY (feature_id)
        REFERENCES vertfeatures (feature_id)
);

alter table vertselectedfeatures add column up timestamp default current_timestamp on update current_timestamp after selected_value;
select * from vertselectedfeatures;
desc vertselectedfeatures;
drop table vertselectedfeatures;
delete from vertselectedfeatures;
truncate table vertselectedfeatures;

insert into vertselectedfeatures(role_id, feature_id, selected_value)
values(2, 1, 2);

INSERT INTO vertselectedfeatures (role_id, feature_id, selected_value) VALUES(3, 1, 3) ON DUPLICATE KEY UPDATE    
selected_value = 2;

SELECT COUNT(*) AS cnt, role_id FROM vertroles WHERE role_name = 'stevejobs';

SELECT T1.role_id, T1.feature_id, T1.selected_value FROM vertselectedfeatures T1 JOIN vertassignedroles T2 ON T1.role_id = T2.role_id
WHERE T2.userId = 18;