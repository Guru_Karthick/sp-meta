

#14.06.18
delete from productvaluemaster WHERE field_id = 26;
delete from productvaluemaster WHERE field_id = 34;
delete from productvaluemaster WHERE field_id = 35;
delete from productvaluemaster WHERE field_id = 36;

DELETE FROM `sellerportal`.`fieldmaster` WHERE `field_id`='26';
DELETE FROM `sellerportal`.`fieldmaster` WHERE `field_id`='34';
DELETE FROM `sellerportal`.`fieldmaster` WHERE `field_id`='36';
DELETE FROM `sellerportal`.`fieldmaster` WHERE `field_id`='35';

INSERT INTO fieldmaster (name, title, datatype, length, ismandotry, default_selected, has_possible_values, possible_values) 
VALUES ('package_dimension_UOM','Package Dimension UOM', 'varchar', 8, false, false, true, JSON_OBJECT("value",JSON_ARRAY("CM", "INCH")));

update fieldmaster set title = 'Parent SKU ID', default_selected = 0, ismandotry = 0 where field_id = 2;


#18.06.18
update fieldmaster set name = 'parent_sku_id' where field_id = 2;
update fieldmaster set name = 'storage', title = 'Storage' where field_id = 76;


#19.06.18
alter table subfeatures
add column subfeatureId INT UNSIGNED AUTO_INCREMENT NOT NULL UNIQUE first;


#21.06.18
alter table stores
add column sellerCode varchar(32) unique after storeId;

update stores set sellerCode = stores.storeId;


#27.06.18
	#orders 
DROP INDEX orderId ON orders;
alter table orders
add constraint unique(orderId, storeId);

alter table orders
DROP primary key;
alter table orders
add constraint primary key(orderId, storeId);

	#orderlist (drop all existing foreign keys)
alter table orderlist
add column storeId INT UNSIGNED NOT NULL after orderId;

UPDATE orderlist AS t1
  INNER JOIN orders AS t2 ON t1.orderId = t2.orderId
SET t1.storeId = t2.storeId;

alter table orderlist
add constraint foreign key(orderId, storeId) references orders(orderId, storeId);

alter table orderlist
DROP primary key;

alter table orderlist
add primary key(orderId, storeId, product_id);

	#customers (drop all existing foreign keys)
alter table customers
add column storeId INT UNSIGNED NOT NULL after orderId;

UPDATE customers AS t1
  INNER JOIN orders AS t2 ON t1.orderId = t2.orderId
SET t1.storeId = t2.storeId;

alter table customers
add constraint foreign key(orderId, storeId) references orders(orderId, storeId);

alter table customers
DROP primary key;

alter table customers
add primary key(orderId, storeId);


#29.6.18
alter table subfeatures
add column subfeatureId INT UNSIGNED AUTO_INCREMENT NOT NULL UNIQUE first;

CREATE TABLE apilist (
    apiId INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    methodName VARCHAR(64)BINARY NOT NULL,
    className VARCHAR(64)BINARY NOT NULL,
    UNIQUE (methodName , className)
);
INSERT INTO apilist (methodName, className) values ('storelogin', 'general');
INSERT INTO apilist (methodName, className) values ('createuser', 'general');
INSERT INTO apilist (methodName, className) values ('updateuser', 'general');
INSERT INTO apilist (methodName, className) values ('updatestorebankdetails', 'general');
INSERT INTO apilist (methodName, className) values ('updatestoretaxdetails', 'general');
INSERT INTO apilist (methodName, className) values ('getallstoreusers', 'general');
INSERT INTO apilist (methodName, className) values ('checkfielduser', 'general');
INSERT INTO apilist (methodName, className) values ('resetpassword', 'general');
INSERT INTO apilist (methodName, className) values ('getalluserstatus', 'general');
INSERT INTO apilist (methodName, className) values ('updateuserstatus', 'general');
INSERT INTO apilist (methodName, className) values ('getuserdetails', 'general');
INSERT INTO apilist (methodName, className) values ('edituserdetails', 'general');

INSERT INTO apilist (methodName, className) values ('getdashboarddetails', 'dashboard');

INSERT INTO apilist (methodName, className) values ('getsalesdetails', 'sales');

INSERT INTO apilist (methodName, className) values ('generateasnnumber', 'asn');
INSERT INTO apilist (methodName, className) values ('checkasnnumber', 'asn');
INSERT INTO apilist (methodName, className) values ('createasn', 'asn');
INSERT INTO apilist (methodName, className) values ('updateasn', 'asn');
INSERT INTO apilist (methodName, className) values ('getallasn', 'asn');
INSERT INTO apilist (methodName, className) values ('getfilteredasn', 'asn');
INSERT INTO apilist (methodName, className) values ('deleteasn', 'asn');

INSERT INTO apilist (methodName, className) values ('getallproducts', 'product');
INSERT INTO apilist (methodName, className) values ('getpageproducts', 'product');
INSERT INTO apilist (methodName, className) values ('getpageproductsdownload', 'product');
INSERT INTO apilist (methodName, className) values ('getselectedproductsdownload', 'product');
INSERT INTO apilist (methodName, className) values ('getproductcount', 'product');
INSERT INTO apilist (methodName, className) values ('addsingleproduct', 'product');
INSERT INTO apilist (methodName, className) values ('addbulkproducts', 'product');
INSERT INTO apilist (methodName, className) values ('editbulkproducts', 'product');
INSERT INTO apilist (methodName, className) values ('deleteproduct', 'product');
INSERT INTO apilist (methodName, className) values ('filterproduct', 'product');
INSERT INTO apilist (methodName, className) values ('getadvancefilter', 'product');
INSERT INTO apilist (methodName, className) values ('getallbrands', 'product');
INSERT INTO apilist (methodName, className) values ('getallfields', 'product');
INSERT INTO apilist (methodName, className) values ('setselectedfields', 'product');
INSERT INTO apilist (methodName, className) values ('getsortedfields', 'product');

INSERT INTO apilist (methodName, className) values ('getproductinventory', 'product');
INSERT INTO apilist (methodName, className) values ('getpageproductinventory', 'product');
INSERT INTO apilist (methodName, className) values ('getproductinventorycount', 'product');

INSERT INTO apilist (methodName, className) values ('getallorders', 'order');
INSERT INTO apilist (methodName, className) values ('getpageorders', 'order');
INSERT INTO apilist (methodName, className) values ('getordercount', 'order');

INSERT INTO apilist (methodName, className) values ('getmanagemeta', 'role');
INSERT INTO apilist (methodName, className) values ('getroleslist', 'role');
INSERT INTO apilist (methodName, className) values ('createrole', 'role');
INSERT INTO apilist (methodName, className) values ('updaterole', 'role');
INSERT INTO apilist (methodName, className) values ('storeroles', 'role');
INSERT INTO apilist (methodName, className) values ('getassignedroles', 'role');
INSERT INTO apilist (methodName, className) values ('getpermissions', 'role');

CREATE TABLE apicontrol (
    apiId INT UNSIGNED NOT NULL,
    featureId INT UNSIGNED NOT NULL,
    subfeatureId INT UNSIGNED,
    reqFeatureVal TINYINT UNSIGNED NOT NULL,
    reqSubfeatureVal TINYINT UNSIGNED,
    PRIMARY KEY (apiId , featureId),
    UNIQUE (apiId, featureId , subfeatureId),
    FOREIGN KEY (apiId)
        REFERENCES apilist (apiId),
    FOREIGN KEY (featureId)
        REFERENCES features (id),
    FOREIGN KEY (subfeatureId)
        REFERENCES subfeatures (subfeatureId)
);
#User Management (General)
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (2, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (3, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (4, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (5, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (6, 6, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (7, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (8, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (9, 6, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (10, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (11, 6, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (12, 6, null, 3, null);

#Dashboard
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (13, 1, null, 2, null);

#Sales
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (14, 3, null, 2, null);

#ASN
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (15, 8, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (16, 8, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (17, 8, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (18, 8, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (19, 8, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (20, 8, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (21, 8, null, 3, null);

#Product-Catalog
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (22, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (23, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (24, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (25, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (26, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (27, 2, 2, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (28, 2, 1, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (29, 2, 1, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (30, 2, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (31, 2, 1, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (32, 2, 1, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (33, 2, 1, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (34, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (35, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (36, 2, null, 2, null);

#Inventory
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (37, 5, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (38, 5, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (39, 5, null, 2, null);

#Order
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (40, 4, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (41, 4, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (42, 4, null, 2, null);

#User Management (Roles)
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (43, 6, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (44, 6, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (45, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (46, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (47, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (48, 6, null, 2, null);


#3.7.18
drop table caseitems;
drop table cases;
drop table asn;

alter table storeusermapper
add constraint unique(sellerId, storeId);


#4.7.18
CREATE TABLE orderreturn (
    orderReturnId VARCHAR(32) NOT NULL,
    orderId VARCHAR(32) NOT NULL,
    storeId INT UNSIGNED NOT NULL,
    product_id INT UNSIGNED NOT NULL,
    returnedQuantity INT UNSIGNED NOT NULL,
    returnStatus VARCHAR(32) NOT NULL,
    refundAmount FLOAT UNSIGNED NOT NULL,
    returnRequestedOn TIMESTAMP NOT NULL DEFAULT now(),
    reasonForReturn VARCHAR(1024) NOT NULL,
    additionalComments VARCHAR(1024),
    PRIMARY KEY (orderReturnId , orderId , storeId),
    FOREIGN KEY (orderId , storeId , product_id)
        REFERENCES orderlist (orderId , storeId , product_id)
);

CREATE TABLE asn (
    asnId INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    asnNumber VARCHAR(16)BINARY NOT NULL UNIQUE,
    sellerId INT UNSIGNED NOT NULL,
    storeId INT UNSIGNED NOT NULL,
    asnStatus VARCHAR(16) NOT NULL DEFAULT 'Inprogress',
    asnType VARCHAR(16) NOT NULL,
    shipmentType VARCHAR(16) NOT NULL,
    deliveryFacility VARCHAR(64),
    referenceNumber VARCHAR(64),
    bolNumber VARCHAR(64),
    carrier VARCHAR(128),
    shippingMethod VARCHAR(64) NOT NULL,
    trackingNumber VARCHAR(64),
    shipmentDate TIMESTAMP NULL DEFAULT NULL,
    estimatedArrival TIMESTAMP NULL DEFAULT NULL,
    modeofTransport VARCHAR(32),
    shipmentFrom VARCHAR(32),
    cogi FLOAT UNSIGNED,
    additionalNotes VARCHAR(1024),
    asnDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    createdBy VARCHAR(32)BINARY NOT NULL,
    createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modifiedBy VARCHAR(32)BINARY NOT NULL,
    modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (sellerId , storeId)
        REFERENCES storeusermapper (sellerId , storeId)
);
CREATE TABLE asnpalletlevel (
    palletId INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    asnId INT UNSIGNED NOT NULL,
    palletLpn VARCHAR(64) NOT NULL,
    palletLength FLOAT UNSIGNED,
    palletWidth FLOAT UNSIGNED,
    palletHeight FLOAT UNSIGNED,
    palletDimensionUOM VARCHAR(16),
    palletWeight FLOAT UNSIGNED,
    palletWeightUOM VARCHAR(16),
    UNIQUE (asnId , palletLpn),
    FOREIGN KEY (asnId)
        REFERENCES asn (asnId)
);
CREATE TABLE asncaselevel (
    caseId INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    asnId INT UNSIGNED NOT NULL,
    palletId INT UNSIGNED,
    caseLpn VARCHAR(64) NOT NULL,
    caseLength FLOAT UNSIGNED,
    caseWidth FLOAT UNSIGNED,
    caseHeight FLOAT UNSIGNED,
    caseDimensionUOM VARCHAR(16),
    caseWeight FLOAT UNSIGNED,
    caseWeightUOM VARCHAR(16),
    specialTreatmentNeeded VARCHAR(3),
    isMaterialHazardous VARCHAR(3),
    UNIQUE (asnId , caseLpn),
    FOREIGN KEY (asnId)
        REFERENCES asn (asnId),
    FOREIGN KEY (palletId)
        REFERENCES asnpalletlevel (palletId)
);
CREATE TABLE asnskulevel (
    id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    asnId INT UNSIGNED NOT NULL,
    palletId INT UNSIGNED,
    caseId INT UNSIGNED NOT NULL,
    product_id INT UNSIGNED NOT NULL,
    quantity INT UNSIGNED NOT NULL,
    status VARCHAR(16),
    UNIQUE (caseId , product_id),
    FOREIGN KEY (asnId)
        REFERENCES asn (asnId),
    FOREIGN KEY (palletId)
        REFERENCES asnpalletlevel (palletId),
    FOREIGN KEY (caseId)
        REFERENCES asncaselevel (caseId),
    FOREIGN KEY (product_id)
        REFERENCES productmapper (product_id)
);

##Role Actions for Orders Page

UPDATE `sellerportal`.`features` SET `view_edit`='0' WHERE `id`='4';
insert into subfeatures (id,feature,none,view,view_edit) values(2,"Order Management",1,1,0) ;
insert into subfeatures (id,feature,none,view,view_edit) values(2,"Return Management",1,1,0) ;
UPDATE `sellerportal`.`features` SET `sub_feature_id`='2' WHERE `id`='4';


#6.7.18 - 03:35PM
alter table roles
add column description varchar(512) not null after storeId;
update roles set description = name;


#6.7.18 - 06:00PM
alter table orderlist
add column price FLOAT UNSIGNED NOT NULL after quantity;

UPDATE orderlist
INNER JOIN productvaluemaster ON orderlist.product_id = productvaluemaster.product_id
SET orderlist.price = CAST(productvaluemaster.value AS DECIMAL(10,2))
WHERE productvaluemaster.field_id = 9;


#9.7.18 - 12:15PM
alter table users add column rp_token varchar(256) default null after password;
alter table users add column rp_token_created_at timestamp default 0 after rp_token ;


#11.7.18 - 1:15PM
delete from apicontrol where apiId = (select apiId from apilist where methodName = 'getalluserstatus');
delete from apilist where methodName = 'getalluserstatus';


#12.7.18 - 4:10PM
alter table users drop column rp_token;
alter table users drop column rp_token_created_at;

CREATE TABLE forgotpassword (
    sellerId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    jti VARCHAR(36)BINARY NOT NULL UNIQUE,
    FOREIGN KEY (sellerId)
        REFERENCES users (sellerId)
);


#13.7.18 - 12:40PM
INSERT INTO productinventory (product_id, availableToPromise, fulfillableStock, damagedStock, reservedStock)
SELECT product_id, 0, 0, 0, 0 FROM productmapper
WHERE product_id NOT IN (SELECT product_id FROM productinventory);

#13.7.18 - 4:20PM
alter table fieldmaster
add column issearchable BOOLEAN NOT NULL default 0 after ismandotry;

UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='1';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='2';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='3';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='4';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='5';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='6';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='7';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='89';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='15';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='19';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='27';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='29';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='78';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='79';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='41';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='43';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='44';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='47';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='48';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='88';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='87';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='63';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='64';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='65';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='66';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='67';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='68';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='69';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='70';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='71';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='72';


#16.7.18 - 5:00PM
CREATE TABLE productmanageprice (
    product_id INT UNSIGNED NOT NULL,
    price FLOAT UNSIGNED NOT NULL,
    scheduled_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (product_id , scheduled_at),
    UNIQUE (product_id , scheduled_at),
    FOREIGN KEY (product_id)
        REFERENCES productmapper (product_id)
);

INSERT INTO productmanageprice (product_id, price, scheduled_at)
SELECT product_id, 0, 0 from productmapper;

UPDATE productmanageprice
INNER JOIN productvaluemaster ON productmanageprice.product_id = productvaluemaster.product_id
SET productmanageprice.price = CAST(productvaluemaster.value AS DECIMAL(10,2))
WHERE productvaluemaster.field_id = 9;

UPDATE productmanageprice
INNER JOIN productvaluemaster ON productmanageprice.product_id = productvaluemaster.product_id
SET productmanageprice.scheduled_at = FROM_UNIXTIME(CAST(productvaluemaster.value AS DECIMAL(15))/1000)
WHERE productvaluemaster.field_id = 74;


#17.7.18 - 6:45PM
alter table apilist auto_increment = 50;

INSERT INTO apilist (methodName, className) values ('getpageadvancefilter', 'product');
INSERT INTO apilist (methodName, className) values ('getpageadvancefilterdownload', 'product');
INSERT INTO apilist (methodName, className) values ('getadvancefiltercount', 'product');

INSERT INTO apilist (methodName, className) values ('forgotpassword', 'general');
INSERT INTO apilist (methodName, className) values ('createpassword', 'general');
INSERT INTO apilist (methodName, className) values ('checktokenexpiry', 'general');

INSERT INTO apilist (methodName, className) values ('getallreturns', 'order');
INSERT INTO apilist (methodName, className) values ('getpagereturns', 'order');
INSERT INTO apilist (methodName, className) values ('getreturncount', 'order');

INSERT INTO apilist (methodName, className) values ('getskuwithname', 'asn');
INSERT INTO apilist (methodName, className) values ('getpageasn', 'asn');
INSERT INTO apilist (methodName, className) values ('getasncount', 'asn');

INSERT INTO apilist (methodName, className) values ('getsearchproducts', 'product');
INSERT INTO apilist (methodName, className) values ('getpagesearchproducts', 'product');
INSERT INTO apilist (methodName, className) values ('getpagesearchproductsdownload', 'product');
INSERT INTO apilist (methodName, className) values ('getsearchproductcount', 'product');
INSERT INTO apilist (methodName, className) values ('getmanageprice', 'product');
INSERT INTO apilist (methodName, className) values ('getpagemanageprice', 'product');
INSERT INTO apilist (methodName, className) values ('getsearchmanageprice', 'product');
INSERT INTO apilist (methodName, className) values ('getpagesearchmanageprice', 'product');
INSERT INTO apilist (methodName, className) values ('getsearchmanagepricecount', 'product');
INSERT INTO apilist (methodName, className) values ('updatemanageprice', 'product');


#18.7.18 - 11:30AM
insert into subfeatures (id,feature,none,view,view_edit) values(1,"Catalog Management",1,1,1);
insert into subfeatures (id,feature,none,view,view_edit) values(1,"Pricing Management",1,1,1);

insert into selectedsubvalues (values_id, feature, selected, none, view, view_edit)
select values_id, 'Return Management', 1, 1, 1, 0 from selectedvalues
WHERE feature = 'Orders Dashboard' AND (values_id, 'Return Management') NOT IN (SELECT values_id, feature FROM selectedsubvalues);

insert into selectedsubvalues (values_id, feature, selected, none, view, view_edit)
select values_id, 'Order Management', 1, 1, 1, 0 from selectedvalues
WHERE feature = 'Orders Dashboard' AND (values_id, 'Order Management') NOT IN (SELECT values_id, feature FROM selectedsubvalues);

insert into selectedsubvalues (values_id, feature, selected, none, view, view_edit)
select values_id, 'Catalog Management', 1, 1, 1, 1 from selectedvalues
WHERE feature = 'Product Catalog' AND (values_id, 'Catalog Management') NOT IN (SELECT values_id, feature FROM selectedsubvalues);

insert into selectedsubvalues (values_id, feature, selected, none, view, view_edit)
select values_id, 'Pricing Management', 1, 1, 1, 1 from selectedvalues
WHERE feature = 'Product Catalog' AND (values_id, 'Pricing Management') NOT IN (SELECT values_id, feature FROM selectedsubvalues);


UPDATE `sellerportal`.`apicontrol` SET `reqSubfeatureVal`='2' WHERE `apiId`='40' and`featureId`='4';
UPDATE `sellerportal`.`apicontrol` SET `reqSubfeatureVal`='2' WHERE `apiId`='41' and`featureId`='4';
UPDATE `sellerportal`.`apicontrol` SET `reqSubfeatureVal`='2' WHERE `apiId`='42' and`featureId`='4';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='3' WHERE `apiId`='40' and`featureId`='4';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='3' WHERE `apiId`='41' and`featureId`='4';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='3' WHERE `apiId`='42' and`featureId`='4';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='5', `reqSubfeatureVal`='2' WHERE `apiId`='22' and`featureId`='2';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='5', `reqSubfeatureVal`='2' WHERE `apiId`='23' and`featureId`='2';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='5', `reqSubfeatureVal`='2' WHERE `apiId`='24' and`featureId`='2';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='5', `reqSubfeatureVal`='2' WHERE `apiId`='25' and`featureId`='2';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='5', `reqSubfeatureVal`='2' WHERE `apiId`='26' and`featureId`='2';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='5', `reqSubfeatureVal`='3' WHERE `apiId`='30' and`featureId`='2';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='5', `reqSubfeatureVal`='2' WHERE `apiId`='34' and`featureId`='2';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='5', `reqSubfeatureVal`='2' WHERE `apiId`='35' and`featureId`='2';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='5', `reqSubfeatureVal`='2' WHERE `apiId`='36' and`featureId`='2';

INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (50, 2, 1, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (51, 2, 1, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (52, 2, 1, 3, 3);

INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (56, 4, 4, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (57, 4, 4, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (58, 4, 4, 2, 2);

INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (59, 8, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (60, 8, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (61, 8, null, 2, null);

INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (62, 2, 5, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (63, 2, 5, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (64, 2, 5, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (65, 2, 5, 2, 2);

INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (66, 2, 6, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (67, 2, 6, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (68, 2, 6, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (69, 2, 6, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (70, 2, 6, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (71, 2, 6, 3, 3);


#19.7.18 - 10:50AM
alter table users add column isCreatedByVert boolean after firstlogin;
update users set isCreatedByVert = 1 where createdBy = 'admin';
update users set isCreatedByVert = 0 where createdBy <> 'admin';

#19.7.18 - 08:40AM
alter table orders
modify column orderStatus ENUM('Processing', 'Partially Shipped', 'Shipped', 'Delivered', 'Cancelled') DEFAULT 'Processing';


#1.8.18 - 11:10AM
delete from apicontrol where apiId = (select apiId from apilist where methodName = 'getuserdetails');


#2.8.18 - 06:00AM
alter table asn modify shipmentFrom VARCHAR(1024);
alter table asn drop column shippingMethod;
alter table asn drop column modeofTransport;

alter table asn modify asnStatus VARCHAR(16) NOT NULL DEFAULT 'Incomplete';
update asn set asnStatus = 'Incomplete';

alter table asn add column sellerName varchar(64) after sellerId;
UPDATE asn AS t1 INNER JOIN users AS t2 ON t1.sellerId = t2.sellerId
SET t1.sellerName = t2.name;

alter table asn add column sellerEmail varchar(80) after sellerName;
UPDATE asn AS t1 INNER JOIN users AS t2 ON t1.sellerId = t2.sellerId
SET t1.sellerEmail = t2.email;

alter table asn add column sellerPhone varchar(16) after sellerEmail;
UPDATE asn AS t1 INNER JOIN users AS t2 ON t1.sellerId = t2.sellerId
SET t1.sellerPhone = t2.phoneNumber;


alter table asnskulevel modify caseId INT UNSIGNED;

alter table asnskulevel add column sku VARCHAR(64) after product_id;
UPDATE asnskulevel AS t1 INNER JOIN productvaluemaster AS t2 ON t1.product_id = t2.product_id
SET t1.sku = t2.value where field_id = 1;

alter table asnskulevel add column product_name VARCHAR(64) after sku;
UPDATE asnskulevel AS t1 INNER JOIN productvaluemaster AS t2 ON t1.product_id = t2.product_id
SET t1.product_name = t2.value where field_id = 5;

alter table asnskulevel add column product_length FLOAT UNSIGNED after quantity;
UPDATE asnskulevel AS t1 INNER JOIN productvaluemaster AS t2 ON t1.product_id = t2.product_id
SET t1.product_length = cast(t2.value as decimal) where field_id = 16;

alter table asnskulevel add column product_width FLOAT UNSIGNED after product_length;
UPDATE asnskulevel AS t1 INNER JOIN productvaluemaster AS t2 ON t1.product_id = t2.product_id
SET t1.product_width = cast(t2.value as decimal) where field_id = 17;

alter table asnskulevel add column product_height FLOAT UNSIGNED after product_width;
UPDATE asnskulevel AS t1 INNER JOIN productvaluemaster AS t2 ON t1.product_id = t2.product_id
SET t1.product_height = cast(t2.value as decimal) where field_id = 18;

alter table asnskulevel add column dimensions_UOM VARCHAR(16) after product_height;
UPDATE asnskulevel AS t1 INNER JOIN productvaluemaster AS t2 ON t1.product_id = t2.product_id
SET t1.dimensions_UOM = t2.value where field_id = 23;

alter table asnskulevel add column weight FLOAT UNSIGNED after dimensions_UOM;
UPDATE asnskulevel AS t1 INNER JOIN productvaluemaster AS t2 ON t1.product_id = t2.product_id
SET t1.weight = cast(t2.value as decimal) where field_id = 8;

alter table asnskulevel add column weight_UOM VARCHAR(16) after weight;
UPDATE asnskulevel AS t1 INNER JOIN productvaluemaster AS t2 ON t1.product_id = t2.product_id
SET t1.weight_UOM = t2.value where field_id = 22;


#7.8.18 - 11:15AM
ALTER TABLE users CHANGE firstlogin isActivated BOOLEAN NOT NULL DEFAULT 0 AFTER sellerStatus;
ALTER TABLE users ADD COLUMN firstlogin BOOLEAN NOT NULL DEFAULT 1 AFTER isActivated;
UPDATE users SET firstlogin = 0 WHERE tokenIssuedAt <> 0;

ALTER TABLE stores ADD COLUMN storeStatus ENUM('ACTIVE', 'INACTIVE') DEFAULT 'INACTIVE' AFTER storeName;
ALTER TABLE stores ADD COLUMN isActivated BOOLEAN DEFAULT 0 AFTER storeStatus;

UPDATE stores T1 SET storeStatus = (SELECT sellerStatus FROM users T2 WHERE T2.sellerId = 
(SELECT T3.sellerId FROM storeusermapper T3 JOIN users T2 ON T2.sellerId = T3.sellerId 
WHERE T1.storeId = T3.storeId AND T2.isCreatedByVert = 1));
UPDATE stores SET storeStatus = 'INACTIVE' WHERE stores.storeStatus IS NULL;

UPDATE stores T1 SET isActivated = (SELECT isActivated FROM users T2 WHERE T2.sellerId = 
(SELECT T3.sellerId FROM storeusermapper T3 JOIN users T2 ON T2.sellerId = T3.sellerId 
WHERE T1.storeId = T3.storeId AND T2.isCreatedByVert = 1));
UPDATE stores SET isActivated = 0 WHERE stores.isActivated IS NULL;

ALTER TABLE stores ADD COLUMN allowedUsersCount INT UNSIGNED NOT NULL DEFAULT 5 AFTER storeName;

#7.8.18 - 01:30PM
##Users
UPDATE users SET sellerStatus = 'INACTIVE' WHERE users.sellerStatus IS NULL;
ALTER TABLE users MODIFY sellerStatus ENUM('ACTIVE', 'INACTIVE') NOT NULL DEFAULT 'INACTIVE';

UPDATE users SET isCreatedByVert = 0 WHERE users.isCreatedByVert IS NULL;
ALTER TABLE users MODIFY isCreatedByVert BOOLEAN NOT NULL DEFAULT 0;

UPDATE users SET phoneNumber =  null WHERE phoneNumber = '';
UPDATE users SET phoneNumber =  LPAD(FLOOR(RAND() * 10000000000), 10, '0') WHERE phoneNumber IS NULL AND isCreatedByVert = 1;
UPDATE users SET phoneNumber = LPAD(FLOOR(RAND() * 10000000000), 10, '0') WHERE phoneNumber IN (
SELECT phoneNumber FROM (SELECT phoneNumber FROM users where isCreatedByVert = 1) AS T1
GROUP BY T1.phoneNumber HAVING COUNT(*) > 1);

ALTER TABLE `sellerportal`.`users` 
DROP INDEX `email`;

##Stores
alter table stores drop column stores.applicationStatus;

set foreign_key_checks=0;
delete from stores where storeId not in (select distinct storeId from storeusermapper);
set foreign_key_checks=1;

ALTER TABLE stores ADD COLUMN activeFrom TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER isActivated;
ALTER TABLE stores ADD COLUMN activeTo TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER activeFrom;

UPDATE stores T1 SET activeFrom = (SELECT activeFrom FROM users T2 WHERE T2.sellerId = 
(SELECT T3.sellerId FROM storeusermapper T3 JOIN users T2 ON T2.sellerId = T3.sellerId 
WHERE T1.storeId = T3.storeId AND T2.isCreatedByVert = 1));

UPDATE stores T1 SET activeTo = (SELECT activeTo FROM users T2 WHERE T2.sellerId = 
(SELECT T3.sellerId FROM storeusermapper T3 JOIN users T2 ON T2.sellerId = T3.sellerId 
WHERE T1.storeId = T3.storeId AND T2.isCreatedByVert = 1));

alter table users drop column activeFrom;
alter table users drop column activeTo;

ALTER TABLE stores ADD COLUMN email VARCHAR(80) AFTER storeName;
ALTER TABLE stores ADD COLUMN phoneNumber VARCHAR(16) AFTER email;

UPDATE stores T1 SET email = (SELECT email FROM users T2 WHERE T2.sellerId = 
(SELECT T3.sellerId FROM storeusermapper T3 JOIN users T2 ON T2.sellerId = T3.sellerId 
WHERE T1.storeId = T3.storeId AND T2.isCreatedByVert = 1));

UPDATE stores T1 SET phoneNumber = (SELECT phoneNumber FROM users T2 WHERE T2.sellerId = 
(SELECT T3.sellerId FROM storeusermapper T3 JOIN users T2 ON T2.sellerId = T3.sellerId 
WHERE T1.storeId = T3.storeId AND T2.isCreatedByVert = 1));

ALTER TABLE stores MODIFY email VARCHAR(80) NOT NULL UNIQUE;
ALTER TABLE stores MODIFY phoneNumber VARCHAR(16) NOT NULL UNIQUE;


update stores set sellerCode = storeId where sellerCode is null;
alter table stores modify sellerCode VARCHAR(32) not null;

UPDATE stores SET storeStatus = 'INACTIVE' WHERE storeStatus IS NULL;
ALTER TABLE stores MODIFY storeStatus ENUM('ACTIVE', 'INACTIVE') NOT NULL DEFAULT 'INACTIVE';

UPDATE stores SET isActivated = 0 WHERE isActivated IS NULL;
ALTER TABLE stores MODIFY isActivated boolean NOT NULL DEFAULT 0;

alter table stores modify revenue VARCHAR(64) after imagePath;
alter table stores modify modeOfContact VARCHAR(8) after revenue;
alter table stores modify timeOfContact VARCHAR(64) after modeOfContact;

update stores set modeOfContact = 'Email' where modeOfContact = 'mail';

update stores set fulfillment = 'Verte' where fulfillment is null;
alter table stores modify fulfillment varchar(64) not null;

update stores set imagePath = '' where imagePath is null;
alter table stores modify imagePath varchar(1024) not null;

update stores set preference = 'Both' where preference is null;
alter table stores modify preference varchar(32) not null;

update stores set maximumProductSku = 1000 where maximumProductSku is null;
alter table stores modify maximumProductSku int unsigned not null;

update stores set preference = 'Both' 
where preference <> binary'Microstore' AND preference <> binary'Marketplace' AND preference <> binary'Both';

UPDATE stores SET address = 'NO.1, WestCornerStreet' WHERE address is null;
alter table stores modify address varchar(80) not null;

UPDATE stores SET zipCode = 55555 WHERE zipCode is null;
alter table stores modify zipCode varchar(5) not null;

UPDATE stores SET city = 'Young America'  WHERE city is null;
alter table stores modify city varchar(32) not null;

UPDATE stores SET state ='Minnesota'  WHERE state is null;
alter table stores modify state varchar(32) not null;

UPDATE stores SET country = 'United States of America'  WHERE country is null;
alter table stores modify country varchar(32) not null;

alter table users add column isFirstUser boolean not null default 0 after isCreatedByVert;
update users set isFirstUser = isCreatedByVert;


#10.8.18 - 01:30PM
update users set sellerStatus = 'INACTIVE' where isFirstUser = 0;
update users set sellerStatus = 'ACTIVE' where isFirstUser = 1;

#10.8.18 - 05:20PM
drop table storebankdetails;
drop table storetaxdetails;


#13.8.18 - 04:50PM
UPDATE users SET phoneNumber = null WHERE phoneNumber IN (
SELECT phoneNumber FROM (SELECT phoneNumber FROM users) AS T1
GROUP BY T1.phoneNumber HAVING COUNT(*) > 1);

alter table users modify email varchar(80) not null unique;
alter table users modify phoneNumber varchar(16) unique;


#14.8.18 - 6.30PM
CREATE TABLE userrecentpassword (
    sellerId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    password1 VARCHAR(192)BINARY NOT NULL,
    password2 VARCHAR(192)BINARY NOT NULL,
    password3 VARCHAR(192)BINARY NOT NULL,
    FOREIGN KEY (sellerId)
        REFERENCES users (sellerId)
);

INSERT INTO userrecentpassword (sellerId, password1, password2, password3)
SELECT sellerId, password , password, password
FROM users ORDER BY sellerId ASC;

CREATE TABLE vertrecentpassword (
    userId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    password1 VARCHAR(192)BINARY NOT NULL,
    password2 VARCHAR(192)BINARY NOT NULL,
    password3 VARCHAR(192)BINARY NOT NULL,
    FOREIGN KEY (userId)
        REFERENCES vertadmin (userId)
);

INSERT INTO vertrecentpassword (userId, password1, password2, password3)
SELECT userId, password , password, password
FROM vertadmin ORDER BY userId ASC;

rename table forgotpassword to userpasswordjti;


UPDATE asnpalletlevel SET palletDimensionUOM = 'INCH';
alter table asnpalletlevel MODIFY COLUMN palletDimensionUOM ENUM('INCH','CM') DEFAULT 'INCH';

UPDATE asnpalletlevel SET palletWeightUOM = 'POUND';
alter table asnpalletlevel MODIFY COLUMN palletWeightUOM ENUM('POUND','KG') DEFAULT 'POUND';

UPDATE asncaselevel SET caseDimensionUOM = 'INCH';
alter table asncaselevel MODIFY COLUMN caseDimensionUOM ENUM('INCH','CM') DEFAULT 'INCH';

UPDATE asncaselevel SET caseWeightUOM = 'POUND';
alter table asncaselevel MODIFY COLUMN caseWeightUOM ENUM('POUND','KG') DEFAULT 'POUND';

UPDATE asnskulevel SET dimensions_UOM = 'INCH';
alter table asnskulevel MODIFY COLUMN dimensions_UOM ENUM('INCH','CM') DEFAULT 'INCH';

UPDATE asnskulevel SET weight_UOM = 'POUND';
alter table asnskulevel MODIFY COLUMN weight_UOM ENUM('POUND','KG') DEFAULT 'POUND';

delete from productvaluemaster where field_id = 21;
delete from productvaluemaster where field_id = 22;
delete from productvaluemaster where field_id = 37;
delete from productvaluemaster where field_id = 23;
delete from productvaluemaster where field_id = 90;

INSERT INTO productvaluemaster (product_id, field_id, value)
  SELECT product_id, 21, 'CU IN'
  FROM productmapper;

INSERT INTO productvaluemaster (product_id, field_id, value)
  SELECT product_id, 22, 'POUND'
  FROM productmapper;

INSERT INTO productvaluemaster (product_id, field_id, value)
  SELECT product_id, 37, 'POUND'
  FROM productmapper;

INSERT INTO productvaluemaster (product_id, field_id, value)
  SELECT product_id, 23, 'INCH'
  FROM productmapper;

INSERT INTO productvaluemaster (product_id, field_id, value)
  SELECT product_id, 90, 'INCH'
  FROM productmapper;


#16.8.18 - 12.05PM
CREATE TABLE userlogindetails (
    sellerId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    failedLoginCount INT UNSIGNED NOT NULL default 0,
    lastLoginAttemptAt TIMESTAMP NULL DEFAULT NULL,
    lastLoginAt TIMESTAMP NULL DEFAULT NULL,
    lastLoginFailedAt TIMESTAMP NULL DEFAULT NULL,
    accountLockedAt TIMESTAMP NULL DEFAULT NULL,
    FOREIGN KEY (sellerId)
        REFERENCES users (sellerId)
);
INSERT INTO userlogindetails (sellerId, failedLoginCount, lastLoginAttemptAt, lastLoginAt, lastLoginFailedAt, accountLockedAt)
SELECT sellerId, 0, NULL, NULL, NULL, NULL
FROM users ORDER BY sellerId ASC;

UPDATE userlogindetails T1 JOIN users T2 ON T1.sellerId = T2.sellerId
SET lastLoginAttemptAt = T2.tokenIssuedAt, lastLoginAt = T2.tokenIssuedAt
WHERE T2.tokenIssuedAt <> 0;

#16.8.18 - 04.05PM
alter table contactvert drop column applicationStatus;

#18.8.18 - 1.45PM
alter table asnpalletlevel MODIFY COLUMN palletWeightUOM ENUM('POUND') DEFAULT 'POUND';
alter table asnpalletlevel MODIFY COLUMN palletDimensionUOM ENUM('INCH') DEFAULT 'INCH';
alter table asncaselevel MODIFY COLUMN caseWeightUOM ENUM('POUND') DEFAULT 'POUND';
alter table asncaselevel MODIFY COLUMN caseDimensionUOM ENUM('INCH') DEFAULT 'INCH';
alter table asnskulevel MODIFY COLUMN weight_UOM ENUM('POUND') DEFAULT 'POUND';
alter table asnskulevel MODIFY COLUMN dimensions_UOM ENUM('INCH') DEFAULT 'INCH';

#18.8.18 - 4.45PM
DELETE FROM vertrecentpassword WHERE userId IN (SELECT userId FROM vertadmin WHERE userName <> 'admin');
DELETE FROM vertadmin WHERE userName <> 'admin';

ALTER TABLE vertadmin ADD COLUMN vertuserStatus ENUM('ACTIVE', 'INACTIVE') NOT NULL DEFAULT 'INACTIVE' AFTER userType;
ALTER TABLE vertadmin ADD COLUMN isActivated BOOLEAN NOT NULL DEFAULT 0 AFTER vertuserStatus;
ALTER TABLE vertadmin ADD COLUMN firstlogin BOOLEAN NOT NULL DEFAULT 1 AFTER isActivated;
ALTER TABLE vertadmin ADD COLUMN isFirstUser BOOLEAN NOT NULL DEFAULT 0 AFTER firstlogin;

UPDATE vertadmin SET vertuserStatus = 'ACTIVE', isActivated = 1, firstlogin = 0, isFirstUser = 1 WHERE userName = 'admin';
UPDATE vertadmin SET email = 'vertadmin@projectverte.com', phoneNumber = '9999888776' WHERE userName = 'admin';

#18.8.18 - 5.20PM
CREATE TABLE vertfeatures (
    feature_id INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    feature VARCHAR(64) NOT NULL UNIQUE,
    title VARCHAR(64) NOT NULL,
    parent_id INT UNSIGNED,
    none INT UNSIGNED NOT NULL,
    view INT UNSIGNED NOT NULL,
    view_edit INT UNSIGNED NOT NULL,
    FOREIGN KEY (parent_id)
        REFERENCES vertfeatures (feature_id)
);
#insert all features of vertadmin login
insert into vertfeatures values(1, 'sellers', 'Sellers', null, 1, 1, 1);
insert into vertfeatures values(2, 'admins', 'Admins', null, 1, 1, 1);
insert into vertfeatures values(3, 'catagory', 'Catagory', null, 1, 1, 1);

CREATE TABLE vertroles (
    role_id INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
    role_name VARCHAR(64) NOT NULL UNIQUE,
    description VARCHAR(256) NULL,
    createdBy VARCHAR(32)BINARY NOT NULL,
    createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modifiedBy VARCHAR(32)BINARY NOT NULL,
    modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
CREATE TABLE vertselectedfeatures (
    id INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
    role_id INT UNSIGNED NOT NULL,
    feature_id INT UNSIGNED NOT NULL,
    selected_value INT UNSIGNED NOT NULL,
    UNIQUE (role_id , feature_id),
    FOREIGN KEY (role_id)
        REFERENCES vertroles (role_id),
    FOREIGN KEY (feature_id)
        REFERENCES vertfeatures (feature_id)
);
CREATE TABLE vertassignedroles (
    userId INT UNSIGNED NOT NULL,
    role_id INT UNSIGNED NOT NULL,
    UNIQUE (userId , role_id),
    FOREIGN KEY (userId)
        REFERENCES vertadmin (userId),
    FOREIGN KEY (role_id)
        REFERENCES vertroles (role_id)
);

CREATE TABLE vertpasswordjti (
    userId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    jti VARCHAR(36)BINARY NOT NULL UNIQUE,
    FOREIGN KEY (userId)
        REFERENCES vertadmin (userId)
);
CREATE TABLE vertlogindetails (
    userId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    failedLoginCount INT UNSIGNED NOT NULL default 0,
    lastLoginAttemptAt TIMESTAMP NULL DEFAULT NULL,
    lastLoginAt TIMESTAMP NULL DEFAULT NULL,
    lastLoginFailedAt TIMESTAMP NULL DEFAULT NULL,
    accountLockedAt TIMESTAMP NULL DEFAULT NULL,
    FOREIGN KEY (userId)
        REFERENCES vertadmin (userId)
);
INSERT INTO vertlogindetails (userId, failedLoginCount, lastLoginAttemptAt, lastLoginAt, lastLoginFailedAt, accountLockedAt)
SELECT userId, 0, NULL, NULL, NULL, NULL
FROM vertadmin ORDER BY userId ASC;

UPDATE vertlogindetails T1 JOIN vertadmin T2 ON T1.userId = T2.userId
SET lastLoginAttemptAt = T2.tokenIssuedAt, lastLoginAt = T2.tokenIssuedAt
WHERE T2.tokenIssuedAt <> 0;

#21.8.18 - 4.20PM
CREATE TABLE vertapilist (
    apiId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    resourceName VARCHAR(64)BINARY NOT NULL,
    methodName VARCHAR(64)BINARY NOT NULL,
    verb ENUM('GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'OPTIONS', 'HEAD') NOT NULL,
    UNIQUE (resourceName , methodName , verb)
);
insert into vertapilist (apiId, resourceName , methodName , verb) values(1, '', 'vertadminlogin', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(2, '', 'getvertpermission', 'GET');
insert into vertapilist (apiId, resourceName , methodName , verb) values(3, '', 'checktokenexpiry', 'GET');
insert into vertapilist (apiId, resourceName , methodName , verb) values(4, '', 'createpassword', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(5, '', 'forgotpassword', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(6, '', 'resetpassword', 'PUT');
insert into vertapilist (apiId, resourceName , methodName , verb) values(7, '', 'resendpasswordlink', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(8, '', 'getmetadata', 'GET');
insert into vertapilist (apiId, resourceName , methodName , verb) values(9, '', 'getallroles', 'GET');
insert into vertapilist (apiId, resourceName , methodName , verb) values(10, '', 'createrole', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(11, '', 'updaterole', 'PUT');
insert into vertapilist (apiId, resourceName , methodName , verb) values(12, '', 'getalladmins', 'GET');
insert into vertapilist (apiId, resourceName , methodName , verb) values(13, '', 'createvertadmin', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(14, '', 'updatevertadmin', 'PUT');
insert into vertapilist (apiId, resourceName , methodName , verb) values(15, '', 'updatevertuserstatus', 'PUT');
insert into vertapilist (apiId, resourceName , methodName , verb) values(16, '', 'checkfieldvert', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(17, '', 'getallsellers', 'GET');
insert into vertapilist (apiId, resourceName , methodName , verb) values(18, '', 'getawaitingsellers', 'GET');
insert into vertapilist (apiId, resourceName , methodName , verb) values(19, '', 'getactivesellers', 'GET');
insert into vertapilist (apiId, resourceName , methodName , verb) values(20, '', 'getinactivesellers', 'GET');
insert into vertapilist (apiId, resourceName , methodName , verb) values(21, '', 'createseller', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(22, '', 'updateseller', 'PUT');
insert into vertapilist (apiId, resourceName , methodName , verb) values(23, '', 'updatestorestatus', 'PUT');
insert into vertapilist (apiId, resourceName , methodName , verb) values(24, '', 'updateuserstatus', 'PUT');
insert into vertapilist (apiId, resourceName , methodName , verb) values(25, '', 'checkfieldstore', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(26, '', 'checkfielduser', 'POST');
insert into vertapilist (apiId, resourceName , methodName , verb) values(27, '', 'getaddress', 'POST');

CREATE TABLE vertapicontrol (
    apiId INT UNSIGNED NOT NULL,
    featureId INT UNSIGNED NOT NULL,
    reqFeatureVal INT UNSIGNED NOT NULL,
    PRIMARY KEY (apiId , featureId),
    FOREIGN KEY (apiId)
        REFERENCES vertapilist (apiId),
    FOREIGN KEY (featureId)
        REFERENCES vertfeatures (feature_id)
);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(8, 2, 2);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(9, 2, 2);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(10, 2, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(11, 2, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(12, 2, 2);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(13, 2, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(14, 2, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(15, 2, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(16, 2, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(17, 1, 2);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(18, 1, 2);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(19, 1, 2);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(20, 1, 2);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(21, 1, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(22, 1, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(23, 1, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(24, 1, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(25, 1, 3);
insert into vertapicontrol (apiId, featureId, reqFeatureVal) values(26, 1, 3);


#23.8.18 - 3.00PM
CREATE TABLE productcategory (
    categoryId INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    categoryName VARCHAR(64) NOT NULL,
    categoryLevel ENUM('PARENT', 'CHILD', 'SUB') NOT NULL,
    parentId INT UNSIGNED NOT NULL,
    UNIQUE (categoryName , parentId)
);
CREATE TABLE fulfillmentcategory (
    categoryId INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    categoryName VARCHAR(64) NOT NULL,
    UNIQUE (categoryName)
);

alter table stores modify sellerCode varchar(32) null;


#27.8.18 - 5.00PM
ALTER TABLE selectedfields ADD COLUMN listId INT UNSIGNED NOT NULL AFTER field_id;

INSERT INTO apilist (methodName, className) values ('resendpasswordlink', 'general');
INSERT INTO apilist (methodName, className) values ('getpageusers', 'general');
INSERT INTO apilist (methodName, className) values ('getpagesearchusers', 'general');
INSERT INTO apilist (methodName, className) values ('getsearchusercount', 'general');

INSERT INTO apilist (methodName, className) values ('gethomedashboarddetails', 'dashboard');

INSERT INTO apilist (methodName, className) values ('editasnshipment', 'asn');
INSERT INTO apilist (methodName, className) values ('getpagesearchasn', 'asn');
INSERT INTO apilist (methodName, className) values ('getsearchasncount', 'asn');

INSERT INTO apilist (methodName, className) values ('getpagesearchorders', 'order');
INSERT INTO apilist (methodName, className) values ('getsearchordercount', 'order');
INSERT INTO apilist (methodName, className) values ('getpagesearchreturns', 'order');
INSERT INTO apilist (methodName, className) values ('getsearchreturncount', 'order');

INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (73, 6, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (74, 6, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (75, 6, null, 2, null);

INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (76, 1, null, 2, null);

INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (77, 8, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (78, 8, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (79, 8, null, 2, null);

INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (80, 4, 4, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (81, 4, 4, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (82, 4, 4, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (83, 4, 4, 2, 2);


#28.8.18 - 1.45PM
UPDATE `sellerportal`.`fieldmaster` SET `ismandotry`='1' WHERE `field_id`='44';
UPDATE `sellerportal`.`fieldmaster` SET `ismandotry`='1' WHERE `field_id`='47';


#30.8.18 - 1.45PM
ALTER TABLE productmapper ADD COLUMN 
productStatus ENUM('Processing', 'Accepted', 'Rejected') DEFAULT 'Processing' NOT NULL AFTER storeId;

ALTER TABLE productmapper ADD COLUMN 
errorDescription VARCHAR(128) NULL AFTER productStatus;


#07.9.18 - 11.15AM
alter table stores modify storeStatus enum('ACTIVE','INACTIVE','WAITING','REJECTED') default 'WAITING';
alter table stores add column reasonForRejection varchar(128) null after storeStatus;

#07.9.18 - 07.15PM
INSERT INTO fieldmaster (name, title, datatype, length, default_selected, ismandotry, issearchable, has_possible_values, possible_values)
VALUES ('safety_stock_qty','Safety Stock', 'int', 8, false, true, false, false, null);
INSERT INTO fieldmaster (name, title, datatype, length, default_selected, ismandotry, issearchable, has_possible_values, possible_values)
VALUES ('distressed_stock_qty','Distressed Stock', 'int', 2, false, true, false, false, null);

UPDATE `sellerportal`.`fieldmaster` SET `ismandotry`='1' WHERE `field_id`='24';
UPDATE `sellerportal`.`fieldmaster` SET `ismandotry`='1' WHERE `field_id`='86';
UPDATE `sellerportal`.`fieldmaster` SET `ismandotry`='1' WHERE `field_id`='85';
UPDATE `sellerportal`.`fieldmaster` SET `ismandotry`='1' WHERE `field_id`='16';
UPDATE `sellerportal`.`fieldmaster` SET `ismandotry`='1' WHERE `field_id`='17';
UPDATE `sellerportal`.`fieldmaster` SET `ismandotry`='1' WHERE `field_id`='18';


#10.9.18 - 07.45PM
CREATE TABLE sellerdocuments (
    id INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    storeId INT UNSIGNED NOT NULL,
    documentName VARCHAR(32) NOT NULL,
    documentPath VARCHAR(512) NOT NULL,
    uploadedBy VARCHAR(32)BINARY NOT NULL,
    uploadedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (storeId , documentName),
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);


#11.9.18 - 09.45PM
insert into productvaluemaster 
(select product_id, 80, 'true' FROM productmapper where product_id not in 
(select product_id from productvaluemaster where field_id = 80));


#12.9.18 - 11.35AM
UPDATE `sellerportal`.`fieldmaster` SET `title`='Online Availability' WHERE `field_id`='80';

#12.9.18 - 06.35PM
alter table contactvert add column processed boolean not null default 0 after timeOfContact;

#14.9.18 - 06.25PM
alter table orders add column orderType VARCHAR(16) NOT NULL after sourceCode;
alter table orders add column paymentCurrency VARCHAR(6) after orderType;
alter table orders add column paymentMethod VARCHAR(16) after paymentCurrency;
alter table orders add column giftCard VARCHAR(16) after paymentMethod;
alter table orders add column giftCardCurrency VARCHAR(6) after giftCard;
alter table orders add column estimatedDeliveryDate TIMESTAMP DEFAULT NULL NULL after giftCardCurrency;
alter table orders add column vasCode VARCHAR(16) after estimatedDeliveryDate;
alter table orders add column vasPrice FLOAT UNSIGNED after vasCode;

alter table orders add column buyerId INT unsigned after vasPrice;

alter table orders add column billing_customerName VARCHAR(80) after buyerId;
alter table orders add column billing_email VARCHAR(80) after billing_customerName;
alter table orders add column billing_phoneNumber VARCHAR(16) after billing_email;
alter table orders add column billing_address1 VARCHAR(80) after billing_phoneNumber;
alter table orders add column billing_address2 VARCHAR(80) after billing_address1;
alter table orders add column billing_address3 VARCHAR(80) after billing_address2;
alter table orders add column billing_city VARCHAR(32) after billing_address3;
alter table orders add column billing_state VARCHAR(32) after billing_city;
alter table orders add column billing_country VARCHAR(32) after billing_state;
alter table orders add column billing_zipCode VARCHAR(5) after billing_country;

alter table orders add column shipping_customerName VARCHAR(80) after billing_zipCode;
alter table orders add column shipping_phoneNumber VARCHAR(16) after shipping_customerName;
alter table orders add column shipping_address1 VARCHAR(80) after shipping_phoneNumber;
alter table orders add column shipping_address2 VARCHAR(80) after shipping_address1;
alter table orders add column shipping_address3 VARCHAR(80) after shipping_address2;
alter table orders add column shipping_city VARCHAR(32) after shipping_address3;
alter table orders add column shipping_state VARCHAR(32) after shipping_city;
alter table orders add column shipping_country VARCHAR(32) after shipping_state;
alter table orders add column shipping_zipCode VARCHAR(5) after shipping_country;

alter table orders add column additionalField VARCHAR(16) after lastStatusUpdate;


CREATE TABLE buyer (
    buyerId INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    customerId INT UNSIGNED NULL,
    name VARCHAR(64) NOT NULL,
    email VARCHAR(80) NOT NULL UNIQUE,
    phoneNumber VARCHAR(16),
    gender ENUM('M', 'F'),
    address1 VARCHAR(80),
    address2 VARCHAR(80),
    address3 VARCHAR(80),
    city VARCHAR(32),
    state VARCHAR(32),
    country VARCHAR(32),
    zipCode VARCHAR(5),
    createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

alter table orders add constraint foreign key (buyerId) references buyer (buyerId);

alter table orderlist add column unitPrice FLOAT UNSIGNED NOT NULL after quantity;
alter table orderlist add column vasCode VARCHAR(16) after price;
alter table orderlist add column vasPrice FLOAT UNSIGNED after vasCode;
alter table orderlist add column estimatedDeliveryDate TIMESTAMP DEFAULT NULL NULL after vasPrice;
alter table orderlist add column additionalField VARCHAR(16) after estimatedDeliveryDate;


#18.9.18 - 01.45PM
alter table buyer add column sourceCode VARCHAR(16) NOT NULL after buyerId;
update buyer T1 join orders T2 on T1.buyerId = T2.buyerId set T1.sourceCode = T2.sourceCode;

#18.9.18 - 03.50PM
CREATE TABLE onboardingseller (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(64) NOT NULL,
    email VARCHAR(64) NOT NULL,
    storeName VARCHAR(64) NOT NULL,
    revenue VARCHAR(64) NOT NULL,
    interestedIn VARCHAR(64) NOT NULL,
    maximumProductSku INT UNSIGNED NOT NULL,
    address VARCHAR(80) NOT NULL,
    city VARCHAR(32) NOT NULL,
    state VARCHAR(32) NOT NULL,
    country VARCHAR(32) NOT NULL,
    zipCode VARCHAR(5) NOT NULL,
    phoneNumber VARCHAR(16) NOT NULL,
    modeOfContact VARCHAR(8),
    preference VARCHAR(64) NOT NULL,
    currentSellingMode VARCHAR(256) NOT NULL,
    bankLocation VARCHAR(256) NOT NULL,
    accountHolderName VARCHAR(256) NOT NULL,
    routingNumber VARCHAR(9) NOT NULL,
    accountNumber VARCHAR(17) NOT NULL,
    incomeReceiver ENUM('Individuals/Sales Proprietor', 'Business') NOT NULL,
    usCitizen BOOLEAN NOT NULL,
    taxName VARCHAR(256) NOT NULL,
    tradeName VARCHAR(200) NOT NULL,
    taxAddress VARCHAR(512) NOT NULL,
    taxClassification ENUM('S Corporation', 'C Corporation', 'Partnership', 'Trust/Estate', 'Limited Liability Company', 'Others') NOT NULL,
    taxpayerIdentificationType ENUM('SSN or TIN Number', 'EIN Number') NOT NULL,
    taxpayerIdentificationNumber VARCHAR(9) NOT NULL,
    needElectronicSignature BOOLEAN NOT NULL,
    sellingOnOtherMarketPlace BOOLEAN NOT NULL,
    sellingOn ENUM('amazon', 'ebay', 'both') DEFAULT NULL,
    ebaySellingCategory VARCHAR(64) DEFAULT NULL,
    allowOffersOnEbay BOOLEAN,
    allowAutomaticFeedback BOOLEAN,
    needFulfillmentForEbay BOOLEAN,
    paymentType VARCHAR(64),
    shippingMethodForEbay VARCHAR(64),
    amazonSellingCategory VARCHAR(64),
    needProductListing ENUM('Retain Amazon existing listing only', 'Retain and Add new Product as well') DEFAULT 'Retain Amazon existing listing only',
    needFulfillmentForAmazon ENUM('Fulfillment by Vert', 'Fulfillment by Seller', 'Third Party Fulfillment') DEFAULT 'Fulfillment by Vert',
    shippingMethodForAmazon VARCHAR(64),
    obtainFBAandFBMForAmazon BOOLEAN,
    productSource ENUM('I manufacture them', 'I resell Products that I Purchase', 'I sell products manufactured by me', 'I dont know'),
    productIdentifier ENUM('All of my products have them', 'Some of my products have them', 'None of my products have them', 'I dont know'),
    productCount ENUM('1-10', '11-100', '101-500', 'More than 500', 'I dont know'),
    webAddressAvailable BOOLEAN DEFAULT NULL,
    webAddress VARCHAR(64),
    mainProductCategory VARCHAR(64),
    ssnDocumentName VARCHAR(32),
    ssnDocumentPath VARCHAR(512),
    govIdDocumentName VARCHAR(32),
    govIdDocumentPath VARCHAR(512),
    bankDocumentName VARCHAR(32),
    bankDocumentPath VARCHAR(512)
);

CREATE TABLE selfregister (
  id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  uuid VARCHAR(36)BINARY NOT NULL UNIQUE,
  name VARCHAR(64) NOT NULL,
  email VARCHAR(80) NOT NULL,
  phoneNumber VARCHAR(16) NOT NULL,
  processed BOOLEAN DEFAULT 0
);


#19.9.18 - 2:50PM
CREATE TABLE sellerquestionnaire (
  storeId INT UNSIGNED NOT NULL PRIMARY KEY,
  sellingOnOtherMarketPlace BOOLEAN NOT NULL,
  sellingOn enum('amazon','ebay','both') DEFAULT NULL,
  ebaySellingCategory varchar(64) DEFAULT NULL,
  allowOffersOnEbay BOOLEAN DEFAULT NULL,
  allowAutomaticFeedback BOOLEAN DEFAULT NULL,
  needFulfillmentForEbay BOOLEAN DEFAULT NULL,
  paymentType VARCHAR(64) DEFAULT NULL,
  shippingMethodForEbay VARCHAR(64) DEFAULT NULL,
  amazonSellingCategory varchar(64) DEFAULT NULL,
  needProductListing ENUM('Retain Amazon existing listing only', 'Retain and Add new Product as well') DEFAULT 'Retain Amazon existing listing only',
  needFulfillmentForAmazon ENUM('Fulfillment by Vert', 'Fulfillment by Seller', 'Third Party Fulfillment') DEFAULT 'Fulfillment by Vert',
  shippingMethodForAmazon VARCHAR(64) DEFAULT NULL,
  obtainFBAandFBMForAmazon BOOLEAN DEFAULT NULL,
  productSource ENUM('I manufacture them', 'I resell Products that I Purchase', 'I sell products manufactured by me', 'I dont know') NOT  NULL,
  productIdentifier ENUM('All of my products have them', 'Some of my products have them', 'None of my products have them', 'I dont know') NOT  NULL,
  productCount ENUM('1-10', '11-100', '101-500', 'More than 500', 'I dont know') NOT  NULL,
  webAddressAvailable BOOLEAN NOT  NULL,
  webAddress VARCHAR(64),
  mainProductCategory VARCHAR(64) NOT NULL,
  FOREIGN KEY (storeId)
      REFERENCES stores (storeId)
);

CREATE TABLE sellerbankdetails (
  storeId INT UNSIGNED NOT NULL PRIMARY KEY,
  accountNumber VARCHAR(17) NOT NULL,
  accountHolderName VARCHAR(256) NOT NULL,
  bankLocation VARCHAR(256) NOT NULL,
  routingNumber VARCHAR(9) NOT NULL,
  FOREIGN KEY (storeId)
      REFERENCES stores (storeId)
);

CREATE TABLE sellertaxdetails (
  storeId INT UNSIGNED NOT NULL PRIMARY KEY,
  incomeReceiver ENUM('Individuals/Sales Proprietor', 'Business') NOT NULL,
  usCitizen BOOLEAN NOT NULL,
  taxName VARCHAR(256) NOT NULL,
  tradeName VARCHAR(200) NOT NULL,
  taxAddress VARCHAR(512) NOT NULL,
  taxClassification ENUM('S Corporation', 'C Corporation', 'Partnership', 'Trust/Estate', 'Limited Liability Company', 'Others') NOT NULL,
  taxpayerIdentificationType ENUM('SSN or TIN Number', 'EIN Number') NOT NULL,
  taxpayerIdentificationNumber VARCHAR(9) NOT NULL,
  needElectronicSignature BOOLEAN NOT  NULL,
  FOREIGN KEY (storeId)
      REFERENCES stores (storeId)
);

ALTER TABLE onboardingseller ADD COLUMN processed BOOLEAN NOT NULL DEFAULT 0 AFTER bankDocumentPath;
ALTER TABLE onboardingseller MODIFY COLUMN email VARCHAR(80) NOT NULL;

ALTER TABLE onboardingseller MODIFY COLUMN revenue VARCHAR(32) NOT NULL;
ALTER TABLE onboardingseller MODIFY COLUMN preference VARCHAR(32) NOT NULL;

ALTER TABLE contactvert MODIFY COLUMN revenue VARCHAR(32);

ALTER TABLE contactvert MODIFY COLUMN sellingMode VARCHAR(64);
alter table onboardingseller MODIFY COLUMN currentSellingMode VARCHAR(64) NOT NULL;

ALTER TABLE onboardingseller MODIFY productSource ENUM('I manufacture them', 'I resell Products that I Purchase', 'I sell products manufactured by me', 'I dont know') NOT NULL ;
ALTER TABLE onboardingseller MODIFY productIdentifier ENUM('All of my products have them', 'Some of my products have them', 'None of my products have them', 'I dont know') NOT NULL ;
ALTER TABLE onboardingseller MODIFY productCount ENUM('1-10', '11-100', '101-500', 'More than 500', 'I dont know') NOT NULL ;
ALTER TABLE onboardingseller MODIFY webAddressAvailable BOOLEAN NOT NULL ;
ALTER TABLE onboardingseller MODIFY mainProductCategory VARCHAR(64) NOT NULL ;


#19.9.18 - 8:00PM
ALTER TABLE onboardingseller MODIFY COLUMN needProductListing enum('Retain Amazon existing listing only','Retain and Add new Product as well');
ALTER TABLE onboardingseller MODIFY COLUMN needFulfillmentForAmazon enum('Fulfillment by Vert','Fulfillment by Seller','Third Party Fulfillment');
ALTER TABLE sellerquestionnaire MODIFY COLUMN needProductListing enum('Retain Amazon existing listing only','Retain and Add new Product as well');
ALTER TABLE sellerquestionnaire MODIFY COLUMN needFulfillmentForAmazon enum('Fulfillment by Vert','Fulfillment by Seller','Third Party Fulfillment');


#20.9.18 - 7:40PM
alter table sellerquestionnaire modify column productSource  ENUM('I manufacture them', 'I resell Products that I Purchase', 'I sell products manufactured by me (private label)') NOT NULL;
alter table sellerquestionnaire modify column productIdentifier    ENUM('All of my products have them', 'Some of my products have them', 'None of my products have them', 'I don''t know') NOT NULL;
alter table sellerquestionnaire modify column productCount ENUM('1-10', '11-100', '101-500', 'More than 500', 'I don''t know') NOT NULL;

alter table onboardingseller modify column productSource  ENUM('I manufacture them', 'I resell Products that I Purchase', 'I sell products manufactured by me (private label)') NOT NULL;
alter table onboardingseller modify column productIdentifier ENUM('All of my products have them', 'Some of my products have them', 'None of my products have them', 'I don''t know') NOT NULL;
alter table onboardingseller modify column productCount ENUM('1-10', '11-100', '101-500', 'More than 500', 'I don''t know') NOT NULL;

alter table sellertaxdetails modify column taxClassification  ENUM('S Corporation', 'C Corporation', 'Partnership', 'Trust/Estate', 'Limited Liability Company', 'Other') NOT NULL;
alter table onboardingseller modify column taxClassification  ENUM('S Corporation', 'C Corporation', 'Partnership', 'Trust/Estate', 'Limited Liability Company', 'Other') NOT NULL;

insert into sellerbankdetails(storeId, accountNumber,accountHolderName,bankLocation,routingNumber)
(select storeId, FLOOR(RAND() * 1000000000), storeName, 'Atlanta',  FLOOR(RAND() * 1000000000) from stores
where storeId not in (select storeId from sellerbankdetails));


insert into sellertaxdetails(storeId, incomeReceiver,usCitizen,taxName,tradeName,taxAddress,taxClassification,taxpayerIdentificationType,
taxpayerIdentificationNumber,needElectronicSignature) (select storeId, 'Business', 1, storeName, storeName, 'Atlanta GA', 'Other',
'SSN or TIN Number',FLOOR(RAND() * 1000000000), 1 from stores where storeId not in (select storeId from sellertaxdetails));

insert into sellerquestionnaire(storeId, sellingOnOtherMarketPlace, productSource, productIdentifier, productCount, webAddressAvailable,
mainProductCategory) (select storeId, 0, 'I resell Products that I Purchase', 'All of my products have them', 'More than 500', 0,
'Toys,Apparels,Electronics,Others' from stores where storeId not in (select storeId from sellerquestionnaire));


#21.9.18 - 10:30AM
RENAME TABLE orderlist TO orderitem;

#21.9.18 - 06:10PM
alter table onboardingseller add column createdOn timestamp default current_timestamp after processed;

#24.9.18 - 06:40PM
CREATE TABLE userLoginAudit (
   loginId INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
   userId INT UNSIGNED NOT NULL,
   originatingIp varchar(15) NOT NULL,
   loginAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   logoutAt TIMESTAMP NULL DEFAULT NULL,
   FOREIGN KEY (userId)
       REFERENCES users(sellerId)
);


#1.10.18 - 06:50PM
CREATE TABLE transactionAudit (
   transcationId INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
   storeId INT UNSIGNED NOT NULL,
   userId INT UNSIGNED NOT NULL,
   transactionType ENUM('SKU') NOT NULL,
   transactionAction ENUM('Created','Modified') NOT NULL,
    transactionItem varchar(64) NOT NULL,
    originatingIp varchar(45) NOT NULL,
   actionPerformedAt TIMESTAMP NOT NULL,
    oldValue JSON
);

alter table userLoginAudit modify originatingIp varchar(45) NOT NULL;


ALTER TABLE onboardingseller MODIFY COLUMN maximumProductSku INT UNSIGNED ;

ALTER TABLE onboardingseller MODIFY COLUMN preference VARCHAR(64);
ALTER TABLE onboardingseller MODIFY incomeReceiver ENUM('Individuals/Sales Proprietor','Individuals/Sole Proprietor', 'Business') NOT NULL;
SET SQL_SAFE_UPDATES = 0;
Update onboardingseller set incomeReceiver = 'Individuals/Sole Proprietor' where incomeReceiver = 'Individuals/Sales Proprietor';
ALTER TABLE onboardingseller MODIFY incomeReceiver ENUM('Individuals/Sole Proprietor', 'Business') NOT NULL;

ALTER TABLE sellertaxdetails MODIFY incomeReceiver ENUM('Individuals/Sales Proprietor','Individuals/Sole Proprietor', 'Business') NOT NULL;
Update sellertaxdetails set incomeReceiver = 'Individuals/Sole Proprietor' where incomeReceiver = 'Individuals/Sales Proprietor';
ALTER TABLE sellertaxdetails MODIFY incomeReceiver ENUM('Individuals/Sole Proprietor', 'Business') NOT NULL;

ALTER TABLE onboardingseller MODIFY needFulfillmentForAmazon ENUM('Fulfillment by Vert','Fulfillment by Verte', 'Fulfillment by Seller', 'Third Party Fulfillment');
Update onboardingseller set needFulfillmentForAmazon = 'Fulfillment by Verte' where needFulfillmentForAmazon = 'Fulfillment by Vert';
ALTER TABLE onboardingseller MODIFY needFulfillmentForAmazon ENUM('Fulfillment by Verte', 'Fulfillment by Seller', 'Third Party Fulfillment');

ALTER TABLE sellerquestionnaire MODIFY needFulfillmentForAmazon ENUM('Fulfillment by Vert','Fulfillment by Verte', 'Fulfillment by Seller', 'Third Party Fulfillment');
Update sellerquestionnaire set needFulfillmentForAmazon = 'Fulfillment by Verte' where needFulfillmentForAmazon = 'Fulfillment by Vert';
ALTER TABLE sellerquestionnaire MODIFY needFulfillmentForAmazon ENUM('Fulfillment by Verte', 'Fulfillment by Seller', 'Third Party Fulfillment');


ALTER TABLE onboardingseller MODIFY productSource ENUM('I manufacture them', 'I purchase them in USA', 'I import them from other countries', 'I resell Products that I Purchase', 'I sell products manufactured by me (private label)') NOT NULL ;
update onboardingseller set productSource = 'I purchase them in USA' where productSource = 'I resell Products that I Purchase';
update onboardingseller set productSource = 'I import them from other countries' WHERE productSource =  'I sell products manufactured by me (private label)';
ALTER TABLE onboardingseller MODIFY productSource ENUM('I manufacture them', 'I purchase them in USA', 'I import them from other countries') NOT NULL ;


ALTER TABLE sellerquestionnaire MODIFY productSource ENUM('I manufacture them', 'I purchase them in USA', 'I import them from other countries', 'I resell Products that I Purchase', 'I sell products manufactured by me (private label)') NOT NULL ;
update sellerquestionnaire set productSource = 'I purchase them in USA' where productSource = 'I resell Products that I Purchase';
update sellerquestionnaire set productSource = 'I import them from other countries' WHERE productSource =  'I sell products manufactured by me (private label)';
ALTER TABLE sellerquestionnaire MODIFY productSource ENUM('I manufacture them', 'I purchase them in USA', 'I import them from other countries') NOT NULL ;


#09.10.18 - 11.20AM
CREATE TABLE menus (
    menuId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    menuName VARCHAR(32) NOT NULL,
    routerPath VARCHAR(64),
    menuIconNormal VARCHAR(512),
    menuIconActive VARCHAR(512),
    parentMenuId INT UNSIGNED,
    yes BOOLEAN NOT NULL,
    no BOOLEAN NOT NULL,
    isMainMenu BOOLEAN NOT NULL,
    menuHierachy INT UNSIGNED,
    FOREIGN KEY (parentMenuId)
        REFERENCES menus (menuId)
);
INSERT INTO menus VALUES(1, 'Dashboards', NULL, '../../../../assets/Assets/Whendonothoverover/home-normal-1.svg', '../../../../assets/Assets/Whendohoverover/home-active-1.svg', NULL, TRUE, TRUE, TRUE, 1);
INSERT INTO menus VALUES(2, 'Summary Dashboard', '/summary-dashboard', NULL, NULL, 1, TRUE, TRUE, FALSE, 1);
INSERT INTO menus VALUES(3, 'Sales Dashboard', '/sales-dashboard', NULL, NULL, 1, TRUE, TRUE, FALSE, 2);
INSERT INTO menus VALUES(4, 'Finance Dashboard', '/financial-dashboard', NULL, NULL, 1, TRUE, TRUE, FALSE, 3);
INSERT INTO menus VALUES(5, 'Orders Dashboard', '/orders-dashboard', NULL, NULL, 1, TRUE, TRUE, FALSE, 4);
INSERT INTO menus VALUES(6, 'Inventory Dashboard', '/inventory-dashboard', NULL, NULL, 1, TRUE, TRUE, FALSE, 5);
INSERT INTO menus VALUES(7, 'Sales', NULL, '../../../../assets/Assets/Whendonothoverover/sales-normal-1.svg', '../../../../assets/Assets/Whendohoverover/sales-active-1.svg', NULL, TRUE, TRUE, TRUE, 2);
INSERT INTO menus VALUES(8, 'Orders Enquiry', '/ordersEnquiry', NULL, NULL, 7, TRUE, TRUE, FALSE, 1);
INSERT INTO menus VALUES(9, 'Order-Returns', '/orderReturns', NULL, NULL, 7, TRUE, TRUE, FALSE, 2);
INSERT INTO menus VALUES(10, 'Inventory', NULL, '../../../../assets/Assets/Whendonothoverover/inventory-normal-1.svg', '../../../../assets/Assets/Whendohoverover/inventory-active-1.svg', NULL, TRUE, TRUE, TRUE, 3);
INSERT INTO menus VALUES(11, 'Stock Enquiry', '/stockEnquiry', NULL, NULL, 10, TRUE, TRUE, FALSE, 1);
INSERT INTO menus VALUES(12, 'Manage ASN', '/manageASN', NULL, NULL, 10, TRUE, TRUE, FALSE, 2);
INSERT INTO menus VALUES(13, 'ASN BulkUpload', '/ASN-bulkupload', NULL, NULL, 10, TRUE, TRUE, FALSE, 3);
INSERT INTO menus VALUES(14, 'Pricing', NULL, '../../../../assets/Assets/Whendonothoverover/catalog-normal-1.svg', '../../../../assets/Assets/Whendohoverover/catalog-active-1.svg', NULL, TRUE, TRUE, TRUE, 4);
INSERT INTO menus VALUES(15, 'Manage Pricing', '/managePrice', NULL, NULL, 14, TRUE, TRUE, FALSE, 1);
INSERT INTO menus VALUES(16, 'Catalog', NULL, '../../../../assets/Assets/Whendonothoverover/catalog-normal-1.svg', '../../../../assets/Assets/Whendohoverover/catalog-active-1.svg', NULL, TRUE, TRUE, TRUE, 5);
INSERT INTO menus VALUES(17, 'Manage Catalog', '/manageCatalog', NULL, NULL, 16, TRUE, TRUE, FALSE, 1);
INSERT INTO menus VALUES(18, 'Settings', NULL, '../../../../assets/Assets/Whendonothoverover/users-normal-1.svg', '../../../../assets/Assets/Whendohoverover/users-active-1.svg', NULL, TRUE, TRUE, TRUE, 6);
INSERT INTO menus VALUES(19, 'Roles and Users', '/rolesAndUsers', NULL, NULL, 18, TRUE, TRUE, FALSE, 1);
INSERT INTO menus VALUES(20, 'Notification Preferences', NULL, NULL, NULL, 18, TRUE, TRUE, FALSE, 2);
INSERT INTO menus VALUES(21, 'Audit Trail', '/audit-trail', NULL, NULL, 18, TRUE, TRUE, FALSE, 3);

CREATE TABLE menuFunctions (
   functionId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
   labelId VARCHAR(64) NOT NULL,
   functionName VARCHAR(32) NOT NULL,
   menuId INT UNSIGNED NOT NULL,
   yes  BOOLEAN NOT NULL,
   no BOOLEAN NOT NULL,
   functionHierachy INT UNSIGNED,
   FOREIGN KEY (menuId)
        REFERENCES menus (menuId)
  );
 INSERT INTO menuFunctions VALUES('1', 'addProduct', 'Add Product', '17', '1', '1', '1');
 INSERT INTO menuFunctions VALUES('2', 'addBulk', 'Add Bulk', '17', '1', '1', '2');
 INSERT INTO menuFunctions VALUES('3', 'bulkEdit', 'Bulk Edit', '17', '1', '1', '3');
 INSERT INTO menuFunctions VALUES('4', 'activateOrDeactivate', 'Activate Or De-Active Products', '17', '1', '1', '4');
 INSERT INTO menuFunctions VALUES('5', 'users', 'Users', '19', '1', '1', '1');
 INSERT INTO menuFunctions VALUES('6', 'roles', 'Roles', '19', '1', '1', '2');
 INSERT INTO menuFunctions VALUES('7', 'addUser', 'Add User', '19', '1', '1', '3');
 INSERT INTO menuFunctions VALUES('8', 'editUser', 'Edit User', '19', '1', '1', '4');
 INSERT INTO menuFunctions VALUES('9', 'addRole', 'Add Role', '19', '1', '1', '5');
 INSERT INTO menuFunctions VALUES('10', 'editRole', 'Edit Role', '19', '1', '1', '6');
 INSERT INTO menuFunctions VALUES('11', 'activateOrDeactivateUsers', 'Activate Or De-Active Users', '19', '1', '1', '7');
 INSERT INTO menuFunctions VALUES('12', 'columns', 'Columns', '17', '1', '1', '5');
 INSERT INTO menuFunctions VALUES('13', 'export', 'Export', '17', '1', '1', '6'); 
 INSERT INTO menuFunctions VALUES('14', 'orderDetails', 'Order Details', '8', '1', '1', '1');
 INSERT INTO menuFunctions VALUES('15', 'orderColumns', 'Columns', '8', '1', '1', '2');
 INSERT INTO menuFunctions VALUES('16', 'orderExport', 'Export', '8', '1', '1', '3'); 
 INSERT INTO menuFunctions VALUES('17', 'returnDetails', 'Return Details', '9', '1', '1', '1');
 INSERT INTO menuFunctions VALUES('18', 'returnColumns', 'Return Columns', '9', '1', '1', '2'); 
 INSERT INTO menuFunctions VALUES('19', 'invColumns', 'Export', '11', '1', '1', '1');
 INSERT INTO menuFunctions VALUES('20', 'invExport', 'Columns', '11', '1', '1', '2');
 INSERT INTO menuFunctions VALUES('21', 'addAsn', 'Add ASN', '12', '1', '1', '1');
 INSERT INTO menuFunctions VALUES('22', 'editAsn', 'Edit ASN', '12', '1', '1', '2'); 
 INSERT INTO menuFunctions VALUES('23', 'addBulkAsn', 'Add Bulk Asn', '13', '1', '1', '1');
 INSERT INTO menuFunctions VALUES('24', 'asnFilter', 'Filters', '13', '1', '1', '2');
 INSERT INTO menuFunctions VALUES('25', 'asnBulkColumns', 'Columns', '13', '1', '1', '3');  
 INSERT INTO menuFunctions VALUES('26', 'editPrice', 'Edit Price', '15', '1', '1', '1');
 INSERT INTO menuFunctions VALUES('27', 'auditDownload', 'Download', '21', '1', '1', '1');

 CREATE TABLE userRoles (
    roleId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
    storeId INT UNSIGNED NOT NULL,
    roleName VARCHAR(64) NOT NULL,
    description VARCHAR(256),
    createdBy VARCHAR(32) NOT NULL,
    createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modifiedBy VARCHAR(32) NOT NULL,
    modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    UNIQUE (roleName , storeId),
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);
INSERT INTO userRoles (roleId, storeId, roleName, description,createdBy, modifiedBy) SELECT roles_id, storeId, name, description,'admin','admin'  FROM  roles;

CREATE TABLE userSelectedMenus (
    roleId INT UNSIGNED NOT NULL,
    menuId INT UNSIGNED NOT NULL,
    UNIQUE (roleId , menuId),
    PRIMARY KEY (roleId , menuId),
    FOREIGN KEY (roleId)
        REFERENCES userRoles (roleId),
    FOREIGN KEY (menuId)
        REFERENCES menus (menuId)
);
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,1 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,2 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,3 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,4 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,5 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,6 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,7 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,8 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,9 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,10 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,11 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,12 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,13 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,14 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,15 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,16 FROM userRoles ;
INSERT INTO userSelectedMenus (roleId, menuId) SELECT roleId,17 FROM userRoles ;

CREATE TABLE userSelectedFunctions (
    roleId INT UNSIGNED NOT NULL,
    functionId INT UNSIGNED NOT NULL,
    UNIQUE (roleId , functionId),
    PRIMARY KEY (roleId , functionId),
    FOREIGN KEY (roleId)
        REFERENCES userRoles (roleId),
    FOREIGN KEY (functionId)
        REFERENCES menuFunctions (functionId)
);

CREATE TABLE userAssignedRoles (
    userId INT UNSIGNED NOT NULL,
    roleId INT UNSIGNED NOT NULL,
    UNIQUE (userId , roleId),
    PRIMARY KEY (userId , roleId),
    FOREIGN KEY (userId)
        REFERENCES users (sellerId),
    FOREIGN KEY (roleId)
        REFERENCES userRoles (roleId)
);
INSERT INTO userAssignedRoles (userId, roleId) SELECT sellerId, roles_id FROM assignednames;


#09.10.18 - 12.40PM
UPDATE menus SET menuIconNormal = 'assets/Assets/Whendonothoverover/home-normal-1.svg' , menuIconActive = 'assets/Assets/Whendohoverover/home-active-1.svg' WHERE menuId = 1;
UPDATE menus SET menuIconNormal = 'assets/Assets/Whendonothoverover/sales-normal-1.svg' , menuIconActive = 'assets/Assets/Whendohoverover/inventory-active-1.svg' WHERE menuId = 7;
UPDATE menus SET menuIconNormal = 'assets/Assets/Whendonothoverover/inventory-normal-1.svg' , menuIconActive = 'assets/Assets/Whendohoverover/home-active-1.svg' WHERE menuId = 10;
UPDATE menus SET menuIconNormal = 'assets/Assets/Whendonothoverover/catalog-normal-1.svg' , menuIconActive = 'assets/Assets/Whendohoverover/catalog-active-1.svg' WHERE menuId = 14;
UPDATE menus SET menuIconNormal = 'assets/Assets/Whendonothoverover/catalog-normal-1.svg' , menuIconActive = 'assets/Assets/Whendohoverover/catalog-active-1.svg' WHERE menuId = 16;
UPDATE menus SET menuIconNormal = 'assets/Assets/Whendonothoverover/users-normal-1.svg' , menuIconActive = 'assets/Assets/Whendohoverover/users-active-1.svg' WHERE menuId = 18;


#09.10.18 - 04.15PM
INSERT INTO menus VALUES(22, 'Order-D3', '/orderdashboardD3', NULL, NULL, 7, TRUE, TRUE, FALSE, 3);
UPDATE menus SET menuIconActive='assets/Assets/Whendohoverover/sales-active-1.svg'  WHERE  menuId=7;
UPDATE menus SET menuIconActive='assets/Assets/Whendohoverover/inventory-active-1.svg' WHERE  menuId=10;


#15.10.18 - 11.15AM
UPDATE `fieldmaster` SET `title`='Standard Price $' WHERE `field_id`='24';
UPDATE `fieldmaster` SET `title`='Retail Price $' WHERE `field_id`='25';


#16.10.18 - 11.15AM
CREATE TABLE `new_asn` (
   `asnId` int(10) unsigned NOT NULL AUTO_INCREMENT,
   `storeId` int(10) unsigned NOT NULL,
   `asnNumber` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
   `asnStatus` enum('FRESH','CONFIRMED','FAILED','CANCELLED','AMENDED_FRESH','AMENDED_CONFIRMED','AMENDMENT_FAILED','CLOSED') NOT NULL DEFAULT 'FRESH',
   `isIncomplete` tinyint(1) NOT NULL DEFAULT '0',
   `createdBy` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
   `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   `modifiedBy` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
   `modifiedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   PRIMARY KEY (`asnId`),
   UNIQUE KEY `asnNumber` (`asnNumber`),
   KEY `storeId` (`storeId`),
   CONSTRAINT `new_asn_ibfk_1` FOREIGN KEY (`storeId`) REFERENCES `stores` (`storeId`)
 );

 CREATE TABLE `new_asn_details` (
   `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
   `asnId` int(10) unsigned NOT NULL,
   `amendmentNumber` tinyint(4) NOT NULL DEFAULT '0',
   `asnDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   `asnSource` enum('SELLER_PORTAL','BULK_UPLOAD','SELLER_SYSTEMS','WMS') NOT NULL DEFAULT 'SELLER_PORTAL',
   `deliveryMode` enum('SMALL_PARCEL','LTL','FTL') NOT NULL,
   `originatingLocation` varchar(50) NOT NULL,
   `destinationLocation` enum('ATLANTA_DC') NOT NULL,
   `appointmentNumber` varchar(20) DEFAULT NULL,
   `sealNumber` varchar(20) DEFAULT NULL,
   `shipmentDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   `estimatedDeliveryDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
   `referenceDocumentType` enum('ASN','PO') NOT NULL,
   `referenceDocumentNumber` varchar(20) NOT NULL,
   `referenceDocumentDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   `vendorNumber` varchar(32) DEFAULT NULL,
   `vendorName` varchar(60) DEFAULT NULL,
   `vendorAsnNumber` varchar(32) DEFAULT NULL,
   `vendorAsnDate` timestamp NULL DEFAULT NULL,
   `asnSentBy` varchar(50) NOT NULL,
   `remarks` varchar(255) DEFAULT NULL,
   `carrierName` varchar(60) NOT NULL,
   `shipVia` varchar(30) DEFAULT NULL,
   `modeOfTransport` enum('ROAD','SHIP','AIR') NOT NULL,
   `vehicleType` enum('TRUCK','CONTAINER') DEFAULT NULL,
   `vehicleNumber` varchar(32) DEFAULT NULL,
   `typeOfContainer` enum('DRY_STORAGE_CONTAINER','FLAT_RACK_CONTAINER') DEFAULT NULL,
   `driverName` varchar(50) NOT NULL,
   `packagingOptions` enum('PALLET','CASE') NOT NULL,
   `trackingNumber` varchar(32) NOT NULL,
   `billOfLadingNumber` varchar(32) NOT NULL,
   `grossWeight` float DEFAULT NULL,
   `nettWeight` float DEFAULT NULL,
   `volume` float DEFAULT NULL,
   `quarantineType` enum('AQIS','IFIP') DEFAULT NULL,
   `quarantineStatus` enum('YES','NO') DEFAULT NULL,
   `blockingStage` enum('UNLOADING','PUTAWAY') DEFAULT NULL,
   `releaseNumber` varchar(32) DEFAULT NULL,
   `releaseDate` timestamp NULL DEFAULT NULL,
   `fromName` varchar(50) NOT NULL,
   `fromAddress1` varchar(50) NOT NULL,
   `fromAddress2` varchar(50) NOT NULL,
   `fromCity` varchar(32) NOT NULL,
   `fromState` varchar(32) NOT NULL,
   `fromCountry` varchar(50) NOT NULL,
   `fromZipCode` varchar(10) NOT NULL,
   `fromPhoneNumber` varchar(16) NOT NULL,
   `toName` varchar(50) NOT NULL,
   `toAddress1` varchar(50) NOT NULL,
   `toAddress2` varchar(50) NOT NULL,
   `toCity` varchar(32) NOT NULL,
   `toState` varchar(32) NOT NULL,
   `toCountry` varchar(50) NOT NULL,
   `toZipCode` varchar(10) NOT NULL,
   `cartonizeOption` enum('MANUAL','AUTO') NOT NULL,
   PRIMARY KEY (`id`),
   UNIQUE KEY `id` (`id`),
   UNIQUE KEY `asnId` (`asnId`),
   CONSTRAINT `new_asn_details_ibfk_1` FOREIGN KEY (`asnId`) REFERENCES `new_asn` (`asnId`)
 );

 CREATE TABLE `new_asn_details_temp` (
   `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
   `asnId` int(10) unsigned NOT NULL,
   `amendmentNumber` tinyint(4) NOT NULL DEFAULT '0',
   `asnDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   `asnSource` enum('SELLER_PORTAL','BULK_UPLOAD') NOT NULL DEFAULT 'SELLER_PORTAL',
   `deliveryMode` enum('SMALL_PARCEL','LTL','FTL') NOT NULL,
   `originatingLocation` varchar(50) DEFAULT NULL,
   `destinationLocation` enum('ATLANTA_DC') NOT NULL,
   `appointmentNumber` varchar(20) DEFAULT NULL,
   `sealNumber` varchar(20) DEFAULT NULL,
   `shipmentDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   `estimatedDeliveryDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
   `referenceDocumentType` enum('ASN','PO') NOT NULL,
   `referenceDocumentNumber` varchar(20) DEFAULT NULL,
   `referenceDocumentDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   `vendorNumber` varchar(32) DEFAULT NULL,
   `vendorName` varchar(60) DEFAULT NULL,
   `vendorAsnNumber` varchar(32) DEFAULT NULL,
   `vendorAsnDate` timestamp NULL DEFAULT NULL,
   `asnSentBy` varchar(50) NOT NULL,
   `remarks` varchar(255) DEFAULT NULL,
   `carrierName` varchar(60) DEFAULT NULL,
   `shipVia` varchar(30) DEFAULT NULL,
   `modeOfTransport` enum('ROAD','SHIP','AIR') NOT NULL,
   `vehicleType` enum('TRUCK','CONTAINER') DEFAULT NULL,
   `vehicleNumber` varchar(32) DEFAULT NULL,
   `typeOfContainer` enum('DRY_STORAGE_CONTAINER','FLAT_RACK_CONTAINER') DEFAULT NULL,
   `driverName` varchar(50) DEFAULT NULL,
   `packagingOptions` enum('PALLET','CASE') NOT NULL,
   `trackingNumber` varchar(32) DEFAULT NULL,
   `billOfLadingNumber` varchar(32) DEFAULT NULL,
   `grossWeight` float DEFAULT NULL,
   `nettWeight` float DEFAULT NULL,
   `volume` float DEFAULT NULL,
   `quarantineType` enum('AQIS','IFIP') DEFAULT NULL,
   `quarantineStatus` enum('YES','NO') DEFAULT NULL,
   `blockingStage` enum('UNLOADING','PUTAWAY') DEFAULT NULL,
   `releaseNumber` varchar(32) DEFAULT NULL,
   `releaseDate` timestamp NULL DEFAULT NULL,
   `fromName` varchar(50) DEFAULT NULL,
   `fromAddress1` varchar(50) DEFAULT NULL,
   `fromAddress2` varchar(50) DEFAULT NULL,
   `fromCity` varchar(32) DEFAULT NULL,
   `fromState` varchar(32) DEFAULT NULL,
   `fromCountry` varchar(50) DEFAULT NULL,
   `fromZipCode` varchar(5) DEFAULT NULL,
   `fromPhoneNumber` varchar(16) DEFAULT NULL,
   `toName` varchar(50) DEFAULT NULL,
   `toAddress1` varchar(50) DEFAULT NULL,
   `toAddress2` varchar(50) DEFAULT NULL,
   `toCity` varchar(32) DEFAULT NULL,
   `toState` varchar(32) DEFAULT NULL,
   `toCountry` varchar(50) DEFAULT NULL,
   `toZipCode` varchar(5) DEFAULT NULL,
   `cartonizeOption` enum('MANUAL','AUTO') NOT NULL,
   PRIMARY KEY (`id`),
   UNIQUE KEY `id` (`id`),
   UNIQUE KEY `asnId` (`asnId`),
   CONSTRAINT `new_asn_details_temp_ibfk_1` FOREIGN KEY (`asnId`) REFERENCES `new_asn` (`asnId`)
 );
 
 CREATE TABLE `new_asn_sku_info` (
   `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
   `asnId` int(10) unsigned NOT NULL,
   `lineNumber` smallint(5) unsigned NOT NULL,
   `productId` int(10) unsigned NOT NULL,
   `productStatus` enum('NEW','REFURBISHED','DAMAGED') NOT NULL,
   `shippedUnits` int(10) unsigned NOT NULL,
   `skuQuantityPerCase` smallint(5) unsigned NOT NULL,
   `caseQuantityPerPallet` tinyint(3) unsigned DEFAULT NULL,
   `countryOfOrigin` varchar(50) NOT NULL,
   `vasCode` varchar(32) DEFAULT NULL,
   PRIMARY KEY (`id`),
   UNIQUE KEY `id` (`id`),
   UNIQUE KEY `asnId` (`asnId`,`lineNumber`),
   UNIQUE KEY `asnId_2` (`asnId`,`productId`),
   KEY `productId` (`productId`),
   CONSTRAINT `new_asn_sku_info_ibfk_1` FOREIGN KEY (`asnId`) REFERENCES `new_asn` (`asnId`),
   CONSTRAINT `new_asn_sku_info_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `productmapper` (`product_id`)
 );
 
  CREATE TABLE `new_asn_packaging_info` (
   `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
   `asnId` int(10) unsigned NOT NULL,
   `productId` int(10) unsigned NOT NULL,
   `palletLpn` varchar(32) DEFAULT NULL,
   `caseLpn` varchar(32) NOT NULL,
   `skuQuantity` smallint(5) unsigned NOT NULL,
   `batchNumber` varchar(32) NOT NULL,
   PRIMARY KEY (`id`),
   UNIQUE KEY `id` (`id`),
   UNIQUE KEY `asnId_2` (`asnId`,`caseLpn`),
   UNIQUE KEY `asnId` (`asnId`,`palletLpn`,`caseLpn`),
   KEY `new_asnpackaginginfo_ibfk_1` (`asnId`,`productId`),
   CONSTRAINT `new_asn_packaging_info_ibfk_1` FOREIGN KEY (`asnId`, `productId`) REFERENCES `new_asn_sku_info` (`asnId`, `productId`) ON DELETE CASCADE
 );
 
 CREATE TABLE `new_asn_documents` (
   `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
   `asnId` int(10) unsigned NOT NULL,
   `documentName` varchar(32) NOT NULL,
   `documentPath` varchar(512) NOT NULL,
   PRIMARY KEY (`id`),
   UNIQUE KEY `id` (`id`),
   UNIQUE KEY `asnId` (`asnId`,`documentName`),
   CONSTRAINT `new_asn_documents_ibfk_1` FOREIGN KEY (`asnId`) REFERENCES `new_asn` (`asnId`)
 );

 
 #23.10.18 - 07.35PM
 ALTER TABLE new_asn ADD COLUMN reasonForRejection VARCHAR(256) NULL AFTER asnStatus;

 
 #25.10.18 - 11.30AM
ALTER TABLE new_asn_details MODIFY COLUMN grossWeight DECIMAL(12,2);
ALTER TABLE new_asn_details MODIFY COLUMN nettWeight DECIMAL(12,2);
ALTER TABLE new_asn_details MODIFY COLUMN volume DECIMAL(12,2);

ALTER TABLE new_asn_details_temp MODIFY COLUMN grossWeight DECIMAL(12,2);
ALTER TABLE new_asn_details_temp MODIFY COLUMN nettWeight DECIMAL(12,2);
ALTER TABLE new_asn_details_temp MODIFY COLUMN volume DECIMAL(12,2);


 #25.10.18 - 7.20PM
UPDATE menus SET menuHierachy = 4 WHERE menuId = 12;
INSERT INTO menus VALUES(23, 'ASN', '/asn', NULL, NULL, 10, TRUE, TRUE, FALSE, 3);

INSERT INTO menuFunctions VALUES(28, 'asnAdd', 'Add ASN', 23, TRUE, TRUE, 1);
INSERT INTO menuFunctions VALUES(29, 'asnColumns', 'Columns', 23, TRUE, TRUE, 2);
INSERT INTO menuFunctions VALUES(30, 'asnAmend', 'Amend', 23, TRUE, TRUE, 3);
INSERT INTO menuFunctions VALUES(31, 'asnPrintPackSlip', 'Print Packslip', 23, TRUE, TRUE, 4);
INSERT INTO menuFunctions VALUES(32, 'asnPrintLabels', 'Print Labels', 23, TRUE, TRUE, 5);
INSERT INTO menuFunctions VALUES(33, 'asnCancel', 'Cancel', 23, TRUE, TRUE, 6);
INSERT INTO menuFunctions VALUES(34, 'asnExport', 'Export', 23, TRUE, TRUE, 7);


 #29.10.18 - 6.20PM
ALTER TABLE orders MODIFY `paymentMethod` VARCHAR(128) DEFAULT NULL;


 #30.10.18 - 6.40PM
ALTER TABLE contactvert ADD COLUMN createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;



#08.11.18 - 4.10PM
INSERT INTO menus VALUES(24, 'Sales-D3', '/salesdashboardD3', NULL, NULL, 7, TRUE, TRUE, FALSE, 4);


 #09.11.18 - 4.00PM
UPDATE fieldmaster SET name = 'storage_strategy', title = 'Storage Strategy' WHERE field_id = 89;


 #09.11.18 - 6.40PM
UPDATE fieldmaster SET datatype = 'varchar' WHERE name = 'MPN';
UPDATE fieldmaster SET datatype = 'varchar' WHERE name = 'UPC';

ALTER TABLE sellerquestionnaire MODIFY needFulfillmentForAmazon ENUM('Fulfillment by Verte', 'Fulfillment by Seller', 'Third Party Fulfillment','Verte', 'Seller', 'Third-Party') DEFAULT 'Verte';
Update sellerquestionnaire set needFulfillmentForAmazon = 'Verte' where needFulfillmentForAmazon = 'Fulfillment by Verte';
Update sellerquestionnaire set needFulfillmentForAmazon = 'Seller' where needFulfillmentForAmazon = 'Fulfillment by Seller';
Update sellerquestionnaire set needFulfillmentForAmazon = 'Third-Party' where needFulfillmentForAmazon = 'Third Party Fulfillment';
ALTER TABLE sellerquestionnaire MODIFY needFulfillmentForAmazon ENUM('Verte', 'Seller', 'Third-Party') DEFAULT 'Verte';

ALTER TABLE onboardingseller MODIFY needFulfillmentForAmazon ENUM('Fulfillment by Verte', 'Fulfillment by Seller', 'Third Party Fulfillment','Verte', 'Seller', 'Third-Party') DEFAULT 'Verte';
Update onboardingseller set needFulfillmentForAmazon = 'Verte' where needFulfillmentForAmazon = 'Fulfillment by Verte';
Update onboardingseller set needFulfillmentForAmazon = 'Seller' where needFulfillmentForAmazon = 'Fulfillment by Seller';
Update onboardingseller set needFulfillmentForAmazon = 'Third-Party' where needFulfillmentForAmazon = 'Third Party Fulfillment';
ALTER TABLE onboardingseller MODIFY needFulfillmentForAmazon ENUM('Verte', 'Seller', 'Third-Party') DEFAULT 'Verte';

ALTER TABLE onboardingseller DROP column preference;

#14-11-2018 12:25AM
CREATE TABLE cartoninfo (
    orderId VARCHAR(32) NOT NULL,
    storeId INT UNSIGNED NOT NULL,
    cartonId VARCHAR(18) NOT NULL UNIQUE,
    cartonStatus ENUM('WAVED', 'PICKED', 'PACKED', 'SHIPPED', 'DELIVERED'),
    trackingNumber VARCHAR(18) unique,
	facilityId VARCHAR(32),    
    carrier VARCHAR(12),
    serviceLevel VARCHAR(12),
    shippingDate TIMESTAMP NULL DEFAULT NULL,
    deliveryDate TIMESTAMP NULL DEFAULT NULL,
    picker VARCHAR(32),
    packer VARCHAR(32),
    psid VARCHAR(32),
    bin VARCHAR(32),
    pickDate TIMESTAMP NULL DEFAULT NULL,
    packDate TIMESTAMP NULL DEFAULT NULL,
    flag varchar(16),
    lastCartonStatusUpdate TIMESTAMP NOT NULL,
	FOREIGN KEY (orderId , storeId)
        REFERENCES orders (orderId , storeId)
);

CREATE TABLE cartonitems (
	cartonId VARCHAR(18) NOT NULL,
    product_id INT UNSIGNED NOT NULL,
	quantity INT UNSIGNED NOT NULL,
    UNIQUE KEY(cartonId, product_id),
    FOREIGN KEY (cartonId)
        REFERENCES cartoninfo (cartonId)
);

ALTER TABLE orders MODIFY orderStatus ENUM('Processing', 'Partially Shipped', 'Shipped', 'Partially Delivered',
'Delivered', 'Cancelled');

ALTER TABLE orders MODIFY orderStatus ENUM('Processing', 'Partially Shipped', 'Shipped', 'Partially Delivered',
'Delivered', 'Cancelled') NOT NULL;

ALTER TABLE cartoninfo change picker pickedBy VARCHAR(32);
ALTER TABLE cartoninfo change packer packedBy VARCHAR(32);
ALTER TABLE cartoninfo MODIFY cartonStatus ENUM('WAVED', 'PICKED', 'PACKED', 'SHIPPED', 'DELIVERED') NOT NULL;


#16-11-2018 02:05AM
CREATE TABLE shipment (
    shipmentId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY AUTO_INCREMENT,
    shipmentNumber VARCHAR(32)BINARY NOT NULL,
    shipmentStatus ENUM('FRESH', 'CONFIRMED', 'CANCELLED', 'DISPATCHED', 'IN_TRANSIT', 'AT_CONSIGNEE', 'DELIVERED') NOT NULL,
    isIncomplete BOOLEAN NOT NULL DEFAULT FALSE,
    createdBy VARCHAR(32)BINARY NOT NULL,
    createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modifiedBy VARCHAR(32)BINARY NOT NULL,
    modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    UNIQUE (shipmentNumber)
);
CREATE TABLE shipment_details (
    shipmentId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    shipmentDateTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    masterBillOfLadingNumber VARCHAR(32) NOT NULL,
    originatingLocation VARCHAR(50) NOT NULL,
    destinationLocation ENUM('ATLANTA_DC') NOT NULL,
    shipmentSource ENUM('VERTE_SYSTEM', 'BULK_UPLOAD', 'SHIPPER_SYSTEMS') NOT NULL DEFAULT 'VERTE_SYSTEM',
    deliveryMode ENUM('CONTAINER', 'LTL', 'FTL') NOT NULL,
    appointmentNumber VARCHAR(20) NOT NULL,
    sealNumber VARCHAR(20),
    shipmentDate TIMESTAMP NOT NULL,
    estimatedDeliveryDate TIMESTAMP NOT NULL,
    referenceDocumentType ENUM('ASN', 'PO') NOT NULL,
    referenceDocumentNumber VARCHAR(20) NOT NULL,
    referenceDocumentDate TIMESTAMP NOT NULL,
    shipmentSentBy VARCHAR(50) NOT NULL,
    isMaterialHazardous BOOLEAN,
    emergencyContactNumber VARCHAR(16),
    emergencyContactName VARCHAR(60),
    remarks VARCHAR(255),
    carrierName VARCHAR(60) NOT NULL,
    shipVia VARCHAR(30),
    modeOfTransport ENUM('ROAD', 'SHIP', 'AIR') NOT NULL,
    vehicleType ENUM('TRUCK', 'CONTAINER'),
    typeOfContainer ENUM('_8FT_CONTAINER', '_10FT_CONTAINER', '_20FT_CONTAINER', '_40FT_CONTAINER'),
    vehicleNumber VARCHAR(32),
    driverName VARCHAR(50),
    containerLength DECIMAL(12 , 2 ),
    containerWidth DECIMAL(12 , 2 ),
    containerHeight DECIMAL(12 , 2 ),
    unloaderName VARCHAR(50),
    consignmentUser VARCHAR(50),
    supervisor VARCHAR(50),
    packagingOptions ENUM('PALLET', 'CASE', 'MIXED') NOT NULL,
    trackingNumber VARCHAR(32) NOT NULL,
    grossWeight DECIMAL(12 , 2 ),
    nettWeight DECIMAL(12 , 2 ),
    volume DECIMAL(12 , 2 ),
    quarantineType ENUM('AQIS', 'IFIP'),
    quarantineStatus ENUM('YES', 'NO'),
    blockingStage ENUM('UNLOADING', 'PUTAWAY'),
    releaseNumber VARCHAR(32),
    releaseDate TIMESTAMP,
    fromName VARCHAR(50) NOT NULL,
    fromAddress1 VARCHAR(50) NOT NULL,
    fromAddress2 VARCHAR(50) NOT NULL,
    fromCity VARCHAR(32) NOT NULL,
    fromState VARCHAR(32) NOT NULL,
    fromCountry VARCHAR(50) NOT NULL,
    fromZipCode VARCHAR(10) NOT NULL,
    fromPhoneNumber VARCHAR(16) NOT NULL,
    toName VARCHAR(50) NOT NULL,
    toAddress1 VARCHAR(50) NOT NULL,
    toAddress2 VARCHAR(50) NOT NULL,
    toCity VARCHAR(32) NOT NULL,
    toState VARCHAR(32) NOT NULL,
    toCountry VARCHAR(50) NOT NULL,
    toZipCode VARCHAR(10) NOT NULL,
    FOREIGN KEY (shipmentId)
        REFERENCES shipment (shipmentId)
);
CREATE TABLE shipment_details_temp (
    shipmentId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    shipmentDateTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    masterBillOfLadingNumber VARCHAR(32),
    originatingLocation VARCHAR(50),
    destinationLocation ENUM('ATLANTA_DC'),
    shipmentSource ENUM('VERTE_SYSTEM', 'BULK_UPLOAD', 'SHIPPER_SYSTEMS'),
    deliveryMode ENUM('CONTAINER', 'LTL', 'FTL'),
    appointmentNumber VARCHAR(20),
    sealNumber VARCHAR(20),
    shipmentDate TIMESTAMP,
    estimatedDeliveryDate TIMESTAMP,
    referenceDocumentType ENUM('ASN', 'PO'),
    referenceDocumentNumber VARCHAR(20),
    referenceDocumentDate TIMESTAMP,
    shipmentSentBy VARCHAR(50),
    isMaterialHazardous BOOLEAN,
    emergencyContactNumber VARCHAR(16),
    emergencyContactName VARCHAR(60),
    remarks VARCHAR(255),
    carrierName VARCHAR(60),
    shipVia VARCHAR(30),
    modeOfTransport ENUM('ROAD', 'SHIP', 'AIR'),
    vehicleType ENUM('TRUCK', 'CONTAINER'),
    typeOfContainer ENUM('_8FT_CONTAINER', '_10FT_CONTAINER', '_20FT_CONTAINER', '_40FT_CONTAINER'),
    vehicleNumber VARCHAR(32),
    driverName VARCHAR(50),
    containerLength DECIMAL(12 , 2 ),
    containerWidth DECIMAL(12 , 2 ),
    containerHeight DECIMAL(12 , 2 ),
    unloaderName VARCHAR(50),
    consignmentUser VARCHAR(50),
    supervisor VARCHAR(50),
    packagingOptions ENUM('PALLET', 'CASE', 'MIXED'),
    trackingNumber VARCHAR(32),
    grossWeight DECIMAL(12 , 2 ),
    nettWeight DECIMAL(12 , 2 ),
    volume DECIMAL(12 , 2 ),
    quarantineType ENUM('AQIS', 'IFIP'),
    quarantineStatus ENUM('YES', 'NO'),
    blockingStage ENUM('UNLOADING', 'PUTAWAY'),
    releaseNumber VARCHAR(32),
    releaseDate TIMESTAMP,
    fromName VARCHAR(50),
    fromAddress1 VARCHAR(50),
    fromAddress2 VARCHAR(50),
    fromCity VARCHAR(32),
    fromState VARCHAR(32),
    fromCountry VARCHAR(50),
    fromZipCode VARCHAR(10),
    fromPhoneNumber VARCHAR(16),
    toName VARCHAR(50),
    toAddress1 VARCHAR(50),
    toAddress2 VARCHAR(50),
    toCity VARCHAR(32),
    toState VARCHAR(32),
    toCountry VARCHAR(50),
    toZipCode VARCHAR(10),
    FOREIGN KEY (shipmentId)
        REFERENCES shipment (shipmentId)
);
CREATE TABLE shipment_asn_info (
    shipmentId INT UNSIGNED NOT NULL,
    asnId INT UNSIGNED NOT NULL,
    UNIQUE (shipmentId , asnId),
    PRIMARY KEY (shipmentId , asnId),
    FOREIGN KEY (shipmentId)
        REFERENCES shipment (shipmentId),
    FOREIGN KEY (asnId)
        REFERENCES new_asn (asnId)
);
CREATE TABLE shipment_status (
	id INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    shipmentId INT UNSIGNED NOT NULL,
    currentShipmentStatus ENUM('DISPATCHED', 'IN_TRANSIT', 'AT_CONSIGNEE', 'DELIVERED') NOT NULL,
    currentLocation VARCHAR(50) NOT NULL,
    currentEstimatedTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updatedBy VARCHAR(32) NOT NULL,
    updatedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (shipmentId)
        REFERENCES shipment (shipmentId)
);
CREATE TABLE shipment_documents (
    id INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    shipmentId INT UNSIGNED NOT NULL,
    documentName VARCHAR(32) NOT NULL,
    documentPath VARCHAR(512) NOT NULL,
    uploadedBy VARCHAR(32) NOT NULL,
    uploadedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    UNIQUE (shipmentId , documentName),
    FOREIGN KEY (shipmentId)
        REFERENCES shipment (shipmentId)
);


#16-11-2018 11:45PM
CREATE TABLE vendor_metadata (
    id INT UNSIGNED NOT NULL PRIMARY KEY,
    vendorCode VARCHAR(64) NOT NULL,
    vendorName VARCHAR(64) NOT NULL,
    country VARCHAR(16) NOT NULL
);

INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (1,'CH01280', 'Anhui Huaining Shansen Garments co.,ltd', 'China');
INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (2, 'CH01281', 'Changzhou Jiacheng Garments Co.,Ltd', 'China');
INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (3, 'CH01282', 'Changzhou Meijia Gongyi Xiupin Factory', 'China' );
INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (4, 'CH01283', 'Chongqing Tooku Dec. Manufacturing Co., Ltd','China' );
INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (5, 'US02345', 'Complete Garment, Inc', 'USA');
INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (6, 'US02346', 'Icon Screening, Inc', 'USA');
INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (7, 'US02347', 'ReadyOne Industries , Inc.', 'USA');
INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (8, 'US02348', 'Yesterwear Production Inc', 'USA');
INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (9, 'VI07589', 'Branch of Yupoong Vietnam Co.,Ltd', 'Vietnam');
INSERT INTO vendor_metadata (id, vendorCode, vendorName, country) VALUES (10, 'VI07590', 'Dong - A Vina Co., Ltd', 'Vietnam');



#19-11-2018 10:30AM
ALTER TABLE new_asn_details MODIFY COLUMN fromAddress2 VARCHAR(50);
ALTER TABLE new_asn_details MODIFY COLUMN toAddress2 VARCHAR(50);

UPDATE  fieldmaster SET  title ='Created On'  WHERE  field_id = 73;
UPDATE  fieldmaster  SET  title = 'Updated On'  WHERE  field_id =74;


#19-11-2018 11:30AM
ALTER TABLE shipment_details ADD totalDeclaredValue DECIMAL(12 , 2 ) NOT NULL AFTER volume;
ALTER TABLE shipment_details_temp ADD totalDeclaredValue DECIMAL(12 , 2 ) AFTER volume;

alter table selfregister add column  createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

UPDATE fieldmaster SET ismandotry=1 WHERE field_id = '89';


#21-11-2018 11:30AM
alter table users modify userName varchar(32) NOT NULL;
alter table vertadmin modify userName varchar(32) NOT NULL;

alter table vertroles modify createdBy varchar(32) NOT NULL;
alter table vertroles modify modifiedBy varchar(32) NOT NULL;

alter table vertadmin modify createdBy varchar(32) NOT NULL;
alter table vertadmin modify modifiedBy varchar(32) NOT NULL;

alter table users modify createdBy varchar(32) NOT NULL;
alter table users modify modifiedBy varchar(32) NOT NULL;

alter table stores modify createdBy varchar(32) NOT NULL;
alter table stores modify modifiedBy varchar(32) NOT NULL;

alter table new_asn modify createdBy varchar(32) NOT NULL;
alter table new_asn modify modifiedBy varchar(32) NOT NULL;


alter table shipment modify createdBy varchar(32) NOT NULL;
alter table shipment modify modifiedBy varchar(32) NOT NULL;


#26-11-2018 05:00PM
CREATE TABLE global_fieldmaster (
    fieldId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    category ENUM('ASN_OUTER_GRID', 'ASN_INNER_GRID') NOT NULL,
    fieldName VARCHAR(32) NOT NULL,
    title VARCHAR(32) NOT NULL,
    datatypeInUI VARCHAR(16) NOT NULL,
    defaultSelected BOOLEAN NOT NULL,
    UNIQUE (category , fieldName)
);
INSERT INTO global_fieldmaster VALUES(1, 'ASN_OUTER_GRID', 'asnNumber', 'ASN Number', 'string', TRUE);
INSERT INTO global_fieldmaster VALUES(2, 'ASN_OUTER_GRID', 'asnStatus', 'ASN Status','string', TRUE);
INSERT INTO global_fieldmaster VALUES(3, 'ASN_OUTER_GRID', 'amendmentNumber', 'Amendment Number','number', TRUE);
INSERT INTO global_fieldmaster VALUES(4, 'ASN_OUTER_GRID', 'asnDateTime', 'ASN Date','datetime', TRUE);
INSERT INTO global_fieldmaster VALUES(5, 'ASN_OUTER_GRID', 'asnSource', 'Source of ASN','string', TRUE);
INSERT INTO global_fieldmaster VALUES(6, 'ASN_OUTER_GRID', 'deliveryMode', 'Delivery Mode','string', TRUE);
INSERT INTO global_fieldmaster VALUES(7, 'ASN_OUTER_GRID', 'shipmentDate', 'Shipment Date','datetime', TRUE);
INSERT INTO global_fieldmaster VALUES(8, 'ASN_OUTER_GRID', 'appointmentNumber', 'Appointment Number','string', TRUE);
INSERT INTO global_fieldmaster VALUES(9, 'ASN_OUTER_GRID', 'originatingLocation', 'Originating Location','string', TRUE);
INSERT INTO global_fieldmaster VALUES(10, 'ASN_OUTER_GRID', 'destinationLocation', 'Destination Location','string', TRUE);
INSERT INTO global_fieldmaster VALUES(11, 'ASN_OUTER_GRID', 'sealNumber', 'Seal Number','string', TRUE);
INSERT INTO global_fieldmaster VALUES(12, 'ASN_OUTER_GRID', 'referenceDocumentNumber', 'Reference Document Number','string', TRUE);
INSERT INTO global_fieldmaster VALUES(13, 'ASN_OUTER_GRID', 'vendorNumber', 'Vendor Number','string', TRUE);
INSERT INTO global_fieldmaster VALUES(14, 'ASN_OUTER_GRID', 'vendorName', 'Vendor Name','string', TRUE);
INSERT INTO global_fieldmaster VALUES(15, 'ASN_OUTER_GRID', 'vendorAsnNumber', 'Vendor ASN Number','string', TRUE);
INSERT INTO global_fieldmaster VALUES(16, 'ASN_OUTER_GRID', 'asnSentBy', 'ASN Sent by','string', TRUE);
INSERT INTO global_fieldmaster VALUES(17, 'ASN_OUTER_GRID', 'carrierName', 'Carrier Name','string', TRUE);
INSERT INTO global_fieldmaster VALUES(18, 'ASN_OUTER_GRID', 'billOfLadingNumber', 'Bill of Lading Number','string', TRUE);
INSERT INTO global_fieldmaster VALUES(19, 'ASN_OUTER_GRID', 'driverName', 'Driver Name','string', TRUE);
INSERT INTO global_fieldmaster VALUES(20, 'ASN_OUTER_GRID', 'vehicleNumber', 'Vehicle Number','string', TRUE);
INSERT INTO global_fieldmaster VALUES(21, 'ASN_OUTER_GRID', 'trackingNumber', 'Tracking Number','string', TRUE);
INSERT INTO global_fieldmaster VALUES(22, 'ASN_OUTER_GRID', 'packagingOptions', 'Packaging Options','string', TRUE);

CREATE TABLE user_selected_fields (
    userId INT UNSIGNED NOT NULL,
    category ENUM('ASN_OUTER_GRID', 'ASN_INNER_GRID') NOT NULL,
    selectedFields JSON NOT NULL,
    PRIMARY KEY (userId, category),
    FOREIGN KEY (userId)
        REFERENCES users (sellerId)
);


#26-11-2018 05:30PM
DELETE T1 FROM userSelectedMenus T1 JOIN 
(SELECT T1.roleId, T1.menuId FROM userSelectedMenus T1 JOIN menus T2 ON T1.menuId = T2.menuId WHERE T2.menuId IN (1,2,3,4,5,6)) AS T2
WHERE (T1.roleId, T1.menuId) = (T2.roleId, T2.menuId);

DELETE FROM menus where menuId IN (2,3,4,5,6);  
DELETE FROM menus where menuId = 1;  

INSERT INTO menus VALUES(1, 'Dashboards', NULL, 'assets/Assets/Whendonothoverover/home-normal-1.svg', 'assets/Assets/Whendohoverover/home-active-1.svg', NULL, TRUE, TRUE, TRUE, 1);
INSERT INTO menus VALUES(25, 'Summary-D3', '/summarydashboardD3', NULL, NULL, 1, TRUE, TRUE, FALSE, 6);

UPDATE menus SET parentMenuId=1, menuHierachy=7 WHERE menuId=22;
UPDATE menus SET parentMenuId=1, menuHierachy=8 WHERE menuId=24;


#29-11-2018 10:00AM
ALTER TABLE fieldmaster CHANGE COLUMN ismandotry ismandatory BOOLEAN NOT NULL;


#06-12-2018 02:20AM
alter table userLoginAudit modify originatingIp varchar(128) NOT NULL;
alter table transactionAudit modify originatingIp varchar(128) NOT NULL;


#07-12-2018 11:30AM
UPDATE `sellerportal`.`menus` SET `menuName`='Order' WHERE `menuId`='22';
UPDATE `sellerportal`.`menus` SET `menuName`='Summary' WHERE `menuId`='25';
UPDATE `sellerportal`.`menus` SET `menuName`='Inventory' WHERE `menuId`='26';
UPDATE `sellerportal`.`menus` SET `menuName`='Sales' WHERE `menuId`='24';

UPDATE `sellerportal`.`menus` SET `menuHierachy`='7' WHERE `menuId`='24';
UPDATE `sellerportal`.`menus` SET `menuHierachy`='8' WHERE `menuId`='26';
UPDATE `sellerportal`.`menus` SET `menuHierachy`='9' WHERE `menuId`='22';

delete from userSelectedFunctions where functionId IN (select functionId from menuFunctions where menuId = 12);
delete from userSelectedMenus where menuId = 12;
delete from menuFunctions where menuId = 12;
delete from menus where menuId = 12;

delete from userSelectedFunctions where functionId IN (select functionId from menuFunctions where menuId = 13);
delete from userSelectedMenus where menuId = 13;
delete from menuFunctions where menuId = 13;
delete from menus where menuId = 13;

delete from userSelectedFunctions where functionId IN (select functionId from menuFunctions where menuId = 20);
delete from userSelectedMenus where menuId = 20;
delete from menuFunctions where menuId = 20;
delete from menus where menuId = 20;


#07-12-2018 02:30AM
UPDATE `sellerportal`.`menus` SET `routerPath`='/salesdashboard' WHERE `menuId`='24';
UPDATE `sellerportal`.`menus` SET `routerPath`='/orderdashboard' WHERE `menuId`='22';
UPDATE `sellerportal`.`menus` SET `routerPath`='/summarydashboard' WHERE `menuId`='25';
UPDATE `sellerportal`.`menus` SET `routerPath`='/inventorydashboard' WHERE `menuId`='26';


#10-12-2018 11:30AM
UPDATE menus SET menuName = 'Order Returns'  WHERE  menuId ='9';
UPDATE menus SET menuName = 'Stock Inquiry'  WHERE  menuId ='11';
UPDATE menus SET routerPath = '/stockInquiry' WHERE  menuId ='11';
UPDATE menus SET menuName = 'Orders Inquiry' WHERE  menuId ='8';
UPDATE menus SET routerPath ='/ordersInquiry' WHERE  menuId ='8';


#10-12-2018 06:30PM
CREATE TABLE IF NOT EXISTS productinventoryledger_temp (
    id INT NOT NULL AUTO_INCREMENT,
    product_id INT UNSIGNED NOT NULL,
    availableToPromise INT NOT NULL,
    fulfillableStock INT NOT NULL,
    damagedStock INT NOT NULL,
    reservedStock INT NOT NULL,
    createdOn TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id),
    FOREIGN KEY (product_id)
        REFERENCES productmapper (product_id)
);

CREATE OR REPLACE VIEW product_pivot AS
    (SELECT 
        T1.product_id,
        T2.storeId,
        GROUP_CONCAT(IF(T4.name = 'product_name',
                value,
                NULL)) AS product_name,
        T2.sku AS sku,
        GROUP_CONCAT(IF(T4.name = 'storage_strategy',
                value,
                NULL)) AS storage_strategy,
        GROUP_CONCAT(IF(T4.name = 'parent_category',
                value,
                NULL)) AS parent_category,
        GROUP_CONCAT(IF(T4.name = 'sub_category',
                value,
                NULL)) AS sub_category,
        GROUP_CONCAT(IF(T4.name = 'child_category',
                value,
                NULL)) AS child_category,
        GROUP_CONCAT(IF(T4.name = 'online_availability',
                value,
                NULL)) AS online_availability,
        GROUP_CONCAT(IF(T4.name = 'safety_stock_qty',
                value,
                NULL)) AS safety_stock_qty,
        GROUP_CONCAT(IF(T4.name = 'distressed_stock_qty',
                value,
                NULL)) AS distressed_stock_qty,
        T3.availabletopromise,
        T3.fulfillablestock,
        T3.damagedstock,
        T3.reservedstock,
        T3.updatedOn
    FROM
        productvaluemaster T1
            JOIN
        productmapper T2 ON T1.product_id = T2.product_id
            JOIN
        productinventory T3 ON T3.product_id = T2.product_id
            JOIN
        fieldmaster T4 ON T4.field_id = T1.field_id
    GROUP BY T2.product_id);

insert into productinventoryledger_temp (product_id,availableToPromise,fulfillableStock,damagedStock,reservedStock, createdOn) 
 select product_id,availableToPromise,fulfillableStock,damagedStock,reservedStock,updatedOn from productinventory;
 

#11-12-2018 04:30PM
alter table sellingmode ADD column sellerType enum('REVENUE','NON_REVENUE') NOT NULL DEFAULT 'NON_REVENUE' after sellerSource;


#13-12-2018 05:00PM
CREATE TABLE orders_file_upload_info (
    documentId INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    storeId INT UNSIGNED NOT NULL,
    documentName VARCHAR(32) NOT NULL,
    documentPath VARCHAR(512) NOT NULL,
    documentStatus ENUM('PROCESSING', 'SUCCESS', 'ERROR') NOT NULL,
    uploadedBy VARCHAR(32) NOT NULL,
    uploadedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    processCompletedOn TIMESTAMP NULL DEFAULT NULL,
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);
 insert into menus values(27, 'Orders Upload', '/ordersUpload', null, null, 7, true, true, false, 3);


 #13-12-2018 07:00PM
ALTER TABLE orders_file_upload_info ADD COLUMN succeededOrderCount INT UNSIGNED NULL AFTER documentStatus;
ALTER TABLE orders_file_upload_info ADD COLUMN failedOrderCount INT UNSIGNED NULL AFTER succeededOrderCount;


 #14-12-2018 01:30PM
ALTER TABLE orders ADD COLUMN siteId VARCHAR(64) AFTER storeId;
ALTER TABLE buyer ADD COLUMN siteId JSON AFTER customerId;


 #14-12-2018 06:30PM
alter table new_asn_details modify packagingOptions enum('PALLET','CASE','SKU') NOT NULL;
alter table new_asn_details_temp modify packagingOptions enum('PALLET','CASE','SKU') NOT NULL;


 #17-12-2018 04:30PM
ALTER TABLE orders_file_upload_info MODIFY documentStatus ENUM('PROCESSING','PARTIALLY_SUCCESSFUL','SUCCESS','ERROR') NOT NULL;


 #20-12-2018 01:20PM
ALTER TABLE new_asn_details MODIFY COLUMN driverName VARCHAR(50);
ALTER TABLE new_asn_details MODIFY COLUMN trackingNumber VARCHAR(32);
ALTER TABLE new_asn_details MODIFY COLUMN billOfLadingNumber VARCHAR(32);


 #20-12-2018 03:20PM
ALTER TABLE new_asn_details_temp MODIFY COLUMN toZipCode VARCHAR(10);
ALTER TABLE new_asn_details_temp MODIFY COLUMN fromZipCode VARCHAR(10);


 #27-12-2018 05:20PM
ALTER TABLE sellingmode change COLUMN SFTPFolderPath sftpFolderPath VARCHAR(512) NOT NULL;

update sellingmode set sellerSource = 'VMS01' where sellerSource IN ('VMS01 for VOSSA built Microstore');
update sellingmode set sellerSource = 'EMS01' where sellerSource IN ('EMS01 for External Sellers Store');
update sellingmode set sellerSource = 'EMP02' where sellerSource IN ('EMP02 for Ebay');
update sellingmode set sellerSource = 'EMP01' where sellerSource IN ('EMP01 for Amazon');
update sellingmode set sellerSource = 'TMP01' where sellerSource IN ('TMP01 for CA Direct');
ALTER TABLE sellingmode change COLUMN sellerSource sourceCode ENUM('EMP01','EMP02','TMP01','EMS01','VMS01') NOT NULL AFTER orderPrefix;

update sellingmode set source = 'NOP_MICROSTORE' where sourceCode IN ('VMS01');
update sellingmode set source = 'EXTERNAL' where sourceCode IN ('EMS01');
update sellingmode set source = 'CA_EBAY' where sourceCode IN ('EMP02');
update sellingmode set source = 'CA_AMAZON' where sourceCode IN ('EMP01');
update sellingmode set source = 'CA_DIRECT' where sourceCode IN ('TMP01');
ALTER TABLE sellingmode MODIFY COLUMN source ENUM('CA_AMAZON','CA_EBAY','CA_DIRECT','NOP_MICROSTORE', 'EXTERNAL') NOT NULL AFTER orderPrefix;

ALTER TABLE sellingmode DROP COLUMN SFTPHostURL;
ALTER TABLE sellingmode DROP COLUMN SFTPUserName;
ALTER TABLE sellingmode DROP COLUMN SFTPPassword;
ALTER TABLE sellingmode DROP COLUMN SFTPKeyPath;
ALTER TABLE sellingmode DROP COLUMN APIKey;

alter table sellingmode MODIFY column hostURL VARCHAR(64);
alter table sellingmode MODIFY column hostUserName VARCHAR(64) after hostURL;
alter table sellingmode MODIFY column hostPassword VARCHAR(192) after hostUserName;
alter table sellingmode MODIFY column port INT after hostPassword;

alter table sellingmode ADD column siteId VARCHAR(4) NOT NULL after storeId;
alter table sellingmode ADD column authorizationToken VARCHAR(100) after sourceCode;
alter table sellingmode ADD column refreshToken VARCHAR(60) after authorizationToken;
alter table sellingmode ADD column ftpLoginPrefix VARCHAR(6) after refreshToken;
alter table sellingmode ADD column fulfillmentLocation JSON  NOT NULL after sftpFolderPath;
alter table sellingmode ADD column returnHandlingLocation JSON  NOT NULL after fulfillmentLocation;

update sellingmode set siteId = 0;
update sellingmode set authorizationToken = 'Basic N2VlaGEza2xheTdla2ZmZHdoazQ3OTkzODBsYXFgdfcysgyuk+++6XAtNVFGRGdvVTJpTEdRdDQ4bFA0Zw=';
update sellingmode set refreshToken = 'sOEoPgEDRF678==_RgWpk7ZvSjzOcMeLFZsgfjgcLOyrxU';
update sellingmode set ftpLoginPrefix = 'OMLFZU';

update sellingmode set authorizationToken = null where source IN ('EXTERNAL', 'NOP_MICROSTORE');
update sellingmode set refreshToken = null where source IN ('EXTERNAL', 'NOP_MICROSTORE');
update sellingmode set ftpLoginPrefix = null where source IN ('EXTERNAL', 'NOP_MICROSTORE');

update sellingmode set fulfillmentLocation = '["Vert fulfillment center"]';
update sellingmode set returnHandlingLocation = '["Vert fulfillment center"]';
 
SET @a = 0;
update sellingmode set orderPrefix = CONCAT('C', LPAD(@a:=@a+1, 4, '0')) where sourceCode IN ('EMP01', 'EMP02', 'TMP01');
 
SET @a = 0;
update sellingmode set orderPrefix = CONCAT('E', LPAD(@a:=@a+1, 4, '0')) where sourceCode IN ('EMS01');
 
SET @a = 0;
update sellingmode set orderPrefix = CONCAT('N', LPAD(@a:=@a+1, 4, '0')) where sourceCode IN ('VMS01');

ALTER TABLE sellingmode change COLUMN currentSellingMode sellingMode VARCHAR(64) NOT NULL AFTER siteId;


 #01-01-2018 03:20PM
ALTER TABLE sellingmode ADD COLUMN sellingModeStatus ENUM('ACTIVE','INACTIVE') NOT NULL DEFAULT 'INACTIVE' AFTER sellingMode;
ALTER TABLE sellingmode MODIFY COLUMN sourceCode ENUM('EMP01','EMP02','TMP01', 'VMS01', 'EMS01') NOT NULL AFTER orderPrefix;
ALTER TABLE sellingmode MODIFY COLUMN source ENUM('CA_AMAZON','CA_EBAY','CA_DIRECT','NOP_MICROSTORE', 'EXTERNAL') NOT NULL AFTER sourceCode;