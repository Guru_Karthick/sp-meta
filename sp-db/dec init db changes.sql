
 #01-01-2019 05:20PM
 CREATE TABLE inventory (
    productId INT UNSIGNED NOT NULL UNIQUE,
    onHandStockQty INT UNSIGNED NOT NULL,
    reservedStockQty INT UNSIGNED NOT NULL,
    updatedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (productId),
    FOREIGN KEY (productId)
        REFERENCES productmapper (product_id)
);
insert into inventory(productId, onHandStockQty, reservedStockQty, updatedOn) (select product_id, availableToPromise, reservedStock, updatedOn FROM productinventory);

CREATE TABLE inventory_future_stock (
   id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
   productId INT UNSIGNED NOT NULL,
   futureStockQty INT UNSIGNED NOT NULL,
   refilledOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   UNIQUE (productId , refilledOn),
   FOREIGN KEY (productId)
       REFERENCES inventory (productId)
);
insert into inventory_future_stock(productId, futureStockQty, refilledOn) (select product_id, fulfillableStock, updatedOn FROM productinventory);


 #03-01-2019 06:20PM
ALTER TABLE orders MODIFY COLUMN orderStatus ENUM('Processing', 'Ready for shipment','Partially Shipped','Shipped','Partially Delivered','Delivered','Cancelled') NOT NULL;

ALTER TABLE orders ADD COLUMN reasonForCancelling VARCHAR(255) NULL;
ALTER TABLE orders ADD COLUMN remarks VARCHAR(255) NULL;


 #04-01-2019 12:40PM
ALTER TABLE orderreturn ADD COLUMN rmaStatus ENUM('REQUESTED','APPROVED', 'REJECTED') NOT NULL AFTER returnStatus;

update orderreturn set returnStatus = 'RETURN_REQUESTED';

ALTER TABLE orderreturn MODIFY COLUMN returnStatus ENUM('RETURN_REQUESTED', 'RETURN_AUTHORIZED', 'RETURN_REJECTED', 'ITEM_RECEIVED',
'ITEM_ACCEPTED','REFUND_INITIATED', 'REFUND_COMPLETE' ) NOT NULL ;

ALTER TABLE orderreturn ADD COLUMN  returnedProductImage varchar(512) after reasonForReturn;

ALTER TABLE orderreturn MODIFY refundAmount FLOAT unsigned;
UPDATE orderreturn SET refundAmount = NULL;


 #04-01-2019 06:10PM
ALTER TABLE orders MODIFY COLUMN orderStatus ENUM('Processing', 'Fulfillment Initiated', 'Ready for shipment','Partially Shipped','Shipped','Partially Delivered','Delivered','Cancelled') NOT NULL;


 #07-01-2019 03:10PM 
ALTER TABLE orderreturn ADD COLUMN rmaRejectedReason varchar(255) NULL;
ALTER TABLE orderreturn ADD COLUMN rmaRemarks varchar(255) NULL;

ALTER TABLE global_fieldmaster MODIFY COLUMN category ENUM('ASN_OUTER_GRID','ASN_INNER_GRID', 'ORDER_RETURN_GRID', 'ORDER_RMA_GRID') NOT NULL;
ALTER TABLE user_selected_fields MODIFY COLUMN category ENUM('ASN_OUTER_GRID','ASN_INNER_GRID', 'ORDER_RETURN_GRID', 'ORDER_RMA_GRID') NOT NULL;

INSERT INTO global_fieldmaster VALUES(23, 'ORDER_RETURN_GRID', 'orderReturnId', 'RMA No','string', TRUE);
INSERT INTO global_fieldmaster VALUES(24, 'ORDER_RETURN_GRID', 'orderId', 'Order ID','string', TRUE);
INSERT INTO global_fieldmaster VALUES(25, 'ORDER_RETURN_GRID', 'source', 'Source','string', TRUE);
INSERT INTO global_fieldmaster VALUES(26, 'ORDER_RETURN_GRID', 'customerName', 'Customer Name','string', TRUE);
INSERT INTO global_fieldmaster VALUES(27, 'ORDER_RETURN_GRID', 'sku', 'SKU','string', TRUE);
INSERT INTO global_fieldmaster VALUES(28, 'ORDER_RETURN_GRID', 'productName', 'Product Name','string', TRUE);
INSERT INTO global_fieldmaster VALUES(29, 'ORDER_RETURN_GRID', 'orderedQuantity', 'Order Quantity','number', TRUE);
INSERT INTO global_fieldmaster VALUES(30, 'ORDER_RETURN_GRID', 'returnedQuantity', 'Return Quantity','number', TRUE);
INSERT INTO global_fieldmaster VALUES(31, 'ORDER_RETURN_GRID', 'purchasedOn', 'Purchased On','datetime', TRUE);
INSERT INTO global_fieldmaster VALUES(32, 'ORDER_RETURN_GRID', 'returnRequestedOn', 'Return Requested On','datetime', TRUE);
INSERT INTO global_fieldmaster VALUES(33, 'ORDER_RETURN_GRID', 'reasonForReturn', 'Reason for Return','string', TRUE);
INSERT INTO global_fieldmaster VALUES(34, 'ORDER_RETURN_GRID', 'returnStatus', 'Return Status','string', TRUE);
INSERT INTO global_fieldmaster VALUES(35, 'ORDER_RETURN_GRID', 'returnedProductImage', 'Product Image','string', TRUE);
INSERT INTO global_fieldmaster VALUES(36, 'ORDER_RETURN_GRID', 'manufacturer', 'Manufacturer Name','string', TRUE);
INSERT INTO global_fieldmaster VALUES(37, 'ORDER_RETURN_GRID', 'additionalComments', 'Comments','string', TRUE);

INSERT INTO global_fieldmaster VALUES(38, 'ORDER_RMA_GRID', 'orderReturnId', 'RMA No','string', TRUE);
INSERT INTO global_fieldmaster VALUES(39, 'ORDER_RMA_GRID', 'orderId', 'Order ID','string', TRUE);
INSERT INTO global_fieldmaster VALUES(40, 'ORDER_RMA_GRID', 'source', 'Source','string', TRUE);
INSERT INTO global_fieldmaster VALUES(41, 'ORDER_RMA_GRID', 'customerName', 'Customer Name','string', TRUE);
INSERT INTO global_fieldmaster VALUES(42, 'ORDER_RMA_GRID', 'sku', 'SKU','string', TRUE);
INSERT INTO global_fieldmaster VALUES(43, 'ORDER_RMA_GRID', 'productName', 'Product Name','string', TRUE);
INSERT INTO global_fieldmaster VALUES(44, 'ORDER_RMA_GRID', 'orderedQuantity', 'Order Quantity','number', TRUE);
INSERT INTO global_fieldmaster VALUES(45, 'ORDER_RMA_GRID', 'returnedQuantity', 'Return Quantity','number', TRUE);
INSERT INTO global_fieldmaster VALUES(46, 'ORDER_RMA_GRID', 'returnRequestedOn', 'Return Requested On','datetime', TRUE);
INSERT INTO global_fieldmaster VALUES(47, 'ORDER_RMA_GRID', 'reasonForReturn', 'Reason for Return','string', TRUE);
INSERT INTO global_fieldmaster VALUES(48, 'ORDER_RMA_GRID', 'rmaStatus', 'RMA Status','string', TRUE);


 #17-01-2019 03:10PM 
ALTER TABLE inventory ADD COLUMN availableToPromiseQty INT UNSIGNED NOT NULL AFTER onHandStockQty;
UPDATE inventory SET availableToPromiseQty = onHandStockQty;

UPDATE menus SET menuName='Manage Orders', routerPath='/manageOrders' WHERE menuId=8;
UPDATE menus SET menuName='Manage Returns', routerPath='/manageReturns' WHERE menuId=9;
UPDATE menus SET menuName='Manage Returns', routerPath='/manageOrders/manage-rma' WHERE menuId=1008;

INSERT INTO productvaluemaster (SELECT product_id, 91, 1 FROM productmapper WHERE product_id NOT IN
(SELECT product_id FROM productvaluemaster WHERE field_id = 91));


 #18-01-2019 11:50AM 
CREATE TABLE notification_preferences (
    storeId INT UNSIGNED NOT NULL,
    notificationCategory ENUM('LOW_STOCK', 'DISTRESSED_STOCK') NOT NULL,
    notificationType ENUM('EMAIL', 'PHONE', 'BOTH') NOT NULL,
	emailIds JSON,
	phoneNumbers JSON,
    notificationEnabled BOOLEAN NOT NULL DEFAULT FALSE,
    UNIQUE (storeId , notificationCategory),
    PRIMARY KEY (storeId , notificationCategory),
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);

CREATE TABLE user_notifications (
    notificationId INT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY,
    storeId INT UNSIGNED NOT NULL,
    notificationCategory ENUM('LOW_STOCK', 'DISTRESSED_STOCK'),
    message VARCHAR(256) NOT NULL,
    notifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);


 #25-01-2019 05:00PM 
UPDATE `sp_dec`.`menus` SET `menuName`='Manage Returns Pickup' WHERE `menuId`='9';


 #28-01-2019 07:00PM 
 CREATE TABLE order_shipment (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY,
    shipmentNumber VARCHAR(32) NOT NULL UNIQUE,
    shipEngineId VARCHAR(12) UNIQUE,
    orderId VARCHAR(32) NOT NULL,
    storeId INT UNSIGNED NOT NULL,
    shipmentDate TIMESTAMP NOT NULL,
    isCustomShipment BOOLEAN NOT NULL,
    carrierId VARCHAR(12),
    carrierCode VARCHAR(32),
    serviceCode VARCHAR(64),
    trackingNumber VARCHAR(32),
    from_name VARCHAR(80) NOT NULL,
    from_phoneNumber VARCHAR(16) NOT NULL,
    from_address1 VARCHAR(80) NOT NULL,
    from_address2 VARCHAR(80) NOT NULL,
    from_address3 VARCHAR(80) NOT NULL,
    from_city VARCHAR(32) NOT NULL,
    from_state VARCHAR(32) NOT NULL,
    from_country VARCHAR(32) NOT NULL,
    from_zipCode VARCHAR(5) NOT NULL,
    labelId VARCHAR(24),
    labelHrefUrl VARCHAR(512),
    deliveryPersonName VARCHAR(64),
    deliveryPersonPhone VARCHAR(16),
    deliveryPersonVehicleNumber VARCHAR(16),
    createdBy VARCHAR(32) NOT NULL,
    createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modifiedBy VARCHAR(32) NOT NULL,
    modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (orderId , storeId)
        REFERENCES orders (orderId , storeId)
);
ALTER TABLE order_shipment ADD COLUMN shipmentStatus ENUM('PROCESSING') NOT NULL DEFAULT 'PROCESSING' AFTER storeId;

CREATE TABLE order_shipment_package (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY,
    packageNumber VARCHAR(32) NOT NULL UNIQUE,
    shipmentId INT UNSIGNED NOT NULL,
    packageCode VARCHAR(64),
    weight DECIMAL(12 , 2 ),
    length DECIMAL(12 , 2 ),
    width DECIMAL(12 , 2 ),
    height DECIMAL(12 , 2 ),
    estimatedShipingCost DECIMAL(6 , 2 ),
    estimatedDeliveryDate TIMESTAMP,
    productId INT UNSIGNED NOT NULL,
    itemQuantity INT UNSIGNED NOT NULL,
    FOREIGN KEY (shipmentId)
        REFERENCES order_shipment (id),
    FOREIGN KEY (productId)
        REFERENCES orderitem (product_id)
);


 #31-01-2019 05:30PM 
 CREATE TABLE tariff (
    tariffId INT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY,
    tariffNumber VARCHAR(18) NULL UNIQUE,
    tariffStatus ENUM('FRESH', 'APPROVED', 'CANCELLED') NOT NULL DEFAULT 'FRESH',
    tariffDescription VARCHAR(60) NOT NULL,
    tariffType ENUM('BUY', 'SELL') NOT NULL,
    tariffElement ENUM('GMV_CHARGES', 'SHIPPING_CHARGES_CARRIER', 'TAX_SELL', 'ORDER_PROCESSING_FEE', 'HOSTING_CHARGES', 'ADDITIONAL_CHARGES') NOT NULL,
    validFrom TIMESTAMP NOT NULL default 0,
    validTo TIMESTAMP NOT NULL default 0,
    chargeType ENUM('FLAT', 'SLAB_BASED', 'FORMULA_BASED') NOT NULL DEFAULT 'FLAT',
    slabRangeFrom FLOAT,
    slabRangeTo FLOAT,
    chargeMetric ENUM('PERCENT', 'RATE', 'FORMULA'),
    formulaValue DECIMAL(13 , 3 ) NOT NULL,
    uom ENUM('UNIT', 'TRANSACTION') NOT NULL,
    chargeFrequency ENUM('DAILY', 'WEEKLY', 'FORTNIGHTLY', 'MONTHLY', 'ANNUALLY') NOT NULL DEFAULT 'DAILY',
    createdBy VARCHAR(32) NOT NULL,
    createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modifiedBy VARCHAR(32) NOT NULL,
    modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

alter table tariff modify column validFrom TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
alter table tariff modify column validTo TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE orderitem ADD COLUMN shippedQuantity INT UNSIGNED NOT NULL DEFAULT 0 AFTER quantity;


 #05-02-2019 03:30PM 
CREATE TABLE contract (
    contractId INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    contractNumber VARCHAR(18) NOT NULL UNIQUE,
    amendmentNumber INT UNSIGNED NOT NULL,
    storeId INT UNSIGNED NOT NULL,
    contractDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    contractDescription VARCHAR(60) NOT NULL,
    contractStatus ENUM('FRESH', 'CONFIRMED', 'CANCELLED', 'AMENDED_FRESH', 'AMENDMENT_CONFIRMED') NOT NULL DEFAULT 'FRESH',
    validityStatus ENUM('VALID', 'INVALID') NOT NULL DEFAULT 'VALID',
    effectiveFrom TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    effectiveTo TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    currencyType ENUM('IND', 'USD') NOT NULL,
    contractLevelDiscount INT UNSIGNED,
    remarks VARCHAR(255),
    referenceDocumentNumber VARCHAR(60),
    paymentTerm VARCHAR(20),
    createdBy VARCHAR(32) NOT NULL,
    createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modifiedBy VARCHAR(32) NOT NULL,
    modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (storeId)
        REFERENCES stores (storeId)
);

CREATE TABLE contract_tariff_mapping (
    contractId INT UNSIGNED NOT NULL,
    tariffId INT UNSIGNED NOT NULL,
    UNIQUE (contractId , tariffId),
    PRIMARY KEY (contractId , tariffId),
    FOREIGN KEY (contractId)
        REFERENCES contract (contractId),
    FOREIGN KEY (tariffId)
        REFERENCES tariff (tariffId)
);

CREATE TABLE contract_documents (
   id INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
   contractId INT UNSIGNED NOT NULL,
   documentName VARCHAR(32) NOT NULL,
   documentPath VARCHAR(512) NOT NULL,
   UNIQUE (contractId , documentName),
   FOREIGN KEY (contractId)
       REFERENCES contract (contractId)
);

ALTER TABLE sellingmode ADD COLUMN contractId INT(10) UNSIGNED DEFAULT NULL AFTER storeId;
ALTER TABLE sellingmode ADD CONSTRAINT FOREIGN KEY (contractId) REFERENCES contract (contractId);


 #07-02-2019 06:30PM 
alter table contract modify column currencyType ENUM('INR', 'USD', 'IND') NOT NULL;
update contract set currencyType = 'INR' where currencyType = 'IND';
alter table contract modify column currencyType ENUM('INR', 'USD', 'IND') NOT NULL;


insert into vertfeatures values(4, 'manageTariff', 'Manage Tariff', null, 1, 1, 1);
insert into vertfeatures values(5, 'manageContract', 'Manage Contract', null, 1, 1, 1);


 #11-02-2019 08:30PM 
CREATE TABLE picked_for_delivery (
    deliveryId INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    deilveryBoyId INT(10) UNSIGNED NOT NULL,
    shipmentId INT(10) UNSIGNED NOT NULL UNIQUE,
    deliveryStatus ENUM('PENDING', 'DELIVERED', 'FAILED_TO_DELIVER') NOT NULL DEFAULT 'PENDING',
    noOfAttempts TINYINT UNSIGNED NOT NULL DEFAULT 0,
    failureReason VARCHAR(256),
    FOREIGN KEY (deilveryBoyId)
        REFERENCES users (sellerId),
    FOREIGN KEY (shipmentId)
        REFERENCES order_shipment (id)
);

INSERT INTO menus VALUES(1100, 'Delivery', NULL, 'assets/Assets/Whendonothoverover/home-normal-1.svg', 'assets/Assets/Whendohoverover/home-active-1.svg', NULL, TRUE, TRUE, TRUE, 110);
INSERT INTO menus VALUES(1101, 'Manage Delivery', '/manage-delivery', NULL, NULL, 1100, TRUE, TRUE, FALSE, 1);
INSERT INTO menus VALUES(1102, 'My Tasks', '/mytasks', NULL, NULL, 1100, TRUE, TRUE, FALSE, 2);

ALTER TABLE order_shipment MODIFY COLUMN shipmentStatus enum('PROCESSING','DELIVERED','FAILED_TO_DELIVER') NOT NULL DEFAULT 'PROCESSING';


