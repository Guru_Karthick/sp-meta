set sql_safe_updates = 0;
set sql_mode = 0;


#27.06.18
	#orders 
DROP INDEX orderId ON orders;
alter table orders
add constraint unique(orderId, storeId);

alter table orders
DROP primary key;
alter table orders
add constraint primary key(orderId, storeId);

	#orderlist (drop all existing foreign keys)
alter table orderlist
add column storeId INT UNSIGNED NOT NULL after orderId;

UPDATE orderlist AS t1
  INNER JOIN orders AS t2 ON t1.orderId = t2.orderId
SET t1.storeId = t2.storeId;

alter table orderlist
add constraint foreign key(orderId, storeId) references orders(orderId, storeId);

alter table orderlist
DROP primary key;

alter table orderlist
add primary key(orderId, storeId, product_id);

	#customers (drop all existing foreign keys)
alter table customers
add column storeId INT UNSIGNED NOT NULL after orderId;

UPDATE customers AS t1
  INNER JOIN orders AS t2 ON t1.orderId = t2.orderId
SET t1.storeId = t2.storeId;

alter table customers
add constraint foreign key(orderId, storeId) references orders(orderId, storeId);

alter table customers
DROP primary key;

alter table customers
add primary key(orderId, storeId);


#29.6.18
alter table subfeatures
add column subfeatureId INT UNSIGNED AUTO_INCREMENT NOT NULL UNIQUE first;

CREATE TABLE apilist (
    apiId INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
    methodName VARCHAR(64)BINARY NOT NULL,
    className VARCHAR(64)BINARY NOT NULL,
    UNIQUE (methodName , className)
);
INSERT INTO apilist (methodName, className) values ('storelogin', 'general');
INSERT INTO apilist (methodName, className) values ('createuser', 'general');
INSERT INTO apilist (methodName, className) values ('updateuser', 'general');
INSERT INTO apilist (methodName, className) values ('updatestorebankdetails', 'general');
INSERT INTO apilist (methodName, className) values ('updatestoretaxdetails', 'general');
INSERT INTO apilist (methodName, className) values ('getallstoreusers', 'general');
INSERT INTO apilist (methodName, className) values ('checkfielduser', 'general');
INSERT INTO apilist (methodName, className) values ('resetpassword', 'general');
INSERT INTO apilist (methodName, className) values ('getalluserstatus', 'general');
INSERT INTO apilist (methodName, className) values ('updateuserstatus', 'general');
INSERT INTO apilist (methodName, className) values ('getuserdetails', 'general');
INSERT INTO apilist (methodName, className) values ('edituserdetails', 'general');

INSERT INTO apilist (methodName, className) values ('getdashboarddetails', 'dashboard');

INSERT INTO apilist (methodName, className) values ('getsalesdetails', 'sales');

INSERT INTO apilist (methodName, className) values ('generateasnnumber', 'asn');
INSERT INTO apilist (methodName, className) values ('checkasnnumber', 'asn');
INSERT INTO apilist (methodName, className) values ('createasn', 'asn');
INSERT INTO apilist (methodName, className) values ('updateasn', 'asn');
INSERT INTO apilist (methodName, className) values ('getallasn', 'asn');
INSERT INTO apilist (methodName, className) values ('getfilteredasn', 'asn');
INSERT INTO apilist (methodName, className) values ('deleteasn', 'asn');

INSERT INTO apilist (methodName, className) values ('getallproducts', 'product');
INSERT INTO apilist (methodName, className) values ('getpageproducts', 'product');
INSERT INTO apilist (methodName, className) values ('getpageproductsdownload', 'product');
INSERT INTO apilist (methodName, className) values ('getselectedproductsdownload', 'product');
INSERT INTO apilist (methodName, className) values ('getproductcount', 'product');
INSERT INTO apilist (methodName, className) values ('addsingleproduct', 'product');
INSERT INTO apilist (methodName, className) values ('addbulkproducts', 'product');
INSERT INTO apilist (methodName, className) values ('editbulkproducts', 'product');
INSERT INTO apilist (methodName, className) values ('deleteproduct', 'product');
INSERT INTO apilist (methodName, className) values ('filterproduct', 'product');
INSERT INTO apilist (methodName, className) values ('getadvancefilter', 'product');
INSERT INTO apilist (methodName, className) values ('getallbrands', 'product');
INSERT INTO apilist (methodName, className) values ('getallfields', 'product');
INSERT INTO apilist (methodName, className) values ('setselectedfields', 'product');
INSERT INTO apilist (methodName, className) values ('getsortedfields', 'product');

INSERT INTO apilist (methodName, className) values ('getproductinventory', 'product');
INSERT INTO apilist (methodName, className) values ('getpageproductinventory', 'product');
INSERT INTO apilist (methodName, className) values ('getproductinventorycount', 'product');

INSERT INTO apilist (methodName, className) values ('getallorders', 'order');
INSERT INTO apilist (methodName, className) values ('getpageorders', 'order');
INSERT INTO apilist (methodName, className) values ('getordercount', 'order');

INSERT INTO apilist (methodName, className) values ('getmanagemeta', 'role');
INSERT INTO apilist (methodName, className) values ('getroleslist', 'role');
INSERT INTO apilist (methodName, className) values ('createrole', 'role');
INSERT INTO apilist (methodName, className) values ('updaterole', 'role');
INSERT INTO apilist (methodName, className) values ('storeroles', 'role');
INSERT INTO apilist (methodName, className) values ('getassignedroles', 'role');
INSERT INTO apilist (methodName, className) values ('getpermissions', 'role');

CREATE TABLE apicontrol (
    apiId INT UNSIGNED NOT NULL,
    featureId INT UNSIGNED NOT NULL,
    subfeatureId INT UNSIGNED,
    reqFeatureVal TINYINT UNSIGNED NOT NULL,
    reqSubfeatureVal TINYINT UNSIGNED,
    PRIMARY KEY (apiId , featureId),
    UNIQUE (apiId, featureId , subfeatureId),
    FOREIGN KEY (apiId)
        REFERENCES apilist (apiId),
    FOREIGN KEY (featureId)
        REFERENCES features (id),
    FOREIGN KEY (subfeatureId)
        REFERENCES subfeatures (subfeatureId)
);
#User Management (General)
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (2, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (3, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (4, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (5, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (6, 6, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (7, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (8, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (9, 6, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (10, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (11, 6, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (12, 6, null, 3, null);

#Dashboard
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (13, 1, null, 2, null);

#Sales
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (14, 3, null, 2, null);

#ASN
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (15, 8, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (16, 8, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (17, 8, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (18, 8, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (19, 8, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (20, 8, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (21, 8, null, 3, null);

#Product-Catalog
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (22, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (23, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (24, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (25, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (26, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (27, 2, 2, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (28, 2, 1, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (29, 2, 1, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (30, 2, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (31, 2, 1, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (32, 2, 1, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (33, 2, 1, 3, 3);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (34, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (35, 2, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (36, 2, null, 2, null);

#Inventory
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (37, 5, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (38, 5, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (39, 5, null, 2, null);

#Order
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (40, 4, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (41, 4, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (42, 4, null, 2, null);

#User Management (Roles)
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (43, 6, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (44, 6, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (45, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (46, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (47, 6, null, 3, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (48, 6, null, 2, null);


#3.7.18
drop table caseitems;
drop table cases;
drop table asn;

alter table storeusermapper
add constraint unique(sellerId, storeId);


#4.7.18
CREATE TABLE orderreturn (
    orderReturnId VARCHAR(32) NOT NULL,
    orderId VARCHAR(32) NOT NULL,
    storeId INT UNSIGNED NOT NULL,
    product_id INT UNSIGNED NOT NULL,
    returnedQuantity INT UNSIGNED NOT NULL,
    returnStatus VARCHAR(32) NOT NULL,
    refundAmount FLOAT UNSIGNED NOT NULL,
    returnRequestedOn TIMESTAMP NOT NULL DEFAULT now(),
    reasonForReturn VARCHAR(1024) NOT NULL,
    additionalComments VARCHAR(1024),
    PRIMARY KEY (orderReturnId , orderId , storeId),
    FOREIGN KEY (orderId , storeId , product_id)
        REFERENCES orderlist (orderId , storeId , product_id)
);

CREATE TABLE asn (
    asnId INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    asnNumber VARCHAR(16)BINARY NOT NULL UNIQUE,
    sellerId INT UNSIGNED NOT NULL,
    storeId INT UNSIGNED NOT NULL,
    asnStatus VARCHAR(16) NOT NULL DEFAULT 'Inprogress',
    asnType VARCHAR(16) NOT NULL,
    shipmentType VARCHAR(16) NOT NULL,
    deliveryFacility VARCHAR(64),
    referenceNumber VARCHAR(64),
    bolNumber VARCHAR(64),
    carrier VARCHAR(128),
    shippingMethod VARCHAR(64) NOT NULL,
    trackingNumber VARCHAR(64),
    shipmentDate TIMESTAMP NULL DEFAULT NULL,
    estimatedArrival TIMESTAMP NULL DEFAULT NULL,
    modeofTransport VARCHAR(32),
    shipmentFrom VARCHAR(32),
    cogi FLOAT UNSIGNED,
    additionalNotes VARCHAR(1024),
    asnDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    createdBy VARCHAR(32)BINARY NOT NULL,
    createdOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modifiedBy VARCHAR(32)BINARY NOT NULL,
    modifiedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (sellerId , storeId)
        REFERENCES storeusermapper (sellerId , storeId)
);
CREATE TABLE asnpalletlevel (
    palletId INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    asnId INT UNSIGNED NOT NULL,
    palletLpn VARCHAR(64) NOT NULL,
    palletLength FLOAT UNSIGNED,
    palletWidth FLOAT UNSIGNED,
    palletHeight FLOAT UNSIGNED,
    palletDimensionUOM VARCHAR(16),
    palletWeight FLOAT UNSIGNED,
    palletWeightUOM VARCHAR(16),
    UNIQUE (asnId , palletLpn),
    FOREIGN KEY (asnId)
        REFERENCES asn (asnId)
);
CREATE TABLE asncaselevel (
    caseId INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    asnId INT UNSIGNED NOT NULL,
    palletId INT UNSIGNED,
    caseLpn VARCHAR(64) NOT NULL,
    caseLength FLOAT UNSIGNED,
    caseWidth FLOAT UNSIGNED,
    caseHeight FLOAT UNSIGNED,
    caseDimensionUOM VARCHAR(16),
    caseWeight FLOAT UNSIGNED,
    caseWeightUOM VARCHAR(16),
    specialTreatmentNeeded VARCHAR(3),
    isMaterialHazardous VARCHAR(3),
    UNIQUE (asnId , caseLpn),
    FOREIGN KEY (asnId)
        REFERENCES asn (asnId),
    FOREIGN KEY (palletId)
        REFERENCES asnpalletlevel (palletId)
);
CREATE TABLE asnskulevel (
    id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    asnId INT UNSIGNED NOT NULL,
    palletId INT UNSIGNED,
    caseId INT UNSIGNED NOT NULL,
    product_id INT UNSIGNED NOT NULL,
    quantity INT UNSIGNED NOT NULL,
    status VARCHAR(16),
    UNIQUE (caseId , product_id),
    FOREIGN KEY (asnId)
        REFERENCES asn (asnId),
    FOREIGN KEY (palletId)
        REFERENCES asnpalletlevel (palletId),
    FOREIGN KEY (caseId)
        REFERENCES asncaselevel (caseId),
    FOREIGN KEY (product_id)
        REFERENCES productmapper (product_id)
);

##Role Actions for Orders Page

UPDATE `sellerportal`.`features` SET `view_edit`='0' WHERE `id`='4';
insert into subfeatures (id,feature,none,view,view_edit) values(2,"Order Management",1,1,0) ;
insert into subfeatures (id,feature,none,view,view_edit) values(2,"Return Management",1,1,0) ;
UPDATE `sellerportal`.`features` SET `sub_feature_id`='2' WHERE `id`='4';


#6.7.18 - 03:35PM
alter table roles
add column description varchar(512) not null after storeId;
update roles set description = name;


#6.7.18 - 06:00PM
alter table orderlist
add column price FLOAT UNSIGNED NOT NULL after quantity;

UPDATE orderlist
INNER JOIN productvaluemaster ON orderlist.product_id = productvaluemaster.product_id
SET orderlist.price = CAST(productvaluemaster.value AS DECIMAL(10,2))
WHERE productvaluemaster.field_id = 9;


#9.7.18 - 12:15PM
alter table users add column rp_token varchar(256) default null after password;
alter table users add column rp_token_created_at timestamp default 0 after rp_token ;


#11.7.18 - 1:15PM
delete from apicontrol where apiId = (select apiId from apilist where methodName = 'getalluserstatus');
delete from apilist where methodName = 'getalluserstatus';


#12.7.18 - 4:10PM
alter table users drop column rp_token;
alter table users drop column rp_token_created_at;

CREATE TABLE forgotpassword (
    sellerId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    jti VARCHAR(36)BINARY NOT NULL UNIQUE,
    FOREIGN KEY (sellerId)
        REFERENCES users (sellerId)
);


#13.7.18 - 12:40PM
INSERT INTO productinventory (product_id, availableToPromise, fulfillableStock, damagedStock, reservedStock)
SELECT product_id, 0, 0, 0, 0 FROM productmapper
WHERE product_id NOT IN (SELECT product_id FROM productinventory);
