
set sql_safe_updates = 0;
set sql_mode = 0;


CREATE TABLE productmanageprice (
    product_id INT UNSIGNED NOT NULL,
    price FLOAT UNSIGNED NOT NULL,
    scheduled_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (product_id , scheduled_at),
    UNIQUE (product_id , scheduled_at),
    FOREIGN KEY (product_id)
        REFERENCES productmapper (product_id)
);

INSERT INTO productmanageprice (product_id, price, scheduled_at)
SELECT product_id, 0, 0 from productmapper;

UPDATE productmanageprice
INNER JOIN productvaluemaster ON productmanageprice.product_id = productvaluemaster.product_id
SET productmanageprice.price = CAST(productvaluemaster.value AS DECIMAL(10,2))
WHERE productvaluemaster.field_id = 9;

UPDATE productmanageprice
INNER JOIN productvaluemaster ON productmanageprice.product_id = productvaluemaster.product_id
SET productmanageprice.scheduled_at = FROM_UNIXTIME(CAST(productvaluemaster.value AS DECIMAL(15))/1000)
WHERE productvaluemaster.field_id = 74;
