
set sql_safe_updates = 0;
set sql_mode = 0;


#17.7.18 - 6:45PM
alter table apilist auto_increment = 50;

INSERT INTO apilist (methodName, className) values ('getpageadvancefilter', 'product');
INSERT INTO apilist (methodName, className) values ('getpageadvancefilterdownload', 'product');
INSERT INTO apilist (methodName, className) values ('getadvancefiltercount', 'product');

INSERT INTO apilist (methodName, className) values ('forgotpassword', 'general');
INSERT INTO apilist (methodName, className) values ('createpassword', 'general');
INSERT INTO apilist (methodName, className) values ('checktokenexpiry', 'general');

INSERT INTO apilist (methodName, className) values ('getallreturns', 'order');
INSERT INTO apilist (methodName, className) values ('getpagereturns', 'order');
INSERT INTO apilist (methodName, className) values ('getreturncount', 'order');

INSERT INTO apilist (methodName, className) values ('getskuwithname', 'asn');
INSERT INTO apilist (methodName, className) values ('getpageasn', 'asn');
INSERT INTO apilist (methodName, className) values ('getasncount', 'asn');

INSERT INTO apilist (methodName, className) values ('getsearchproducts', 'product');
INSERT INTO apilist (methodName, className) values ('getpagesearchproducts', 'product');
INSERT INTO apilist (methodName, className) values ('getpagesearchproductsdownload', 'product');
INSERT INTO apilist (methodName, className) values ('getsearchproductcount', 'product');
INSERT INTO apilist (methodName, className) values ('getmanageprice', 'product');
INSERT INTO apilist (methodName, className) values ('getpagemanageprice', 'product');
INSERT INTO apilist (methodName, className) values ('getsearchmanageprice', 'product');
INSERT INTO apilist (methodName, className) values ('getpagesearchmanageprice', 'product');
INSERT INTO apilist (methodName, className) values ('getsearchmanagepricecount', 'product');
INSERT INTO apilist (methodName, className) values ('updatemanageprice', 'product');


#18.8.18 - 11:30AM
insert into subfeatures (id,feature,none,view,view_edit) values(1,"Catalog Management",1,1,1);
insert into subfeatures (id,feature,none,view,view_edit) values(1,"Pricing Management",1,1,1);

insert into selectedsubvalues (values_id, feature, selected, none, view, view_edit)
select values_id, 'Return Management', 1, 1, 1, 0 from selectedvalues
WHERE feature = 'Orders Dashboard' AND (values_id, 'Return Management') NOT IN (SELECT values_id, feature FROM selectedsubvalues);

insert into selectedsubvalues (values_id, feature, selected, none, view, view_edit)
select values_id, 'Order Management', 1, 1, 1, 0 from selectedvalues
WHERE feature = 'Orders Dashboard' AND (values_id, 'Order Management') NOT IN (SELECT values_id, feature FROM selectedsubvalues);

insert into selectedsubvalues (values_id, feature, selected, none, view, view_edit)
select values_id, 'Catalog Management', 1, 1, 1, 1 from selectedvalues
WHERE feature = 'Product Catalog' AND (values_id, 'Catalog Management') NOT IN (SELECT values_id, feature FROM selectedsubvalues);

insert into selectedsubvalues (values_id, feature, selected, none, view, view_edit)
select values_id, 'Pricing Management', 1, 1, 1, 1 from selectedvalues
WHERE feature = 'Product Catalog' AND (values_id, 'Pricing Management') NOT IN (SELECT values_id, feature FROM selectedsubvalues);


UPDATE `sellerportal`.`apicontrol` SET `reqSubfeatureVal`='2' WHERE `apiId`='40' and`featureId`='4';
UPDATE `sellerportal`.`apicontrol` SET `reqSubfeatureVal`='2' WHERE `apiId`='41' and`featureId`='4';
UPDATE `sellerportal`.`apicontrol` SET `reqSubfeatureVal`='2' WHERE `apiId`='42' and`featureId`='4';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='3' WHERE `apiId`='40' and`featureId`='4';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='3' WHERE `apiId`='41' and`featureId`='4';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='3' WHERE `apiId`='42' and`featureId`='4';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='5', `reqSubfeatureVal`='2' WHERE `apiId`='22' and`featureId`='2';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='5', `reqSubfeatureVal`='2' WHERE `apiId`='23' and`featureId`='2';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='5', `reqSubfeatureVal`='2' WHERE `apiId`='24' and`featureId`='2';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='5', `reqSubfeatureVal`='2' WHERE `apiId`='25' and`featureId`='2';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='5', `reqSubfeatureVal`='2' WHERE `apiId`='26' and`featureId`='2';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='5', `reqSubfeatureVal`='3' WHERE `apiId`='30' and`featureId`='2';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='5', `reqSubfeatureVal`='2' WHERE `apiId`='34' and`featureId`='2';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='5', `reqSubfeatureVal`='2' WHERE `apiId`='35' and`featureId`='2';
UPDATE `sellerportal`.`apicontrol` SET `subfeatureId`='5', `reqSubfeatureVal`='2' WHERE `apiId`='36' and`featureId`='2';

INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (56, 4, 4, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (57, 4, 4, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (58, 4, 4, 2, 2);

INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (59, 8, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (60, 8, null, 2, null);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (61, 8, null, 2, null);

INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (62, 2, 5, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (63, 2, 5, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (64, 2, 5, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (65, 2, 5, 2, 2);

INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (66, 2, 6, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (67, 2, 6, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (68, 2, 6, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (69, 2, 6, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (70, 2, 6, 2, 2);
INSERT INTO apicontrol (apiId, featureId, subfeatureId, reqFeatureVal, reqSubfeatureVal) values (71, 2, 6, 3, 3);
