
set sql_safe_updates = 0;
set sql_mode = 0;


alter table orders
modify column orderStatus ENUM('Processing', 'Partially Shipped', 'Shipped', 'Delivered', 'Cancelled') DEFAULT 'Processing';