
set sql_safe_updates = 0;
set sql_mode = 0;


#19.7.18 - 10:50AM
alter table users add column isCreatedByVert boolean after firstlogin;
update users set isCreatedByVert = 1 where createdBy = 'admin';
update users set isCreatedByVert = 0 where createdBy <> 'admin';