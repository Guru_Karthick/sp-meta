set sql_safe_updates = 0;
set sql_mode = 0;


#6.7.18 - 03:35PM
alter table roles
add column description varchar(512) not null after storeId;
update roles set description = name;


#6.7.18 - 06:00PM
alter table orderlist
add column price FLOAT UNSIGNED NOT NULL after quantity;

UPDATE orderlist
INNER JOIN productvaluemaster ON orderlist.product_id = productvaluemaster.product_id
SET orderlist.price = CAST(productvaluemaster.value AS DECIMAL(10,2))
WHERE productvaluemaster.field_id = 9;


#9.7.18 - 12:15PM
alter table users add column rp_token varchar(256) default null after password;
alter table users add column rp_token_created_at timestamp default 0 after rp_token ;


#11.7.18 - 1:15PM
delete from apicontrol where apiId = (select apiId from apilist where methodName = 'getalluserstatus');
delete from apilist where methodName = 'getalluserstatus';


#12.7.18 - 4:10PM
alter table users drop column rp_token;
alter table users drop column rp_token_created_at;

CREATE TABLE forgotpassword (
    sellerId INT UNSIGNED NOT NULL UNIQUE PRIMARY KEY,
    jti VARCHAR(36)BINARY NOT NULL UNIQUE,
    FOREIGN KEY (sellerId)
        REFERENCES users (sellerId)
);


#13.7.18 - 12:40PM
INSERT INTO productinventory (product_id, availableToPromise, fulfillableStock, damagedStock, reservedStock)
SELECT product_id, 0, 0, 0, 0 FROM productmapper
WHERE product_id NOT IN (SELECT product_id FROM productinventory);

#13.7.18 - 4:20PM
alter table fieldmaster
add column issearchable BOOLEAN NOT NULL default 0 after ismandotry;

UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='1';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='2';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='3';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='4';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='5';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='6';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='7';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='89';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='15';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='19';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='27';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='29';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='78';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='79';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='41';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='43';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='44';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='47';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='48';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='88';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='87';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='63';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='64';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='65';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='66';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='67';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='68';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='69';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='70';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='71';
UPDATE `sellerportal`.`fieldmaster` SET `issearchable`='1' WHERE `field_id`='72';


#16.7.18 - 5:00PM
CREATE TABLE productmanageprice (
    product_id INT UNSIGNED NOT NULL,
    price FLOAT UNSIGNED NOT NULL,
    scheduled_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (product_id , scheduled_at),
    UNIQUE (product_id , scheduled_at),
    FOREIGN KEY (product_id)
        REFERENCES productmapper (product_id)
);

INSERT INTO productmanageprice (product_id, price, scheduled_at)
SELECT product_id, 0, 0 from productmapper;

UPDATE productmanageprice
INNER JOIN productvaluemaster ON productmanageprice.product_id = productvaluemaster.product_id
SET productmanageprice.price = CAST(productvaluemaster.value AS DECIMAL(10,2))
WHERE productvaluemaster.field_id = 9;

UPDATE productmanageprice
INNER JOIN productvaluemaster ON productmanageprice.product_id = productvaluemaster.product_id
SET productmanageprice.scheduled_at = FROM_UNIXTIME(CAST(productvaluemaster.value AS DECIMAL(15))/1000)
WHERE productvaluemaster.field_id = 74;
