DEV-CLOUD WAR and DB deployment

5.7.18 10:30AM - DB Deployment (apilist, apicontrol, orderreturn, asn, asnpalletlevel, asncaselevel, asnskulevel)
6.7.18 03:35PM - War file and db deployed in SP_DEV (asn,roles)
6.7.18 06:30PM - War file and db deployed in SP_DEV (orderslist)
10.7.18 07:00PM - War file in DEV (CORS in Vertadmin)
12.7.18 05:00PM - WAR file and db deployed in DEV (forgotpassword changes)
16.7.18 1:00PM - WAR file and db deployed in DEV (product search)
16.7.18 5:00PM - WAR file and db deployed in DEV (productmanageprice)
17.7.18 5:00PM - WAR file deployed in DEV
19.7.18 08:45PM - WAR and DB deployed in DEV (Orders fix)
20.7.18 12:50PM - WAR file deployed in DEV (Accept header fix in CORS options)
