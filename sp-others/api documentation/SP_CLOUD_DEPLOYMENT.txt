Installing Java (Ref: http://tipsonubuntu.com/2016/07/31/install-oracle-java-8-9-ubuntu-16-04-linux-mint-18/)
 sudo add-apt-repository ppa:webupd8team/java
 sudo apt update
 sudo apt install oracle-java8-installer
 sudo apt install oracle-java8-set-default

Installing Mysql (Ref: https://support.rackspace.com/how-to/installing-mysql-server-on-ubuntu/)
 sudo apt-get update
 sudo apt-get install mysql-server
 sudo ufw allow mysql
 sudo systemctl start mysql
 sudo systemctl enable mysql

Installing Tomcat 9 (Ref: https://www.digitalocean.com/community/tutorials/how-to-install-apache-tomcat-8-on-ubuntu-16-04)
 sudo apt-get update
 sudo groupadd tomcat
 sudo useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat
 cd /tmp
 curl -O http://apache.mirrors.ionfish.org/tomcat/tomcat-9/v9.0.8/bin/apache-tomcat-9.0.8.tar.gz
 sudo mkdir /opt/tomcat
 sudo tar xzvf apache-tomcat-9*tar.gz -C /opt/tomcat --strip-components=1
 cd /opt/tomcat
 sudo chgrp -R tomcat /opt/tomcat
 sudo chmod -R g+r conf
 sudo chmod g+x conf
 sudo chown -R tomcat webapps/ work/ temp/ logs/
 sudo update-java-alternatives -l
 sudo nano /etc/systemd/system/tomcat.service
 sudo systemctl daemon-reload
 sudo systemctl start tomcat