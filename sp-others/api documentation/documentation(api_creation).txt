

													API CREATION END-TO-END

STEP 1: IN ECLIPSE ENVIRONMENT:

1.file -->other -->dynamic web project
2. give a project name and select the target runtime as Apache Tomcat v9.0
3. choose generate web.xml file and then finish

STEP 2 : AFTER SUCCESSFUL CREATION OF PROJECT
1.src package is found.
2. if we want to create a package inside such as com.stores, com.general classes
3.Inside each package we basically have 3 types of java file sources, which is used for api calls.
4. service, resources ,getter and setter 

example :
Users.java
	public String getSellerId() {
		return sellerId;
	}

	public void setSellerId(String sellerId) {
		this.sellerId = sellerId;
	}

StoreServices.java
	public ResponseClass createUser(Users user, int storeid, String createdBy) {
		PreparedStatement ps;
		try (Connection connection = getDBConnection()) {
			String cipherPassword = AES.encrypt(user.getPassword());

			ps = connection.prepareStatement("INSERT INTO " + Table.users
					+ "(userName, password, name, email, phoneNumber, userType, "
					+ " activefrom, activeto, createdBy, modifiedBy)" + " values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

			ps.setString(1, user.getUserName());
			ps.setString(2, cipherPassword);
			ps.setString(3, user.getName());
			ps.setString(4, user.getEmail());
			ps.setString(5, user.getPhoneNumber());
			ps.setString(6, user.getUserType());
			if (user.getActiveFrom() != 0)
				ps.setTimestamp(7, new Timestamp(user.getActiveFrom()));
			else
				ps.setTimestamp(7, new Timestamp(System.currentTimeMillis()));

			if (user.getActiveTo() != 0)
				ps.setTimestamp(8, new Timestamp(user.getActiveTo()));
			else
				ps.setTimestamp(8, new Timestamp(System.currentTimeMillis() + 86400000));
			ps.setString(9, createdBy);
			ps.setString(10, createdBy);
			ps.execute();
			ps.close();

			StoreServices service = new StoreServices();
			int sellerid = service.getUserid(user.getUserName());
			// int storeid =
			// service.getStoreid(service.getStoreName(service.getUserid(user.getUserName())));
			ps = connection.prepareStatement("INSERT INTO " + Table.storeusermapper + " VALUES (?, ?)");
			ps.setInt(1, sellerid);
			ps.setInt(2, storeid);
			ps.execute();
			ps.close();
			System.out.println("Record Created");

			return new ResponseClass(201, "success", "New SUPER_USER successfully created or registered");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getMessage(), "SUPER_USER not successfully created or registered");
		}
	}
	
StoreResource.java
	@POST
	@Secured
	@Path("createuser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseClass createUser(Users user, @Context HttpHeaders httpHeaders) {

		try {
			String createdBy = httpHeaders.getRequestHeader("userName").get(0);
			String _storename = service.getStoreName(service.findStore(service.getUserid(createdBy)));
			int storeid = service.findStore(service.getUserid(createdBy));
			user.setStoreName(_storename);

			if (!user.getUserName().isEmpty() && !user.getPassword().isEmpty() && !user.getName().isEmpty() 
					&& !user.getEmail().isEmpty() && !user.getUserType().isEmpty())
			{
				return service.createUser(user, storeid, createdBy);
			} 
			else 
			{
				return new ResponseClass(500, "error", "Oops, Mandatory Fiels are not entered. Try again later.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseClass(500, "error", e.getLocalizedMessage(),
					"Oops, something went wrong. Internal server error. Try again later.");
		}
	}
	
STEP 3 :DATABASE CONNECTIONN IS IMPORTANT ADD IT IN GENERAL CLASS

IF WE NEED TO RUN THE PROJECT IN DIFFRENT DATABASE AND IN CLOUD:
1.create a package called resources as such as src 
2. add spconfig.properties which has a common database structure which is used in different local machine.

#Backend properties
domain = http://localhost:8080

#CORS Filter
corsorigin = http://localhost:4200
corsorigin* = *

#Token Generation
tokenscheme = Verte
tokenissuer = http://localhost:8080
tokenaudience = http://localhost:4200

#Database Connection
mysqldriver = com.mysql.jdbc.Driver
mysqlurl = jdbc:mysql://localhost:3306
databasename = sellerportal
dbusername = root
dbpassword = root 

STEP 4 : ADD EXTERNAL JAR FILE IN JRE SYSTEM LIBRARY. JUST DRAG AND DROP THE JAR FILE.
STEP 5 : IN WEB-CONTENT.There is a web.xml file. Servlet is added here. 

example:
 <servlet>
    <servlet-name>Vert Administrator</servlet-name>
    <servlet-class>org.glassfish.jersey.servlet.ServletContainer</servlet-class>
    <init-param>
        <param-name>jersey.config.server.provider.packages</param-name>
        <param-value>com.vertadmin</param-value>
    </init-param>
    <load-on-startup>1</load-on-startup>
  </servlet>
  <servlet-mapping>
    <servlet-name>Vert Administrator</servlet-name>
    <url-pattern>/admin/*</url-pattern>
  </servlet-mapping>
  NOTE : SERVLET-NAME IN BOTH PLACE MUST BE SAME. PARAM-VALUE MUST BE PACKAGE NAME. URL-PATTERN MAY OR MAY NOT BE *. IT ACCEPTS EVERY ENDPOINT URL.

  STEP 6 : CORS-FILE IS IMPORTANT
  
CORSResponseFilter.java
	package com.stores;
	import java.io.IOException;
	import javax.ws.rs.container.ContainerRequestContext;
	import javax.ws.rs.container.ContainerResponseContext;
	import javax.ws.rs.container.ContainerResponseFilter;
	import javax.ws.rs.core.MultivaluedMap;
	import javax.ws.rs.ext.Provider;
//@Provider
	public class CORSResponseFilter implements ContainerResponseFilter {
	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException 
	{
		System.out.println("response-store");

		MultivaluedMap<String, Object> headers = responseContext.getHeaders();
//		headers.add("Access-Control-Allow-Origin", "http://localhost:4200");
//		headers.add("Access-Control-Allow-Origin", "http://13.232.4.131");
		headers.add("Access-Control-Allow-Origin", "*");

//		headers.add( "Access-Control-Allow-Credentials", true );
		headers.add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");	
		
		headers.add( "Access-Control-Allow-Headers", "Content-Type, Authorization, token" );
		headers.add("Access-Control-Expose-Headers", "Content-Type, Authorization, token" );
	}
}

CORSResponseFilter.java
	package com.stores;
	import java.io.IOException;
	import javax.ws.rs.container.ContainerRequestContext;
	import javax.ws.rs.container.ContainerRequestFilter;
	import javax.ws.rs.core.MultivaluedMap;
	import javax.ws.rs.core.Response;
	import javax.ws.rs.ext.Provider;
	import com.token.TokenGenerator;
	import io.jsonwebtoken.*;

//@Provider
public class CORSRequestFilter implements ContainerRequestFilter {

	private static final String TOKEN_HEADER_KEY = "token";
	private static final String OPTIONS_METHOD = "OPTIONS";

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException 
	{	
		System.out.println("request-store");
		if( requestContext.getMethod().equals(OPTIONS_METHOD) )
			return;
		
		if( requestContext.getUriInfo().getPath().contains("login") )
			return;
		
		String authToken = requestContext.getHeaderString(TOKEN_HEADER_KEY);
		if( authToken == null )
		{
			Response response = Response.status(Response.Status.UNAUTHORIZED).entity("There is no valid Authorization header").build();
			requestContext.abortWith(response);
			return;
		}
		
		String token = authToken;
		try
		{			
			TokenGenerator tokenGenerator = new TokenGenerator();
			Claims claims = tokenGenerator.parseToken(token);
			
			requestContext.setProperty( "claims", claims);
			MultivaluedMap<String, String> headers = requestContext.getHeaders();
			headers.add("userName", claims.getSubject());
			return;
		}
		catch(Exception e)
		{
			System.out.println(e);
			Response response = Response.status(Response.Status.UNAUTHORIZED).entity(e.toString()).build();
			requestContext.abortWith(response);
			return;
		}		
	}
}

STEP 7 :
add the tomcat server under server and select the project and run the project
once it started

STEP 8 : GO TO ARC WHICH IS A REST CLIENT
 
